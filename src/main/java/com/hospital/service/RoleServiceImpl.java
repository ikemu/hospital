package com.hospital.service;

import com.hospital.mapper.RoleMapper;
import com.hospital.pojo.Role;
import com.hospital.pojo.RoleExample;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Administrator on 2018/12/17.
 */
@Service
public class RoleServiceImpl  implements  RoleService{
    @Autowired
    private RoleMapper roleMapper;
    @Override
    public List<Role> list() {
        RoleExample example=new RoleExample();
        List<Role> list = roleMapper.selectByExample(example);
        return  list;
    }

    @Override
    public List<Role> listByName(String rolename) {
        RoleExample example=new RoleExample();
        RoleExample.Criteria criteria=example.createCriteria();
        criteria.andRolenameLike("%"+rolename+"%");
        List<Role> roles = roleMapper.selectByExample(example);
        return roles;
    }

    @Override
    public Role getById(Integer roleid) {
        return roleMapper.selectByPrimaryKey(roleid);
    }

    @Override
    public boolean add(Role role) {
        int result = roleMapper.insertSelective(role);

        if(result>0){
            return  true;
        }
        return false;
    }

    @Override
    public boolean remove(Integer roleid) {
        int result= roleMapper.deleteByPrimaryKey(roleid);
        if(result>0){
            return  true;
        }
        return false;
    }

    @Override
    public boolean update(Role role) {
        int result= roleMapper.updateByPrimaryKeySelective(role);
        if(result>0){
            return  true;
        }
        return false;
    }

    public Role roleByroleId(int roleid) {
        return roleMapper.selectByPrimaryKey(roleid);
    }
}
