package com.hospital.service;

import com.hospital.pojo.Chargeproject;

import java.util.List;

public interface ChargeprojectService {

    /**
     * 收费项目查询全部
     * @return List<Chargeproject>
     */
    List<Chargeproject>  chargeprojectList();

    /**
     * 收费项目模糊查询
     * @param  cmname
     * @return List<Chargeproject>
     */
    List<Chargeproject> chargeprojectgetBycmName(String cmname);


    /**
     * 收费项目根据id查询
     * @param  cmid
     * @return Chargeproject
     */
    Chargeproject  chargeprojectgetBycmId(int cmid);


    /**
     * 收费项目根据id修改
     * @param  chargeproject
     * @return 返回true成功 false失败
     */
    boolean   updateChargeproject(Chargeproject chargeproject);


    /**
     * 收费项目根据id删除
     * @param  cmid
     * @return 返回true成功 false失败
     */
    boolean removeChargeproject(int cmid);


    /**
     * 添加收费项目
     * @param  chargeproject
     * @return 返回true成功 false失败
     */
    boolean saveChargeproject(Chargeproject chargeproject);


    /**
     * 查询收费项目名是否存在
     * @param  cmname
     * @return 返回true存在  false不存在
     */
    boolean chargeprojectBycmName(String cmname);



    /**
     * 根据收费项目名查询金额
     * @param  cmname
     * @return  chargeproject
     */
    Chargeproject  chargeprojectfindcmName(String cmname);


}
