package com.hospital.service;


import com.hospital.pojo.Role;

import java.util.List;

/**
 * Created by Administrator on 2018/12/17.
 */
public interface RoleService {

    public List<Role> list();
    public List<Role> listByName(String rolename);
    public Role getById(Integer roleid);
    public  boolean  add(Role role);
    public boolean remove(Integer roleid);
    public boolean update(Role role);

    /**
     * 根据角色id查询是否存在被禁用
     * @param  roleid
     * @return  Role
     */
    Role roleByroleId(int roleid);


}
