package com.hospital.service;

import com.hospital.mapper.PricepeopleMapper;
import com.hospital.pojo.Behospitalinfo;
import com.hospital.pojo.Pricepeople;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Create by Administrator on 2018/12/19 14:10
 */

@Service
public class PricepeopleServiceImpl implements PricepeopleService {

	@Autowired
	private PricepeopleMapper pricepeopleMapper;

	@Override
	public List<Pricepeople> pricepeoList() {
		return pricepeopleMapper.pricepeoList();
	}

	@Override
	public List<Pricepeople> pricepeoLists(String behpid,String behpname) {
		Map<Object,Object> map = new HashMap<>();
		map.put("behpid",behpid);
		map.put("behpname",behpname);
		if (map.get("behpid")==null&&map.get("behpname")==null){//查询条件为空时
			return pricepeopleMapper.pricepeoList();
		}
		return pricepeopleMapper.pricepeoLists(map);//有查询条件时执行
	}

	@Override
	public List<Pricepeople> getPricepeoByHpId(String hpid) {
		return pricepeopleMapper.getPricepeoByHpId(hpid);
	}

	@Override
	public Double getCostByHpId(String hpid) {
		Double costByHpId = pricepeopleMapper.getCostByHpId(hpid);
		return costByHpId;
	}

	@Override
	public List<Pricepeople> priList() {
		return pricepeopleMapper.priList();
	}

}
