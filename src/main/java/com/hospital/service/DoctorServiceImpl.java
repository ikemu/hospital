package com.hospital.service;

import com.hospital.mapper.DoctorMapper;
import com.hospital.pojo.Doctor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

@Service
public class DoctorServiceImpl implements DoctorService{
    @Autowired
    private DoctorMapper doctorMapper;
    @Override
    public List<Doctor> list() {
        List<Doctor> list = doctorMapper.doctorlist();
        return list;
    }

    @Override
    public Doctor getDoctorById(int did) {
        Doctor doctor = doctorMapper.getDoctorById(did);
        return doctor;
    }

    @Override
    public boolean updateDoctor(Doctor doctor) {
        if (doctorMapper.updateByPrimaryKeySelective(doctor)>0){
            return  true;
        }
        return false;
    }

    @Override
    public boolean saveDoctor(Doctor doctor) {
        if (doctorMapper.insertSelective(doctor)>0){
            return true;
        }
        return false;
    }

    @Override
    public List<Doctor> getDoctor(Integer did, String dname, String departname) {
        Map<Object,Object> map =new HashMap<Object,Object>();
        map.put("did",did);
        map.put("dname",dname);
        map.put("departname",departname);
       if (map.get("did")==null&&map.get("dname")==null&&map.get("departname")==null){
                return doctorMapper.doctorlist();
        }
        return doctorMapper.getDoctor(map);
    }
}
