package com.hospital.service;

import com.hospital.mapper.UsersMapper;
import com.hospital.pojo.Role;
import com.hospital.pojo.Users;
import com.hospital.pojo.UsersExample;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Create by Administrator on 2018/12/14 19:43
 */

@Service
public class UsersServiceImpl implements  UsersService {

	@Autowired
	private UsersMapper usersMapper;

	@Override
	public List<Users> list() {
		UsersExample example = new UsersExample();
		List<Users> users = usersMapper.selectByExample(example);
		return users;
	}

	@Override
	public List<Users> listByUsername(String name) {
		UsersExample example=new UsersExample();
		UsersExample.Criteria criteria=example.createCriteria();
		criteria.andNameLike("%"+name+"%");
		List<Users> users = usersMapper.selectByExample(example);
		return  users;
	}

	@Override
	public Users getById(Integer usersid) {
		return usersMapper.selectByPrimaryKey(usersid);
	}

	@Override
	public boolean add(Users users) {
		int  result =usersMapper.insertSelective(users);
		if(result>0){
			return  true;
		}
		return false;
	}

	@Override
	public boolean remove(Integer userid) {
		int result=usersMapper.deleteByPrimaryKey(userid);
		if(result>0){
			return  true;
		}
		return false;
	}

	@Override
	public boolean updateUsers(Users users) {
		int i = usersMapper.updateByPrimaryKeySelective(users);
		if(i>0){
			return true;
		}
		return false;
	}

	@Override
	public boolean login(Users users) {
		UsersExample example = new UsersExample();
        UsersExample.Criteria criteria =example.createCriteria();
        criteria.andUsernameEqualTo(users.getUsername());
		criteria.andPasswordEqualTo(users.getPassword());
		int result = usersMapper.countByExample(example);
        if(result>0){
        	return true;
		}
		return false;
	}

	@Override
	public boolean update(Users users) {
		int result=usersMapper.updateByPrimaryKeySelective(users);
		if(result>0){
			return  true;
		}
		return false;
	}

	public boolean updatePasswordById(Users users) {
		UsersExample example=new UsersExample();
		UsersExample.Criteria  criteria=example.createCriteria();
		criteria.andUseridEqualTo(users.getUserid());
		int result=usersMapper.updateByExampleSelective(users,example);
		if(result>0){
			return  true;
		}
		return false;
	}



	@Override
	public Users getByUsername(String username) {
		UsersExample example = new UsersExample();
		UsersExample.Criteria criteria = example.createCriteria();
		criteria.andUsernameEqualTo(username);
		return  usersMapper.selectByExample(example).get(0);
	}


	public boolean userByuserName(String username) {
		UsersExample example = new UsersExample();
		UsersExample.Criteria criteria = example.createCriteria();
		criteria.andUsernameEqualTo(username);
		int result = usersMapper.countByExample(example);
		if(result>0){
			return  true;
		}
		return false;
	}


}
