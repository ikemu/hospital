package com.hospital.service;

import com.hospital.mapper.MenuMapper;
import com.hospital.pojo.Menu;
import com.hospital.pojo.MenuExample;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MenuServiceImpl implements MenuService {

    @Autowired
    private MenuMapper menuMapper;

    public List<Menu> MenuByroleId(int roleid) {
        return menuMapper.MenuByroleId(roleid);
    }

    @Override
    public List<Menu> list() {
        MenuExample example=new MenuExample();
        List<Menu> list = menuMapper.selectByExample(example);
        return  list;
    }

    @Override
    public Menu getById(Integer menuid) {
        return menuMapper.selectByPrimaryKey(menuid);
    }

    @Override
    public List<Menu> listByName(String menuname) {
        MenuExample example=new MenuExample();
        MenuExample.Criteria criteria=example.createCriteria();
        criteria.andMenunameLike(menuname);
        List<Menu> menus = menuMapper.selectByExample(example);
        return menus;
    }

    @Override
    public boolean add(Menu menu) {
        int result= menuMapper.insertSelective(menu);
        if(result>0){
            return  true;
        }
        return false;
    }

    @Override
    public boolean remove(Integer menuid) {
        int result= menuMapper.deleteByPrimaryKey(menuid);

        if(result>0){
            return  true;
        }
        return false;
    }

    @Override
    public boolean update(Menu menu) {
        int result= menuMapper.updateByPrimaryKeySelective(menu);
        if(result>0){
            return  true;
        }
        return false;
    }


}
