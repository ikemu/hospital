package com.hospital.service;

import com.hospital.mapper.DrugpeopleMapper;
import com.hospital.pojo.Drugpeople;
import com.hospital.pojo.DrugpeopleExample;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DrugPeopleServiceImpl implements DrugPeopleService{

	@Autowired
	private DrugpeopleMapper drugpeopleMapper;

	@Override
	public List<Drugpeople> getByBrid(int brid) {
		DrugpeopleExample example = new DrugpeopleExample();
		example.createCriteria().andDrIdEqualTo(brid);
		List<Drugpeople> list = drugpeopleMapper.selectByExample(example);
		if (list != null){
			return list;
		}
		return null;
	}
}
