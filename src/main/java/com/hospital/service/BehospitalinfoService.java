package com.hospital.service;

import com.hospital.pojo.Behospitalinfo;
import java.util.List;
import java.util.Map;


/**
 * Create by Administrator on 2018/12/15 16:39
 *	住院信息层接口
 */

public interface BehospitalinfoService {

	//查所有 不包括其他信息
	List<Behospitalinfo> list();
	/**
	 * 查询所有 住院办理-信息 包括其他信息
	 * @return 住院办理信息集合
	 */
	List<Behospitalinfo> hospList();
	/**
	 * 模糊查询所有住院办理信息 包括其他信息
	 * @return 集合
	 */
	List<Behospitalinfo> hospLists(String behpid, String dname, String departname, String hosrfromdate, String hosrtodate);
	/**
	 * 查询住院对象
	 * @param hpid 病历号
	 * @return 对象
	 */
	Behospitalinfo gethosByHpId(String beHpId);
	/**
	 * 更新
	 * @param behospitalinfo
	 * @return
	 */
	boolean doUpdate(Behospitalinfo behospitalinfo);

	boolean doAdd(Behospitalinfo behospitalinfo);



	/**
	 * 根据病历号查询
	 * @param  behpid
	 * @return Behospitalinfo
	 */
	Behospitalinfo behospitalinfoBybeHpId(String behpid);

	/**
	 * 根据病历号查询病人是否挂号
	 * @param  behpid
	 * @return true 已挂号 false 未挂号
	 */
	boolean behospitalinfofindbeHpId(String behpid);

}
