package com.hospital.service;

import com.hospital.pojo.Hosregister;

import java.util.Date;
import java.util.List;

public interface RegistrationService {
    /**
     * 查询所有挂号信息
     * @return list
     */
     List<Hosregister> list();

    /**
     * 通过输入模糊查找
     * @param hosregister
     * @return list
     */
     List<Hosregister> listByHosregister(Hosregister hosregister);

    /**
     * 通过hosrid查找挂号信息
     * @param hosrid
     * @return Hosregister
     */
     public  Hosregister getHosregisterById(int hosrid);

    /**
     * 通过id更新挂号表
     * @param hosregister
     * @return boolean
     */
     public  boolean updateHosregister(Hosregister hosregister);

    /**
     * 添加挂号信息
     * @param hosregister
     * @return Boolean
     */
     public  boolean saveHosregister(Hosregister hosregister);

    /**
     * 通过hosrid进行退号
     * @param hosregister
     * @return  Boolean
     */
     public boolean updateRegistration(Hosregister hosregister);

    /**
     * 通过hosrid获取状态
     * @param hosrid
     * @return int
     */
     public int getSate(int hosrid);

    /**
     * 通过hosrid获取挂号信息
     * @param hosrid
     * @return Hosregister
     */
    public Hosregister getRoles(int hosrid);

    public List<Hosregister> getHosregister(String behpid, String dname, String departname, String hosrfromdate, String hosrtodate);
}
