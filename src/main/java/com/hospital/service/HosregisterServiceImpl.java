package com.hospital.service;

import com.hospital.mapper.HosregisterMapper;
import com.hospital.pojo.Hosregister;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.SQLException;
import java.util.List;

/**
 * Create by Administrator on 2018/12/18 17:06
 */

@Service
public class HosregisterServiceImpl implements HosregisterService {

	@Autowired
	private HosregisterMapper hosregisterMapper;

	@Override
	public Hosregister getHosreByHpId(String hpid) {
		return hosregisterMapper.getHosreByHpId(hpid);
	}

	@Override
	public boolean doUpdate(Hosregister hosregister) {
		//通过主键更新表单中需要主键的隐藏表单域
		int i = hosregisterMapper.updateByPrimaryKeySelective(hosregister);
		System.out.println("是否更新成功："+i);
		if(i>0){
			return true;
		}
		return false;
	}

	@Override
	public boolean doAdd(Hosregister hosregister) {
		int i = hosregisterMapper.insertSelective(hosregister);
		if(i>0){
			return true;
		}
		return false;
	}
}
