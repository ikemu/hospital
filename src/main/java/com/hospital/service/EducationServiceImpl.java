package com.hospital.service;

import com.hospital.mapper.EducationMapper;
import com.hospital.pojo.Education;
import com.hospital.pojo.EducationExample;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class EducationServiceImpl implements EducationService {
    @Autowired
    private EducationMapper educationMapper;
    @Override
    public List<Education> list() {
        EducationExample example = new EducationExample();
        List<Education> list = educationMapper.selectByExample(example);
        return list;
    }
}
