package com.hospital.service;

import com.hospital.pojo.Education;

import java.util.List;

public interface EducationService {
    public List<Education> list();
}
