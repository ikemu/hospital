package com.hospital.service;

import com.hospital.pojo.Drug;
import com.hospital.pojo.DrugCondition;

import java.util.List;

public interface MedicineService {

	/**
	 *  查找所有的药品
	 * @return 药品集合
	 */
	List<Drug> list();

	/**
	 * 根据条件查询药品列表
	 * @param drugCondition 查询条件对象
	 * @return 药品列表
	 */
	List<Drug> listByCondition(DrugCondition drugCondition);

	/**
	 * 根据id查找药品
	 * @param id 药品id
	 * @return 药品对象
	 */
	Drug getDrugById(int id);

	/**
	 * 修改药品库存,正数加库存,负数减库存
	 * @param num 修改药品影响数量
	 * @return 修改成功返回true,失败返回false
	 */
	boolean changeDrugRepertory(int id, int num);

	/**
	 * 修改药品信息
	 * @param drug 药品对象
	 * @return 修改成功返回true ,失败 false
	 */
	boolean updateDrug(Drug drug);

	/**
	 * 创建的药品
	 * @param drug 药品对象
	 * @return 创建成功返回true,失败返回false
	 */
	boolean saveDrug(Drug drug);

	/**
	 * 根据id数组找到药品
	 * @param id 药品id数组
	 * @return 药品集合
	 */
	List<Drug> listDrugByIds(int[] id);
}
