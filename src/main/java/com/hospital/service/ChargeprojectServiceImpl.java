package com.hospital.service;

import com.hospital.mapper.ChargeprojectMapper;
import com.hospital.pojo.Chargeproject;
import com.hospital.pojo.ChargeprojectExample;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ChargeprojectServiceImpl implements ChargeprojectService {

    @Autowired
    private ChargeprojectMapper chargeprojectMapper;


    public List<Chargeproject> chargeprojectList() {
        ChargeprojectExample example=new  ChargeprojectExample();
        ChargeprojectExample.Criteria criteria = example.createCriteria();
        criteria.andCmdelEqualTo(1);
        return chargeprojectMapper.selectByExample(example);
    }


    public List<Chargeproject> chargeprojectgetBycmName(String cmname) {
        ChargeprojectExample example=new  ChargeprojectExample();
        if(cmname==null||cmname==""){
            ChargeprojectExample.Criteria criteria = example.createCriteria();
            criteria.andCmdelEqualTo(1);
            return chargeprojectMapper.selectByExample(example);
        }
        ChargeprojectExample.Criteria criteria = example.createCriteria();
        criteria.andCmnameLike("%"+cmname+"%");
        criteria.andCmdelEqualTo(1);
        return chargeprojectMapper.selectByExample(example);
    }


    public Chargeproject chargeprojectgetBycmId(int cmid) {
        ChargeprojectExample example=new  ChargeprojectExample();
        ChargeprojectExample.Criteria criteria = example.createCriteria();
        criteria.andCmidEqualTo(cmid);
        return chargeprojectMapper.selectByExample(example).get(0);
    }


    public boolean updateChargeproject(Chargeproject chargeproject) {
        int result = chargeprojectMapper.updateByPrimaryKeySelective(chargeproject);
        if(result>0){
            return true;
        }
        return false;
    }

    public boolean removeChargeproject(int cmid) {
        Chargeproject chargeproject = chargeprojectgetBycmId(cmid);
        chargeproject.setCmdel(0);
        int result = chargeprojectMapper.updateByPrimaryKeySelective(chargeproject);
        if (result>0){
            return true;
        }
        return false;
    }

    public boolean saveChargeproject(Chargeproject chargeproject) {
        int result = chargeprojectMapper.insertSelective(chargeproject);
        if(result>0){
            return  true;
        }
        return false;
    }

    public boolean chargeprojectBycmName(String cmname) {
        ChargeprojectExample example=new  ChargeprojectExample();
        ChargeprojectExample.Criteria criteria = example.createCriteria();
        criteria.andCmnameEqualTo(cmname);
        int result = chargeprojectMapper.countByExample(example);
        if(result>0){
            return  true;
        }
        return false;
    }

    public Chargeproject chargeprojectfindcmName(String cmname) {
        ChargeprojectExample example=new  ChargeprojectExample();
        ChargeprojectExample.Criteria criteria = example.createCriteria();
        criteria.andCmnameEqualTo(cmname);
        return chargeprojectMapper.selectByExample(example).get(0);
    }

}
