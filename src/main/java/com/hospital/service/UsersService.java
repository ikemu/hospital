package com.hospital.service;

import com.hospital.pojo.Users;
import org.apache.catalina.User;

import java.util.List;

/**
 * Create by Administrator on 2018/12/14 19:39
 * 	用户业务层接口
 */

public interface UsersService {
	public List<Users> list();
	//通过用户名查询
	public List<Users> listByUsername(String name);
	/*通过ID查询用户*/
	public Users getById(Integer usersid);
	/*添加用户*/
	public boolean add(Users users);
	/*删除用户*/
	public boolean remove(Integer userid);
	/*修改用户*/
	public boolean update(Users users);

	public boolean updateUsers(Users users);




	/**
	 * 用户登录
	 * @param  users
	 * @return 登陆成功返回true 失败返回false
	 */
	boolean login(Users users);

	/**
	 * 根据用户名查询用户
	 * @param  username
	 * @return  users
	 */
	Users getByUsername(String username);

	/**
	 * 根据用户名查询用户是否存在
	 * @param  username
	 * @return  true 存在 false不存在
	 */
	boolean userByuserName(String username);



}
