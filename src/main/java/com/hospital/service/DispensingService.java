package com.hospital.service;

import com.hospital.pojo.DispensingCondition;
import com.hospital.pojo.Hosregister;

import java.util.List;

public interface DispensingService {

	/**
	 * 根据可选条件查询 在院发药 的清单
	 * @param condition 条件对象
	 * @return 查询到的清单集合
	 */
	List<Hosregister> listByCondition(DispensingCondition condition);

	/**
	 * 查询对应病人id的开药清单
	 * @param id 挂号id
	 * @return 病人的开药清单集合(一对多关系)
	 */
	List<Hosregister> listForDrugdetail(int id);

	/**
	 * 发药接口,减库存再发药
	 * @param brid 挂号id
	 * @param id 药品id
	 * @param num 要发药品的数量
	 * @return 发药成功返回true,失败返回false
	 */
	boolean dispatchDrug(int brid, int id, int num);

}
