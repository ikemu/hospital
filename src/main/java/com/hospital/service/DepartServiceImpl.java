package com.hospital.service;

import com.hospital.mapper.DepartmentMapper;
import com.hospital.pojo.Department;
import com.hospital.pojo.DepartmentExample;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class DepartServiceImpl implements DepartService {
    @Autowired
    private DepartmentMapper departmentMapper;
    @Override
    public List<Department> list() {
        DepartmentExample example = new DepartmentExample();
        List<Department> list = departmentMapper.selectByExample( example);
        return list;
    }
}
