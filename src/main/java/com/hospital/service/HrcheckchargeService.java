package com.hospital.service;

import com.hospital.pojo.Hrcheckcharge;

import java.util.List;

public interface HrcheckchargeService {

    /**
     * 收费项目登记查询全部
     * @return List<Hrcheckcharge>
     */
    List<Hrcheckcharge>  hrcheckchargeList();

    /**
     * 收费项目登记双条件模糊查询
     * @param  hrcheckcharge
     * @return List<Hrcheckcharge>
     */
    List<Hrcheckcharge>   hrcheckchargeBybehpIdAndbehpName(Hrcheckcharge hrcheckcharge);

    /**
     * 收费项目登记详情查询
     * @param  behpid
     * @return List<Hrcheckcharge>
     */
    List<Hrcheckcharge>  hrcheckchargeBybehpId(String  behpid);

    /**
     * 收费项目计算总金额
     * @param  behpid
     * @return Hrcheckcharge
     */
    Hrcheckcharge   hrcheckchargeBycmMoney(String behpid);


    /**
     * 收费项目登记根据病历号去重查询
     * @param  behpid
     * @return Hrcheckcharge
     */
    Hrcheckcharge   hrcheckchargefinbehpId(String behpid);


    /**
     * 收费项目登记添加收费项
     * @param  hrcheckcharge
     * @return true成功  false失败
     */
    boolean savehrcheckcharge(Hrcheckcharge hrcheckcharge);


}
