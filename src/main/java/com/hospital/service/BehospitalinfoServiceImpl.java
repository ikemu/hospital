package com.hospital.service;

import com.hospital.mapper.BehospitalinfoMapper;
import com.hospital.pojo.Behospitalinfo;
import com.hospital.pojo.BehospitalinfoExample;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Create by Administrator on 2018/12/15 16:41
 *
 */

@Service
public class BehospitalinfoServiceImpl implements BehospitalinfoService {

	@Autowired
	private BehospitalinfoMapper behospitalinfoMapper;

    public Behospitalinfo behospitalinfoBybeHpId(String behpid) {
        BehospitalinfoExample example = new BehospitalinfoExample();
        BehospitalinfoExample.Criteria criteria = example.createCriteria();
        criteria.andBehpidEqualTo(behpid);
        return  behospitalinfoMapper.selectByExample(example).get(0);
    }


    public boolean behospitalinfofindbeHpId(String behpid) {
        BehospitalinfoExample example = new BehospitalinfoExample();
        BehospitalinfoExample.Criteria criteria = example.createCriteria();
        criteria.andBehpidEqualTo(behpid);
        int result = behospitalinfoMapper.countByExample(example);
        if(result>0){
            return true;
        }
        return false;
    }

    @Override
    public List<Behospitalinfo> list() {
        BehospitalinfoExample example = new BehospitalinfoExample();
        return behospitalinfoMapper.selectByExample(example);
    }

    @Override
    public List<Behospitalinfo> hospList() {
        return behospitalinfoMapper.hospList();
    }

    @Override
    public List<Behospitalinfo> hospLists(String behpid, String dname, String departname, String hosrfromdate, String hosrtodate) {
        Map<Object,Object> map = new HashMap<>();
        map.put("behpid",behpid);
        map.put("dname",dname);
        map.put("departname",departname);
        map.put("hosrfromdate",hosrfromdate);
        map.put("hosrtodate",hosrtodate);
        //查询条件为空时
        if (map.get("behpid")==null&&map.get("dname")==null&&map.get("departname")==null&&map.get("hosrfromdate")==null&&map.get("hosrtodate")==null){
            return behospitalinfoMapper.hospList();
        }
        return behospitalinfoMapper.hospLists(map);//有查询条件时执行
    }


    @Override
    public Behospitalinfo gethosByHpId(String beHpId) {
        return behospitalinfoMapper.gethosByHpId(beHpId);
    }

    @Override
    public boolean doUpdate(Behospitalinfo behospitalinfo) {
        int i = behospitalinfoMapper.updateByPrimaryKeySelective(behospitalinfo);
        if(i>0){
            return true;
        }
        return false;
    }

    @Override
    public boolean doAdd(Behospitalinfo behospitalinfo) {
        int i = behospitalinfoMapper.insertSelective(behospitalinfo);
        if(i>0){
            return true;
        }
        return false;
    }
}
