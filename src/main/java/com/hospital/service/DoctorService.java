package com.hospital.service;

import com.hospital.pojo.Doctor;


import java.util.List;

public interface DoctorService {
    /**
     * 查找所有医生
     * @return list
     */
    public List<Doctor> list();

    /**
     * 查看医生详情
     * @param did
     * @return  Doctor
     */
    public Doctor getDoctorById(int did);

    /**
     * 更新医生信息
     * @param doctor
     * @return boolean
     */
    public boolean updateDoctor( Doctor doctor);

    /**
     * 添加医生
     * @param doctor
     * @return Boolean
     */
    public boolean saveDoctor(Doctor doctor);

    public List<Doctor> getDoctor(Integer did,String dname,String departname);
}
