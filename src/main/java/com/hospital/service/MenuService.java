package com.hospital.service;

import com.hospital.pojo.Menu;

import java.util.List;

public interface MenuService {

    /**
     *  根据角色id查询权限访问页面
     * @param  roleid
     * @return  Menu
     */
    List<Menu>  MenuByroleId(int roleid);


    public List<Menu> list();
    public Menu getById(Integer menuid);
    public List<Menu> listByName(String menuname);
    public boolean add(Menu menu);
    public boolean remove(Integer menuid);
    public boolean update(Menu menu);

}
