package com.hospital.service;

import com.hospital.pojo.Behospitalinfo;
import com.hospital.pojo.Pricepeople;

import java.util.List;

/**
 * Create by Administrator on 2018/12/19 14:09
 * 收费项和病人之间的中间表接口
 */

public interface PricepeopleService {
	/**
	 * 查询所有住院结算信息 包括其他信息住院/科室
	 * @return 集合
	 */
	List<Pricepeople> pricepeoList();
	/**
	 * 模糊查询所有住院结算信息 包括其他信息住院/科室
	 * @return
	 */
	List<Pricepeople> pricepeoLists(String behpid,String behpname);
	/**
	 * 根据病历号查询用户住院结算的详情 科室/住院信息
	 * @param hpid 病历号
	 * @return 集合
 	 */
	List<Pricepeople> getPricepeoByHpId(String hpid);
	/**
	 * 根据病历号获取个人总花费
	 * @param hpid 病历号
	 * @return double类型
	 */
	Double getCostByHpId(String hpid);
	/**
	 * 所有集合 - 个人总花费
	 * @return 集合
	 */
	List<Pricepeople> priList();
}
