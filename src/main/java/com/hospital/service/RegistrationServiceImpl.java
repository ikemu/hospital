package com.hospital.service;

import com.hospital.mapper.HosregisterMapper;
import com.hospital.pojo.Hosregister;
import com.hospital.pojo.HosregisterExample;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class RegistrationServiceImpl implements RegistrationService {
    @Autowired
    private HosregisterMapper hosregisterMapper;
    @Override
    public List<Hosregister> list() {
         List<Hosregister> list = hosregisterMapper.hosregisterlist();
        return list;
    }

    @Override
    public List<Hosregister> listByHosregister(Hosregister hosregister) {
       /* if (hosregister != null){
            HosregisterExample example = new HosregisterExample();
            HosregisterExample.Criteria criteria = example.createCriteria();
            if (hosregister.getHosrid() != null && hosregister.getHosrid() != 0) {
                criteria.andHosridEqualTo(hosregister.getHosrid());
            }
            if (hosregister.getDoctor().getDname() != null && hosregister.getDoctor().getDname() != "") {
                criteria.andHrDocIdEqualTo(hosregister.getHrDocId());
            }
        }*/
        return null;
    }

    @Override
    public Hosregister getHosregisterById(int hosrid) {
        return hosregisterMapper.getHosregisterById(hosrid);
    }

    @Override
    public boolean updateHosregister(Hosregister hosregister) {
        if (hosregisterMapper.updateByPrimaryKeySelective(hosregister)>0){
            return true;
        }
        return false;
    }

    @Override
    public boolean saveHosregister(Hosregister hosregister) {
        if(hosregisterMapper.insertSelective(hosregister)>0){
            return  true;
        }
        return false;
    }

    @Override
    public boolean updateRegistration(Hosregister hosregister) {
        if(hosregisterMapper.updateByPrimaryKeySelective(hosregister)>0){
            return  true;
        }
        return false;
    }

    @Override
    public int getSate(int hosrid) {
        Hosregister hosregister = hosregisterMapper.selectByPrimaryKey(hosrid);
        return hosregister.getHrStateId();
    }

    @Override
    public Hosregister getRoles(int hosrid) {
        return hosregisterMapper.getRoles(hosrid);
    }

    @Override
    public List<Hosregister> getHosregister(String behpid, String dname, String departname, String hosrfromdate, String hosrtodate) {
        Map<Object,Object> map =new HashMap<Object,Object>();
        map.put("behpid",behpid);
        map.put("dname",dname);
        map.put("departname",departname);
        map.put("hosrfromdate",hosrfromdate);
        map.put("hosrtodate",hosrtodate);
        if (map.get("behpid")==null&&map.get("dname")==null&&map.get("departname")==null&&map.get("hosrfromdate")==null&&map.get("hosrtodate")==null){
            return hosregisterMapper.hosregisterlist();
        }
        return hosregisterMapper.getHosregister(map);
    }
}
