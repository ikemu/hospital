package com.hospital.service;

import com.hospital.pojo.Drug;

public interface DrugService {

    /**
     * 根据药品名查询
     * @param     drugdname
     * @return   drug
     */
    Drug drugfindBydrugdname(String drugdname);

    /**
     * 根据药品名查询药品
     * @param     drugdname
     * @return   true 存在 false 不存在
     */
    boolean  druggetBydrugdName(String drugdname);
}
