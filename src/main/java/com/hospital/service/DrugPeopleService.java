package com.hospital.service;

import com.hospital.pojo.Drugpeople;

import java.util.List;

public interface DrugPeopleService {

	/**
	 * 根据挂号id找到开药清单
	 * @param brid 挂号id,也算是病人(br)id
	 * @return 开药清单集合
	 */
	List<Drugpeople> getByBrid(int brid);

}
