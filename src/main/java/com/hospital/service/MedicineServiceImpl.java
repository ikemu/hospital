package com.hospital.service;

import com.github.pagehelper.util.StringUtil;
import com.hospital.mapper.DrugMapper;
import com.hospital.pojo.Drug;
import com.hospital.pojo.DrugCondition;
import com.hospital.pojo.DrugExample;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
public class MedicineServiceImpl implements MedicineService{

	@Autowired
	private DrugMapper drugMapper;

	@Override
	public List<Drug> list() {
		List<Drug> list = drugMapper.selectByExample(new DrugExample());
		return list;
	}

	@Override
	public List<Drug> listByCondition(DrugCondition drugCondition) {
		DrugExample example = new DrugExample();
		DrugExample.Criteria criteria = example.createCriteria();
		if (!StringUtils.isEmpty(drugCondition.getDrugName())){
			criteria.andDrugnameLike("%"+drugCondition.getDrugName()+"%");
		}
		if (drugCondition.getDrugType() != null && !drugCondition.getDrugType().equals("8")){
			criteria.andDrugtypeEqualTo(drugCondition.getDrugType());
		}
		List<Drug> list = drugMapper.selectByExample(example);
		return list;
	}

	@Override
	public Drug getDrugById(int id) {
		if (id != 0){
			Drug drug = drugMapper.selectByPrimaryKey(id);
			return drug;
		}
		return null;
	}

	@Override
	public boolean changeDrugRepertory(int id, int num) {
		Drug drug = drugMapper.selectByPrimaryKey(id);
		if (drug != null){
			Drug newDrug = new Drug();
			newDrug.setDrugid(drug.getDrugid());
			newDrug.setDruglastnum(drug.getDruglastnum()+num);
			int i = drugMapper.updateByPrimaryKeySelective(newDrug);
			if (i > 0){
				return true;
			}
		}
		return false;
	}

	@Override
	public boolean updateDrug(Drug drug) {
		if (drug != null){
			int i = drugMapper.updateByPrimaryKeySelective(drug);
			if (i > 0){
				return true;
			}
		}
		return false;
	}

	@Override
	public boolean saveDrug(Drug drug) {
		int i = drugMapper.insertSelective(drug);
		if (i > 0){
			return true;
		}
		return false;
	}

	@Override
	public List<Drug> listDrugByIds(int[] id) {
		if (id.length > 0){
			ArrayList<Integer> list = new ArrayList<>();
			for (int i : id) {
				list.add(i);
			}
			DrugExample example = new DrugExample();
			example.createCriteria().andDrugidIn(list);
			List<Drug> drugList = drugMapper.selectByExample(example);

			return drugList;
		}

		return null;
	}
}
