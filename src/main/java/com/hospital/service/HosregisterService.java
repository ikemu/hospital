package com.hospital.service;


import com.hospital.pojo.Hosregister;

import java.util.List;

/**
 * Create by Administrator on 2018/12/18 16:46
 * 挂号接口
 */

public interface HosregisterService {

	 /**
	 * 通过病历号查询住院办理信息详情 一个挂号信息-对应一个部门/医生/住院信息
	 * @param hpid 参数类型：病历号
	 * @return 住院办理信息对象
	 */
	Hosregister getHosreByHpId(String hpid);
	/**
	 * 更新挂号者信息
	 * @param hosregister 挂号对象
	 * @return 成功返回true反之false
	 */
	boolean doUpdate(Hosregister hosregister);

	boolean doAdd(Hosregister hosregister);
}
