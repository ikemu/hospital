package com.hospital.service;

import com.hospital.pojo.Department;

import java.util.List;

public interface DepartService {
    List<Department> list();
}
