package com.hospital.service;

import com.hospital.mapper.DrugMapper;
import com.hospital.mapper.DrugpeopleMapper;
import com.hospital.mapper.HosregisterMapper;
import com.hospital.pojo.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class DispensingServiceImpl implements DispensingService{

	@Autowired
	private HosregisterMapper hosregisterMapper;
	@Autowired
	private DrugpeopleMapper drugpeopleMapper;
	@Autowired
	private DrugMapper drugMapper;

	@Override
	public List<Hosregister> listByCondition(DispensingCondition condition) {
		DispensingCondition dispensingCondition = new DispensingCondition();
		if (condition.getBrid() != null){
			dispensingCondition.setBrid("%"+condition.getBrid()+"%");
		}
		if (condition.getBrname() != null){
			dispensingCondition.setBrname("%"+condition.getBrname()+"%");
		}
		List<Hosregister> list = hosregisterMapper.listForDispensing(dispensingCondition);
		return list;
	}

	@Override
	public List<Hosregister> listForDrugdetail(int id) {
		List<Hosregister> list = hosregisterMapper.listDrugDetailByPeopleId(id);
		if (list.size() > 0){
			return list;
		}
		return null;
	}

	//变量分别表示  挂号id  药品id  发药数量
	@Transactional
	@Override
	public boolean dispatchDrug(int brid, int id, int num) {
		DrugpeopleExample example = new DrugpeopleExample();
		example.createCriteria().andPeopleidEqualTo(brid).andDrIdEqualTo(id);
		List<Drugpeople> list = drugpeopleMapper.selectByExample(example);
		if (list.size() > 0){
			Drug drug = drugMapper.selectByPrimaryKey(id);
			Drugpeople drugpeople = smartDispatchDrug(list.get(0), num);
			if (drug.getDruglastnum() >= drugpeople.getDrGive()-list.get(0).getDrGive() ) {
				drug.setDruglastnum(drug.getDruglastnum()-drugpeople.getDrGive()+list.get(0).getDrGive());
				int i1 = drugMapper.updateByPrimaryKeySelective(drug);
				int i = drugpeopleMapper.updateByExampleSelective(drugpeople, example);
				if (i > 0 && i1 > 0)
					return true;
			} else {
				return false;
			}

		}
		return false;
	}

	//聪明发药,避免过量发药
	public Drugpeople smartDispatchDrug(Drugpeople drugpeople,int num){
		Drugpeople drugpeople2 = new Drugpeople();
		drugpeople2.setDrId(drugpeople.getDrId());
		if (num == 0 || drugpeople.getDrGive()+num >= drugpeople.getDrNumber()){
			drugpeople2.setDrGive(drugpeople.getDrNumber());
			return drugpeople2;
		} else {
			drugpeople2.setDrGive(drugpeople.getDrGive()+num);
			return drugpeople2;
		}
	}



}
