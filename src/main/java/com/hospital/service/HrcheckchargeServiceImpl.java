package com.hospital.service;

import com.hospital.mapper.HrcheckchargeMapper;
import com.hospital.pojo.Hrcheckcharge;
import com.hospital.pojo.HrcheckchargeExample;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import java.util.List;

@Service
public class HrcheckchargeServiceImpl implements HrcheckchargeService {

    @Autowired
    private HrcheckchargeMapper  hrcheckchargeMapper;


    public List<Hrcheckcharge> hrcheckchargeList() {
       return hrcheckchargeMapper.hrcheckchargeList();
    }

    public List<Hrcheckcharge> hrcheckchargeBybehpIdAndbehpName(Hrcheckcharge hrcheckcharge) {
        if(hrcheckcharge==null){
            return hrcheckchargeMapper.hrcheckchargeList();
        }
        return hrcheckchargeMapper.hrcheckchargeBybehpIdAndbehpName(hrcheckcharge);
    }

    public List<Hrcheckcharge> hrcheckchargeBybehpId(String behpid) {
        HrcheckchargeExample  example =new HrcheckchargeExample();
        HrcheckchargeExample.Criteria criteria = example.createCriteria();
        criteria.andBehpidEqualTo(behpid);
        return hrcheckchargeMapper.selectByExample(example);
    }


    public Hrcheckcharge hrcheckchargeBycmMoney(String behpid) {
        return hrcheckchargeMapper.hrcheckchargeBycmMoney(behpid);
    }


    public Hrcheckcharge hrcheckchargefinbehpId(String behpid) {
        return hrcheckchargeMapper.hrcheckchargefinbehpId(behpid);
    }

    public boolean savehrcheckcharge(Hrcheckcharge hrcheckcharge) {
        int result = hrcheckchargeMapper.insertSelective(hrcheckcharge);
        if(result>0){
            return true;
        }
        return false;
    }

}
