package com.hospital.service;

import com.hospital.mapper.DrugMapper;
import com.hospital.pojo.Drug;
import com.hospital.pojo.DrugExample;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DrugServiceImpl  implements  DrugService{

    @Autowired
    private DrugMapper drugMapper;


    public Drug drugfindBydrugdname(String drugdname) {
        DrugExample example = new DrugExample();
        DrugExample.Criteria criteria = example.createCriteria();
        criteria.andDrugnameEqualTo(drugdname);
        return drugMapper.selectByExample(example).get(0);
    }


    public boolean druggetBydrugdName(String drugdname) {
        DrugExample example = new DrugExample();
        DrugExample.Criteria criteria = example.createCriteria();
        criteria.andDrugnameEqualTo(drugdname);
        int result = drugMapper.countByExample(example);
        if(result>0){
            return true;
        }
        return false;
    }
}
