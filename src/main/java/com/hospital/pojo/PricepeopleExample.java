package com.hospital.pojo;

import java.util.ArrayList;
import java.util.List;

public class PricepeopleExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public PricepeopleExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andChapIdIsNull() {
            addCriterion("chaP_id is null");
            return (Criteria) this;
        }

        public Criteria andChapIdIsNotNull() {
            addCriterion("chaP_id is not null");
            return (Criteria) this;
        }

        public Criteria andChapIdEqualTo(Integer value) {
            addCriterion("chaP_id =", value, "chapId");
            return (Criteria) this;
        }

        public Criteria andChapIdNotEqualTo(Integer value) {
            addCriterion("chaP_id <>", value, "chapId");
            return (Criteria) this;
        }

        public Criteria andChapIdGreaterThan(Integer value) {
            addCriterion("chaP_id >", value, "chapId");
            return (Criteria) this;
        }

        public Criteria andChapIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("chaP_id >=", value, "chapId");
            return (Criteria) this;
        }

        public Criteria andChapIdLessThan(Integer value) {
            addCriterion("chaP_id <", value, "chapId");
            return (Criteria) this;
        }

        public Criteria andChapIdLessThanOrEqualTo(Integer value) {
            addCriterion("chaP_id <=", value, "chapId");
            return (Criteria) this;
        }

        public Criteria andChapIdIn(List<Integer> values) {
            addCriterion("chaP_id in", values, "chapId");
            return (Criteria) this;
        }

        public Criteria andChapIdNotIn(List<Integer> values) {
            addCriterion("chaP_id not in", values, "chapId");
            return (Criteria) this;
        }

        public Criteria andChapIdBetween(Integer value1, Integer value2) {
            addCriterion("chaP_id between", value1, value2, "chapId");
            return (Criteria) this;
        }

        public Criteria andChapIdNotBetween(Integer value1, Integer value2) {
            addCriterion("chaP_id not between", value1, value2, "chapId");
            return (Criteria) this;
        }

        public Criteria andBehIdIsNull() {
            addCriterion("beH_id is null");
            return (Criteria) this;
        }

        public Criteria andBehIdIsNotNull() {
            addCriterion("beH_id is not null");
            return (Criteria) this;
        }

        public Criteria andBehIdEqualTo(String value) {
            addCriterion("beH_id =", value, "behId");
            return (Criteria) this;
        }

        public Criteria andBehIdNotEqualTo(String value) {
            addCriterion("beH_id <>", value, "behId");
            return (Criteria) this;
        }

        public Criteria andBehIdGreaterThan(String value) {
            addCriterion("beH_id >", value, "behId");
            return (Criteria) this;
        }

        public Criteria andBehIdGreaterThanOrEqualTo(String value) {
            addCriterion("beH_id >=", value, "behId");
            return (Criteria) this;
        }

        public Criteria andBehIdLessThan(String value) {
            addCriterion("beH_id <", value, "behId");
            return (Criteria) this;
        }

        public Criteria andBehIdLessThanOrEqualTo(String value) {
            addCriterion("beH_id <=", value, "behId");
            return (Criteria) this;
        }

        public Criteria andBehIdLike(String value) {
            addCriterion("beH_id like", value, "behId");
            return (Criteria) this;
        }

        public Criteria andBehIdNotLike(String value) {
            addCriterion("beH_id not like", value, "behId");
            return (Criteria) this;
        }

        public Criteria andBehIdIn(List<String> values) {
            addCriterion("beH_id in", values, "behId");
            return (Criteria) this;
        }

        public Criteria andBehIdNotIn(List<String> values) {
            addCriterion("beH_id not in", values, "behId");
            return (Criteria) this;
        }

        public Criteria andBehIdBetween(String value1, String value2) {
            addCriterion("beH_id between", value1, value2, "behId");
            return (Criteria) this;
        }

        public Criteria andBehIdNotBetween(String value1, String value2) {
            addCriterion("beH_id not between", value1, value2, "behId");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}