package com.hospital.pojo;

public class Drugpeople {
    private Integer drId;

    private Integer peopleid;

    private Integer drNumber;

    private Integer drGive;

    private Drug drug;

    public Integer getDrId() {
        return drId;
    }

    public Drug getDrug() {
        return drug;
    }

    public void setDrug(Drug drug) {
        this.drug = drug;
    }

    public void setDrId(Integer drId) {
        this.drId = drId;
    }

    public Integer getPeopleid() {
        return peopleid;
    }

    public void setPeopleid(Integer peopleid) {
        this.peopleid = peopleid;
    }

    public Integer getDrNumber() {
        return drNumber;
    }

    public void setDrNumber(Integer drNumber) {
        this.drNumber = drNumber;
    }

    public Integer getDrGive() {
        return drGive;
    }

    public void setDrGive(Integer drGive) {
        this.drGive = drGive;
    }
}