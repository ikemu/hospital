package com.hospital.pojo;

public class DispensingCondition {

	private String brid;

	private String brname;

	public DispensingCondition() {
	}
	public DispensingCondition(String brid, String brname) {
		this.brid = brid;
		this.brname = brname;
	}

	public String getBrid() {
		return brid;
	}

	public void setBrid(String brid) {
//		this.brid = "%"+brid+"%";
		this.brid = brid;
	}

	public String getBrname() {
		return brname;
	}

	public void setBrname(String brname) {
//		this.brname = "%"+brname+"%";
		this.brname = brname;
	}

	@Override
	public String toString() {
		return "DispensingCondition{" +
				"brid='" + brid + '\'' +
				", brname='" + brname + '\'' +
				'}';
	}
}
