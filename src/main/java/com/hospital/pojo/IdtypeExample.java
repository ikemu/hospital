package com.hospital.pojo;

import java.util.ArrayList;
import java.util.List;

public class IdtypeExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public IdtypeExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdtypeidIsNull() {
            addCriterion("idtypeId is null");
            return (Criteria) this;
        }

        public Criteria andIdtypeidIsNotNull() {
            addCriterion("idtypeId is not null");
            return (Criteria) this;
        }

        public Criteria andIdtypeidEqualTo(Integer value) {
            addCriterion("idtypeId =", value, "idtypeid");
            return (Criteria) this;
        }

        public Criteria andIdtypeidNotEqualTo(Integer value) {
            addCriterion("idtypeId <>", value, "idtypeid");
            return (Criteria) this;
        }

        public Criteria andIdtypeidGreaterThan(Integer value) {
            addCriterion("idtypeId >", value, "idtypeid");
            return (Criteria) this;
        }

        public Criteria andIdtypeidGreaterThanOrEqualTo(Integer value) {
            addCriterion("idtypeId >=", value, "idtypeid");
            return (Criteria) this;
        }

        public Criteria andIdtypeidLessThan(Integer value) {
            addCriterion("idtypeId <", value, "idtypeid");
            return (Criteria) this;
        }

        public Criteria andIdtypeidLessThanOrEqualTo(Integer value) {
            addCriterion("idtypeId <=", value, "idtypeid");
            return (Criteria) this;
        }

        public Criteria andIdtypeidIn(List<Integer> values) {
            addCriterion("idtypeId in", values, "idtypeid");
            return (Criteria) this;
        }

        public Criteria andIdtypeidNotIn(List<Integer> values) {
            addCriterion("idtypeId not in", values, "idtypeid");
            return (Criteria) this;
        }

        public Criteria andIdtypeidBetween(Integer value1, Integer value2) {
            addCriterion("idtypeId between", value1, value2, "idtypeid");
            return (Criteria) this;
        }

        public Criteria andIdtypeidNotBetween(Integer value1, Integer value2) {
            addCriterion("idtypeId not between", value1, value2, "idtypeid");
            return (Criteria) this;
        }

        public Criteria andIdtypenameIsNull() {
            addCriterion("idtypeName is null");
            return (Criteria) this;
        }

        public Criteria andIdtypenameIsNotNull() {
            addCriterion("idtypeName is not null");
            return (Criteria) this;
        }

        public Criteria andIdtypenameEqualTo(String value) {
            addCriterion("idtypeName =", value, "idtypename");
            return (Criteria) this;
        }

        public Criteria andIdtypenameNotEqualTo(String value) {
            addCriterion("idtypeName <>", value, "idtypename");
            return (Criteria) this;
        }

        public Criteria andIdtypenameGreaterThan(String value) {
            addCriterion("idtypeName >", value, "idtypename");
            return (Criteria) this;
        }

        public Criteria andIdtypenameGreaterThanOrEqualTo(String value) {
            addCriterion("idtypeName >=", value, "idtypename");
            return (Criteria) this;
        }

        public Criteria andIdtypenameLessThan(String value) {
            addCriterion("idtypeName <", value, "idtypename");
            return (Criteria) this;
        }

        public Criteria andIdtypenameLessThanOrEqualTo(String value) {
            addCriterion("idtypeName <=", value, "idtypename");
            return (Criteria) this;
        }

        public Criteria andIdtypenameLike(String value) {
            addCriterion("idtypeName like", value, "idtypename");
            return (Criteria) this;
        }

        public Criteria andIdtypenameNotLike(String value) {
            addCriterion("idtypeName not like", value, "idtypename");
            return (Criteria) this;
        }

        public Criteria andIdtypenameIn(List<String> values) {
            addCriterion("idtypeName in", values, "idtypename");
            return (Criteria) this;
        }

        public Criteria andIdtypenameNotIn(List<String> values) {
            addCriterion("idtypeName not in", values, "idtypename");
            return (Criteria) this;
        }

        public Criteria andIdtypenameBetween(String value1, String value2) {
            addCriterion("idtypeName between", value1, value2, "idtypename");
            return (Criteria) this;
        }

        public Criteria andIdtypenameNotBetween(String value1, String value2) {
            addCriterion("idtypeName not between", value1, value2, "idtypename");
            return (Criteria) this;
        }

        public Criteria andIdtypedelIsNull() {
            addCriterion("idtypeDel is null");
            return (Criteria) this;
        }

        public Criteria andIdtypedelIsNotNull() {
            addCriterion("idtypeDel is not null");
            return (Criteria) this;
        }

        public Criteria andIdtypedelEqualTo(Integer value) {
            addCriterion("idtypeDel =", value, "idtypedel");
            return (Criteria) this;
        }

        public Criteria andIdtypedelNotEqualTo(Integer value) {
            addCriterion("idtypeDel <>", value, "idtypedel");
            return (Criteria) this;
        }

        public Criteria andIdtypedelGreaterThan(Integer value) {
            addCriterion("idtypeDel >", value, "idtypedel");
            return (Criteria) this;
        }

        public Criteria andIdtypedelGreaterThanOrEqualTo(Integer value) {
            addCriterion("idtypeDel >=", value, "idtypedel");
            return (Criteria) this;
        }

        public Criteria andIdtypedelLessThan(Integer value) {
            addCriterion("idtypeDel <", value, "idtypedel");
            return (Criteria) this;
        }

        public Criteria andIdtypedelLessThanOrEqualTo(Integer value) {
            addCriterion("idtypeDel <=", value, "idtypedel");
            return (Criteria) this;
        }

        public Criteria andIdtypedelIn(List<Integer> values) {
            addCriterion("idtypeDel in", values, "idtypedel");
            return (Criteria) this;
        }

        public Criteria andIdtypedelNotIn(List<Integer> values) {
            addCriterion("idtypeDel not in", values, "idtypedel");
            return (Criteria) this;
        }

        public Criteria andIdtypedelBetween(Integer value1, Integer value2) {
            addCriterion("idtypeDel between", value1, value2, "idtypedel");
            return (Criteria) this;
        }

        public Criteria andIdtypedelNotBetween(Integer value1, Integer value2) {
            addCriterion("idtypeDel not between", value1, value2, "idtypedel");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}