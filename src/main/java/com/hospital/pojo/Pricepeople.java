package com.hospital.pojo;

import java.util.List;

public class Pricepeople {
	private Integer chapId;

    private String behId;

    private Chargeproject chargeproject;
    private Behospitalinfo  behospitalinfo;
    public Chargeproject getChargeproject() {
        return chargeproject;
    }
    public void setChargeproject(Chargeproject chargeproject) {
        this.chargeproject = chargeproject;
    }
    public Behospitalinfo getBehospitalinfo() {
        return behospitalinfo;
    }
    public void setBehospitalinfo(Behospitalinfo behospitalinfo) {
        this.behospitalinfo = behospitalinfo;
    }
    private List<Chargeproject> chargeprojectList;
    public List<Chargeproject> getChargeprojectList() {
        return chargeprojectList;
    }
    public void setChargeprojectList(List<Chargeproject> chargeprojectList) {
        this.chargeprojectList = chargeprojectList;
    }


    public Integer getChapId() {
        return chapId;
    }

    public void setChapId(Integer chapId) {
        this.chapId = chapId;
    }

    public String getBehId() {
        return behId;
    }

    public void setBehId(String behId) {
        this.behId = behId == null ? null : behId.trim();
    }
}