package com.hospital.pojo;

public class Idtype {
    private Integer idtypeid;

    private String idtypename;

    private Integer idtypedel;

    public Integer getIdtypeid() {
        return idtypeid;
    }

    public void setIdtypeid(Integer idtypeid) {
        this.idtypeid = idtypeid;
    }

    public String getIdtypename() {
        return idtypename;
    }

    public void setIdtypename(String idtypename) {
        this.idtypename = idtypename == null ? null : idtypename.trim();
    }

    public Integer getIdtypedel() {
        return idtypedel;
    }

    public void setIdtypedel(Integer idtypedel) {
        this.idtypedel = idtypedel;
    }
}