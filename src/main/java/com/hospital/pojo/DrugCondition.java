package com.hospital.pojo;

public class DrugCondition {

	private String drugName;
	private String drugType;

	public DrugCondition() {
	}

	public DrugCondition(String drugName, String drugType) {
		this.drugName = drugName;
		this.drugType = drugType;
	}

	public String getDrugName() {
		return drugName;
	}

	public void setDrugName(String drugName) {
		this.drugName = drugName;
	}

	public String getDrugType() {
		return drugType;
	}

	public void setDrugType(String drugType) {
		this.drugType = drugType;
	}

	@Override
	public String toString() {
		return "DrugCondition{" +
				"drugName='" + drugName + '\'' +
				", drugType='" + drugType + '\'' +
				'}';
	}
}
