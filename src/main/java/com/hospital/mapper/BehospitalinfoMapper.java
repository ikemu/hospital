package com.hospital.mapper;

import com.hospital.pojo.Behospitalinfo;
import com.hospital.pojo.BehospitalinfoExample;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

public interface BehospitalinfoMapper {
    int countByExample(BehospitalinfoExample example);

    int deleteByExample(BehospitalinfoExample example);

    int deleteByPrimaryKey(Integer bhid);

    int insert(Behospitalinfo record);

    int insertSelective(Behospitalinfo record);

    List<Behospitalinfo> selectByExample(BehospitalinfoExample example);

    Behospitalinfo selectByPrimaryKey(Integer bhid);

    int updateByExampleSelective(@Param("record") Behospitalinfo record, @Param("example") BehospitalinfoExample example);

    int updateByExample(@Param("record") Behospitalinfo record, @Param("example") BehospitalinfoExample example);

    int updateByPrimaryKeySelective(Behospitalinfo record);

    int updateByPrimaryKey(Behospitalinfo record);

    /**
     * 查询所有住院办理信息 包括其他信息
     * @return
     */
    List<Behospitalinfo> hospList();
    /**
     * 模糊查询所有住院办理信息 包括其他信息
     * @return
     */
    List<Behospitalinfo> hospLists(Map<Object,Object> map);
    /**
     * 通过病历号查询 包括其他信息
     * @param beHpId
     * @return
     */
    List<Behospitalinfo> gethosListByHpId(String beHpId);
    /**
     * 通过病历号查询对象 包括其他信息
     * @param beHpId
     * @return
     */
    Behospitalinfo gethosByHpId(String beHpId);



}