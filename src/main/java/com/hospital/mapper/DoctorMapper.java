package com.hospital.mapper;

import com.hospital.pojo.Doctor;
import com.hospital.pojo.DoctorExample;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

public interface DoctorMapper {
    int countByExample(DoctorExample example);

    int deleteByExample(DoctorExample example);

    int deleteByPrimaryKey(Integer did);

    int insert(Doctor record);

    int insertSelective(Doctor record);

    List<Doctor> selectByExample(DoctorExample example);

    Doctor selectByPrimaryKey(Integer did);

    int updateByExampleSelective(@Param("record") Doctor record, @Param("example") DoctorExample example);

    int updateByExample(@Param("record") Doctor record, @Param("example") DoctorExample example);

    int updateByPrimaryKeySelective(Doctor record);

    int updateByPrimaryKey(Doctor record);

    List<Doctor> doctorlist();

    Doctor getDoctorById(int did);

    public Doctor getRoles(int did);

    public List<Doctor> getDoctor(Map<Object,Object> map);
}