package com.hospital.mapper;

import com.hospital.pojo.Drugpeople;
import com.hospital.pojo.DrugpeopleExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface DrugpeopleMapper {
    int countByExample(DrugpeopleExample example);

    int deleteByExample(DrugpeopleExample example);

    int insert(Drugpeople record);

    int insertSelective(Drugpeople record);

    List<Drugpeople> selectByExample(DrugpeopleExample example);

    int updateByExampleSelective(@Param("record") Drugpeople record, @Param("example") DrugpeopleExample example);

    int updateByExample(@Param("record") Drugpeople record, @Param("example") DrugpeopleExample example);
}