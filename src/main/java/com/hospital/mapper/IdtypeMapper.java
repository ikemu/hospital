package com.hospital.mapper;

import com.hospital.pojo.Idtype;
import com.hospital.pojo.IdtypeExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface IdtypeMapper {
    int countByExample(IdtypeExample example);

    int deleteByExample(IdtypeExample example);

    int deleteByPrimaryKey(Integer idtypeid);

    int insert(Idtype record);

    int insertSelective(Idtype record);

    List<Idtype> selectByExample(IdtypeExample example);

    Idtype selectByPrimaryKey(Integer idtypeid);

    int updateByExampleSelective(@Param("record") Idtype record, @Param("example") IdtypeExample example);

    int updateByExample(@Param("record") Idtype record, @Param("example") IdtypeExample example);

    int updateByPrimaryKeySelective(Idtype record);

    int updateByPrimaryKey(Idtype record);
}