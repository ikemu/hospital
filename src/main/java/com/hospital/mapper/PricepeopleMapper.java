package com.hospital.mapper;

import com.hospital.pojo.Behospitalinfo;
import com.hospital.pojo.Pricepeople;
import com.hospital.pojo.PricepeopleExample;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

public interface PricepeopleMapper {
    int countByExample(PricepeopleExample example);

    int deleteByExample(PricepeopleExample example);

    int insert(Pricepeople record);

    int insertSelective(Pricepeople record);

    List<Pricepeople> selectByExample(PricepeopleExample example);

    int updateByExampleSelective(@Param("record") Pricepeople record, @Param("example") PricepeopleExample example);

    int updateByExample(@Param("record") Pricepeople record, @Param("example") PricepeopleExample example);

    //查询所有住院结算信息 包括其他信息住院/科室
    List<Pricepeople> pricepeoList();
    //模糊查询所有住院结算信息 包括其他信息住院/科室
    List<Pricepeople> pricepeoLists(Map<Object,Object> map);
    //根据病历号查询用户住院结算的详情 科室/住院信息
    List<Pricepeople> getPricepeoByHpId(String hpid);
    //获取个人总花费
    Double getCostByHpId(String hpid);
    //所有集合-个人总花费
    List<Pricepeople> priList();
}