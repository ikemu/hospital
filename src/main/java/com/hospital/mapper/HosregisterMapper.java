package com.hospital.mapper;

import com.hospital.pojo.DispensingCondition;
import com.hospital.pojo.Hosregister;
import com.hospital.pojo.HosregisterExample;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

public interface HosregisterMapper {
    int countByExample(HosregisterExample example);

    int deleteByExample(HosregisterExample example);

    int deleteByPrimaryKey(Integer hosrid);

    int insert(Hosregister record);

    int insertSelective(Hosregister record);

    List<Hosregister> selectByExample(HosregisterExample example);

    Hosregister selectByPrimaryKey(Integer hosrid);

    int updateByExampleSelective(@Param("record") Hosregister record, @Param("example") HosregisterExample example);

    int updateByExample(@Param("record") Hosregister record, @Param("example") HosregisterExample example);

    int updateByPrimaryKeySelective(Hosregister record);

    int updateByPrimaryKey(Hosregister record);

    //通过病历号查询住院办理信息详情 一个挂号信息-对应一个部门/医生/住院信息
    Hosregister getHosreByHpId(String hpid);

    List<Hosregister> hosregisterlist();

    Hosregister getHosregisterById(int hosrid);

    List<Hosregister> listForDispensing(DispensingCondition condition);

    List<Hosregister> listDrugDetailByPeopleId(int id);

    public Hosregister getRoles(int hosrid);

    List<Hosregister> getHosregister(Map<Object,Object> map);
}