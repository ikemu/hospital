package com.hospital.mapper;

import com.hospital.pojo.Hrcheckcharge;
import com.hospital.pojo.HrcheckchargeExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface HrcheckchargeMapper {
    int countByExample(HrcheckchargeExample example);

    int deleteByExample(HrcheckchargeExample example);

    int deleteByPrimaryKey(Integer cuid);

    int insert(Hrcheckcharge record);

    int insertSelective(Hrcheckcharge record);

    List<Hrcheckcharge> selectByExample(HrcheckchargeExample example);

    Hrcheckcharge selectByPrimaryKey(Integer cuid);

    int updateByExampleSelective(@Param("record") Hrcheckcharge record, @Param("example") HrcheckchargeExample example);

    int updateByExample(@Param("record") Hrcheckcharge record, @Param("example") HrcheckchargeExample example);

    int updateByPrimaryKeySelective(Hrcheckcharge record);

    int updateByPrimaryKey(Hrcheckcharge record);

    //去重查询全部
    List<Hrcheckcharge> hrcheckchargeList();

    //双条件模糊去重查询
    List<Hrcheckcharge>   hrcheckchargeBybehpIdAndbehpName(Hrcheckcharge hrcheckcharge);

    //计算收费项目总金额
    Hrcheckcharge   hrcheckchargeBycmMoney(String behpid);

    //去重查询根据病历号
   Hrcheckcharge  hrcheckchargefinbehpId(String behpid);

}