package com.hospital.mapper;

import com.hospital.pojo.Role_menu;
import com.hospital.pojo.Role_menuExample;
import com.hospital.pojo.Role_menuKey;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface Role_menuMapper {
    int countByExample(Role_menuExample example);

    int deleteByExample(Role_menuExample example);

    int deleteByPrimaryKey(Role_menuKey key);

    int insert(Role_menu record);

    int insertSelective(Role_menu record);

    List<Role_menu> selectByExample(Role_menuExample example);

    Role_menu selectByPrimaryKey(Role_menuKey key);

    int updateByExampleSelective(@Param("record") Role_menu record, @Param("example") Role_menuExample example);

    int updateByExample(@Param("record") Role_menu record, @Param("example") Role_menuExample example);

    int updateByPrimaryKeySelective(Role_menu record);

    int updateByPrimaryKey(Role_menu record);
}