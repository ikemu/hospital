package com.hospital.controller;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hospital.pojo.DispensingCondition;
import com.hospital.pojo.Doctor;
import com.hospital.pojo.Drugpeople;
import com.hospital.pojo.Hosregister;
import com.hospital.service.DispensingService;
import com.hospital.service.DrugPeopleService;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Controller
@RequestMapping("dispensing")
public class DispensingController {

	@Autowired
	private DispensingService dispensingService;
	@Autowired
	private DrugPeopleService drugPeopleService;

	//在院开药路由
	@RequestMapping
	public String index(Model model, DispensingCondition condition, @RequestParam(required=true,defaultValue="1")int pageNum,
						@RequestParam(required=true,defaultValue="12")int pageSize) {
		PageHelper.startPage(pageNum, pageSize);
		List<Hosregister> list = dispensingService.listByCondition(condition);
		PageInfo<Hosregister> page = new PageInfo<>(list);
		model.addAttribute("page", page);
		model.addAttribute("dispensingList", list);
		model.addAttribute("condition", condition);
		return "hospital/dispensing";
	}

	//查看开药详情
	@RequestMapping("/drugDetail")
	public String drugDetail(int id, Model model, @RequestParam(required=true,defaultValue="1")int pageNum) {
		PageHelper.startPage(pageNum, 12);
		List<Hosregister> list = dispensingService.listForDrugdetail(id);
		PageInfo<Hosregister> page = new PageInfo<>(list);
		model.addAttribute("detailList", list);
		model.addAttribute("page", page);
		return "hospital/dispensing-look";
	}

	//ajax发药接口
	@RequestMapping("/dispatch")
	@ResponseBody
	public String dispatchDrug(int brid, int id, int num){
		if (dispensingService.dispatchDrug(brid, id, num)){
			return "true";
		}
		return "false";
	}

	//发药路由
	@RequestMapping("/dispensing-give")
	public String dispensing(int brid, Model model){
		List<Drugpeople> drugList = drugPeopleService.getByBrid(brid);
		List<Hosregister> hosregisterList = dispensingService.listForDrugdetail(brid);
		model.addAttribute("drugList", drugList);
		model.addAttribute("hosregisterList", hosregisterList);
		return "hospital/dispensing-give";
	}




}
