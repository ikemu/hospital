package com.hospital.controller;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hospital.pojo.Doctor;
import com.hospital.pojo.Drug;
import com.hospital.pojo.DrugCondition;
import com.hospital.service.MedicineService;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletResponse;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Controller
@RequestMapping("medicine")
public class MedicineController {

	@Autowired
	private MedicineService medicineService;

	//药品管理首页
	@RequestMapping
	public String index(Model model, DrugCondition drugCondition, @RequestParam(required=true,defaultValue="1")int pageNum,
						@RequestParam(required=true,defaultValue="12")int pageSize) {
		if (drugCondition.getDrugType() == null)
			drugCondition.setDrugType("8");
		PageHelper.startPage(pageNum, pageSize);

		List<Drug> list = medicineService.listByCondition(drugCondition);
		PageInfo<Drug> page = new PageInfo<>(list);
		model.addAttribute("page", page);
		model.addAttribute("drugList", list);
		model.addAttribute("condition", drugCondition);

		return "medicine/index";
	}

	//添加药品库存路由
	@RequestMapping("/addRepertory")
	public String addRepertory(int id, Model model){
		Drug drug = medicineService.getDrugById(id);
		model.addAttribute("drug", drug);
		return "medicine/add_Repertory";
	}

	//添加药品库存(repertory)
	@RequestMapping("/add")
	public String add(int id, int num, Model model, HttpServletResponse response) throws IOException {
		if (medicineService.changeDrugRepertory(id, num)){
			return "redirect:/medicine";
		}
		response.setCharacterEncoding("utf-8");
		PrintWriter writer = response.getWriter();

		writer.write("<script>alert(\"修改失败,请再试一次\")</script>");
		model.addAttribute("drug", medicineService.getDrugById(id));
		return "medicine/add_Repertory";
	}

	//查看药品详情
	@RequestMapping("/look")
	public String look(int id, Model model){
		Drug drug = medicineService.getDrugById(id);
		model.addAttribute("drug", drug);
		return "medicine/look";
	}

	//修改药品路由
	@RequestMapping("/updateDrug")
	public String updateDrug(int id, Model model){
		Drug drug = medicineService.getDrugById(id);
		model.addAttribute("drug", drug);
		return "/medicine/update";
	}

	//修改药品
	@RequestMapping("/update")
	public String update(Drug drug, Model model){
		if (medicineService.updateDrug(drug)){
			return "redirect:/medicine";
		}
		model.addAttribute("drug", drug);
		return "redirect:/medicine/update";
	}

	//添加药品路由
	@RequestMapping("/addDrug")
	public String addDrug(Model model){
		model.addAttribute("drug", new Drug());
		return "medicine/add";
	}

	//创建新的药品
	@RequestMapping("/add2")
	public String add2(Drug drug, Model model, HttpServletResponse response) throws IOException {
		if (medicineService.saveDrug(drug)){
			return "redirect:/medicine";
		}
		response.setCharacterEncoding("utf-8");
		response.getWriter().write("<script>alert(\"添加失败,请再试一次\")</script>");
		model.addAttribute("drug", drug);
		return "medicine/add";
	}

	@RequestMapping("/excel")
	public String exportExcel(int[] ids) throws IOException {

		List<Drug> list = medicineService.listDrugByIds(ids);

		// 要导出的数据集合
		XSSFWorkbook wb = new XSSFWorkbook();
		// 2.创建工作表对象并命名
		XSSFSheet sheet = wb.createSheet("药品信息表");
		int rowCount = 0; // 行数
		sheet.addMergedRegion(new CellRangeAddress(0, 0, 0, 5));
		XSSFRow rowTitle = sheet.createRow(rowCount++);
		XSSFCell cellTile = rowTitle.createCell(0);
		cellTile.setCellValue("药品信息表");
		// 4. 处理标题
		String[] title = new String[] {  "药品编号", "药品名称", "药品类型", "药品状态","剩余量" };
		XSSFRow smallRow;
		smallRow = sheet.createRow(rowCount++);
		for (int i = 0;i < title.length; i++) {
			XSSFCell createCell = smallRow.createCell(i);
			createCell.setCellValue(title[i]);
		}
		for (int i = 0; i < list.size(); i++) {
			Drug drug = list.get(i);
			// 创建行
			XSSFRow row = sheet.createRow(rowCount++);
			// 开始创建单元格并赋值
			XSSFCell dtdId = row.createCell(0);
			dtdId.setCellValue(drug.getDrugid());

			XSSFCell dtName = row.createCell(1);
			dtName.setCellValue(drug.getDrugname());

			XSSFCell dtdInnerDate = row.createCell(2);
			String[] type = {"处方药","中药","西药"};
			dtdInnerDate.setCellValue(type[Integer.parseInt(drug.getDrugtype())]);

			XSSFCell dtd_depart_id = row.createCell(3);
			if (drug.getDrugstate()==0){
				dtd_depart_id.setCellValue("销售中");
			} else {
				dtd_depart_id.setCellValue("禁止销售");
			}


			XSSFCell dtd_depart_i = row.createCell(4);
			dtd_depart_i.setCellValue(drug.getDruglastnum());
		}
		String filename = new Date().getTime()+"药品信息表.xlsx";
		FileOutputStream out = new FileOutputStream("c:/EXcel/"+filename);
		wb.write(out);
		out.close();

		return "redirect:/medicine";
	}


}
