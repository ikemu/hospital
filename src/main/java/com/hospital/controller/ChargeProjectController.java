package com.hospital.controller;


import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hospital.pojo.Chargeproject;
import com.hospital.service.ChargeprojectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Date;
import java.util.List;

@Controller
@RequestMapping("chargeproject")
public class ChargeProjectController {

    @Autowired
    private ChargeprojectService chargeprojectService;


    //收费项目查询全部&根据名称模糊查询
    @RequestMapping("chargeManage")
    public String chargeManage(Model model,String cmname,Chargeproject chargeproject,@RequestParam(required=true,defaultValue="1")int pageNum, @RequestParam(required=true,defaultValue="10")int pageSize){
        PageHelper.startPage(pageNum, pageSize);
        cmname=chargeproject.getCmname();
        List<Chargeproject> list = chargeprojectService.chargeprojectgetBycmName(cmname);
        PageInfo<Chargeproject> page = new PageInfo<>(list);
        model.addAttribute("page", page);
        model.addAttribute("chargeprojectList",list);
        if(chargeproject==null){
            model.addAttribute("chargeproject",new Chargeproject());
        }
        model.addAttribute("chargeproject",chargeproject);
        return "hospital/charge";
    }

    //根据id查看详情
    @RequestMapping("chargeDetails")
    public String chargeDetails(Model model,int cmid){
        Chargeproject chargeproject = chargeprojectService.chargeprojectgetBycmId(cmid);
        model.addAttribute("chargeproject",chargeproject);
        return "hospital/chargedetails";
  }

   //根据id删除
   @RequestMapping("chargeRemove")
   public String chargeRemove(Model model,int cmid) throws IOException {
      if(chargeprojectService.removeChargeproject(cmid)){
         model.addAttribute("msg","<script>alert(\"删除失败!\")</script>");
          return "redirect:/chargeproject/chargeManage";
      }
       model.addAttribute("msg","<script>alert(\"删除成功!\")</script>");
       return "redirect:/chargeproject/chargeManage";
   }


    //根据id修改操作
    @RequestMapping("chargeUpdate")
    public String chargeUpdate(Model model,int cmid){
        Chargeproject chargeproject = chargeprojectService.chargeprojectgetBycmId(cmid);
        model.addAttribute("chargeproject",chargeproject);
        return "hospital/charge-edit1";
    }
    @RequestMapping("updateCharge")
    public String  updateCharge(Chargeproject chargeproject,Model model) throws IOException {
        if(chargeprojectService.updateChargeproject(chargeproject)){
            model.addAttribute("msg","<script>alert(\"更新成功!\")</script>");
            return "redirect:/chargeproject/chargeManage";
        }
        model.addAttribute("msg","<script>alert(\"更新失败!\")</script>");
        model.addAttribute("chargeproject", chargeproject);
        return "hospital/charge-edit1";
    }

    //添加操作
    @RequestMapping("chargeSave")
    public String chargeSave(Model model){
       model.addAttribute("chargeproject",new Chargeproject());
        return "hospital/chargeAdd";

    }
    @RequestMapping("saveCharge")
    public String saveCharge(Model model,Chargeproject chargeproject) throws IOException {
        chargeproject.setCmdate(new Date());
        chargeproject.setCmdel(1);
        if(chargeprojectService.saveChargeproject(chargeproject)){
            model.addAttribute("msg","<script>alert(\"添加成功!\")</script>");
            return "redirect:/chargeproject/chargeManage";
        }
        model.addAttribute("msg","<script>alert(\"添加失败!\")</script>");
        model.addAttribute("chargeproject", chargeproject);
        return "hospital/chargeAdd";
    }

    //ajax验证项目名称是否存在
    @RequestMapping("/addCharge")
    @ResponseBody
    public boolean addCharge(String cmname){
         if(chargeprojectService.chargeprojectBycmName(cmname)){
             return  false;
         }
        return true;
    }

}
