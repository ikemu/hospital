package com.hospital.controller;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.hospital.pojo.Behospitalinfo;
import com.hospital.pojo.Hrcheckcharge;
import com.hospital.service.BehospitalinfoService;
import com.hospital.service.BehospitalinfoServiceImpl;
import com.hospital.service.HrcheckchargeService;
import com.hospital.service.HrcheckchargeServiceImpl;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class ExcelExport {


    @Autowired
    private HrcheckchargeService hrcheckchargeService;

    @Autowired
    private BehospitalinfoService behospitalinfoService;


    @RequestMapping("/exceldownload")
    public void downLoadExcelModel(HttpServletRequest request, HttpServletResponse response,String behpid) throws Exception {
        // 获取下载路劲
        String path ="C:/Users/Administrator/Desktop/";
        SimpleDateFormat sdf=new SimpleDateFormat("yyyyMMddHHmmssSSS");
        String midName=sdf.format(new Date());
        //生成的PDF文档名称
        String fileName =midName+"病历清单.xlsx";
        //创建Excel
        createExcel(path + fileName,behpid);
        File file = new File(path + fileName); // 根据文件路径获得File文件

        response.setContentType("application/pdf;charset=GBK");
        // 文件名
        response.setHeader("Content-Disposition",
                "attachment;filename=\"" + new String(fileName.getBytes(), "ISO8859-1") + "\"");
        response.setContentLength((int) file.length());
        byte[] buffer = new byte[4096];// 缓冲区
        BufferedOutputStream output = null;
        BufferedInputStream input = null;
        try {
            output = new BufferedOutputStream(response.getOutputStream());
            input = new BufferedInputStream(new FileInputStream(file));
            int n = -1;
            // 遍历，开始下载
            while ((n = input.read(buffer, 0, 4096)) > -1) {
                output.write(buffer, 0, n);
            }
            output.flush();
            response.flushBuffer();
        } catch (Exception e) {
            // 异常自己捕捉
        } finally {
            // 关闭流，不可少
            if (input != null)
                input.close();
            if (output != null)
                output.close();
        }
    }

    public void createExcel(String dest,String behpid) throws IOException {

        List<Hrcheckcharge> list = hrcheckchargeService.hrcheckchargeBybehpId(behpid);
        Hrcheckcharge hrcheckcharge = hrcheckchargeService.hrcheckchargeBycmMoney(behpid);
        Behospitalinfo behospitalinfo = behospitalinfoService.behospitalinfoBybeHpId(behpid);

        //拿押金减去总花费得到余额
        double cmmoneySum = hrcheckcharge.getCmmoney();//总花费
        double behpmoney = behospitalinfo.getBehpmoney();//押金
        double balance = behpmoney - cmmoneySum;//余额

        //1、创建工作簿
        XSSFWorkbook workbook=new XSSFWorkbook();
        int rowCount = 0; // 行数
        //2、新建工作表
        XSSFSheet sheet=workbook.createSheet("新建工作表1");
        String [] title={"病历号","姓名","收费项目","收费金额","收费日期"};
        XSSFRow smallRow = sheet.createRow(rowCount++);
        for (int i = 0; i < title.length; i++) {
            XSSFCell createCell = smallRow.createCell(i);
            createCell.setCellValue(title[i]);
        }

            for(int i=0;i<list.size();i++){
                Hrcheckcharge hrcheckcharges = list.get(i);
                XSSFRow  row=sheet.createRow(rowCount++);
                    XSSFCell cell11 = row.createCell(0);
                    cell11.setCellValue(hrcheckcharges.getBehpid());
                    XSSFCell cell12 = row.createCell(1);
                    cell12.setCellValue(hrcheckcharges.getBehpname());
                    XSSFCell cell13 = row.createCell(2);
                    cell13.setCellValue(hrcheckcharges.getCmname());
                    XSSFCell cell14 = row.createCell(3);
                    cell14.setCellValue(hrcheckcharges.getCmmoney());
                    XSSFCell cell15 = row.createCell(4);
                    cell15.setCellValue(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(hrcheckcharges.getCmdate()));
            }

            XSSFRow moneyRow = sheet.createRow(rowCount++);
            String [] moneytitle={"总金额","押金","余额"};
        for (int i = 0; i < moneytitle.length; i++) {
            XSSFCell moneyCell = moneyRow.createCell(i);
            moneyCell.setCellValue(moneytitle[i]);
        }

       XSSFRow rowMoeny=sheet.createRow(rowCount++);
        XSSFCell cell1=rowMoeny.createCell(0);
        cell1.setCellValue(cmmoneySum);
        XSSFCell cell2=rowMoeny.createCell(1);
        cell2.setCellValue(behpmoney);
        XSSFCell cell3=rowMoeny.createCell(2);
        cell3.setCellValue(balance);

        //6、保存工作簿
        FileOutputStream fileout=new FileOutputStream(dest);
        workbook.write(fileout);
        System.out.println("创建2007格式的Excel成功");
    }
}
