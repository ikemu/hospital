package com.hospital.controller;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hospital.pojo.Behospitalinfo;
import com.hospital.pojo.Chargeproject;
import com.hospital.pojo.Drug;
import com.hospital.pojo.Hrcheckcharge;
import com.hospital.service.*;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.Date;
import java.util.List;

@Controller
@RequestMapping("hrcheckcharge")
public class HrcheckchargeController {


    @Autowired
    private DrugService drugService;

    @Autowired
    private ChargeprojectService chargeprojectService;

    @Autowired
    private HrcheckchargeService hrcheckchargeService;

    @Autowired
    private BehospitalinfoService behospitalinfoService;

    //双条件模糊查询，查询全部
    @RequestMapping("checkCharge")
    public String checkCharge(Model model, Hrcheckcharge hrcheckcharge, @RequestParam(required = true, defaultValue = "1") int pageNum, @RequestParam(required = true, defaultValue = "10") int pageSize) {
        PageHelper.startPage(pageNum, pageSize);
        List<Hrcheckcharge> list = hrcheckchargeService.hrcheckchargeBybehpIdAndbehpName(hrcheckcharge);
        PageInfo<Hrcheckcharge> page = new PageInfo<>(list);
        model.addAttribute("page", page);
        model.addAttribute("hrcheckchargeList", list);
        if (hrcheckcharge == null) {
            model.addAttribute("hrcheckcharge", new Hrcheckcharge());
        }
        model.addAttribute("hrcheckcharge", hrcheckcharge);
        return "hospital/charge2";
    }

    //根据病历号查询详情
    @RequestMapping("checkChargedetails")
    public String checkChargedetails(Model model, String behpid, @RequestParam(required = true, defaultValue = "1") int pageNum, @RequestParam(required = true, defaultValue = "10") int pageSize) {
        PageHelper.startPage(pageNum, pageSize);
        List<Hrcheckcharge> list;
        Hrcheckcharge hrcheckcharge;
        Behospitalinfo behospitalinfo;
        //判断该病历号是否挂号
        if(behospitalinfoService.behospitalinfofindbeHpId(behpid)){
            list = hrcheckchargeService.hrcheckchargeBybehpId(behpid);
            hrcheckcharge = hrcheckchargeService.hrcheckchargeBycmMoney(behpid);
            behospitalinfo = behospitalinfoService.behospitalinfoBybeHpId(behpid);

            model.addAttribute("behospitalinfo", behospitalinfo);
            PageInfo<Hrcheckcharge> page = new PageInfo<>(list);
            model.addAttribute("page", page);
            model.addAttribute("hrcheckcharge", hrcheckcharge);
            model.addAttribute("hrcheckchargeList", list);
            //拿押金减去总花费得到余额
            double cmmoneySum = hrcheckcharge.getCmmoney();
            double behpmoney = behospitalinfo.getBehpmoney();
            double balance = behpmoney - cmmoneySum;
            model.addAttribute("balance", balance);
            return "hospital/accountnew";

        }
        model.addAttribute("msg","<script>alert(\"该病人未挂号,查询详情失败!\")</script>");
        return "redirect:/hrcheckcharge/checkCharge";

    }


    //添加收费项目
    @RequestMapping("checkChargeSave")
    public String checkChargeSave(Model model, String behpid) {
        if(behospitalinfoService.behospitalinfofindbeHpId(behpid)) {
            Hrcheckcharge hrcheckchargeOld = hrcheckchargeService.hrcheckchargefinbehpId(behpid);
            Hrcheckcharge hrcheckchargeNew = new Hrcheckcharge();
            hrcheckchargeNew.setBehpid(hrcheckchargeOld.getBehpid());
            hrcheckchargeNew.setBehpname(hrcheckchargeOld.getBehpname());
            model.addAttribute("hrcheckcharge", hrcheckchargeNew);
            return "hospital/charge-new";
        }
        model.addAttribute("msg","<script>alert(\"该病人未挂号,添加收费项目失败!\")</script>");
        return "redirect:/hrcheckcharge/checkCharge";

    }
    @RequestMapping("savecheckCharge")
    public String savecheckCharge(Model model, Hrcheckcharge hrcheckcharge) {
        hrcheckcharge.setCmdate(new Date());
        hrcheckcharge.setCmstate(1);
        hrcheckcharge.setHccdel(1);
        if (hrcheckchargeService.savehrcheckcharge(hrcheckcharge)) {
            model.addAttribute("msg", "添加成功！");
            return "redirect:/hrcheckcharge/checkCharge";
        }
        model.addAttribute("msg", "添加失败！请重新添加");
        model.addAttribute("hrcheckcharge", hrcheckcharge);
        return "hospital/charge-new";
    }


    //ajax验证收费项目是否存在并回显收费金额
    @RequestMapping("checkChargeVerifier")
    @ResponseBody
    public double checkChargeVerifier(String cmname) {
        Chargeproject chargeproject = null;
        Drug drug = null;
        if (cmname != null || cmname != "") {

            if (chargeprojectService.chargeprojectBycmName(cmname)) {
                chargeproject = chargeprojectService.chargeprojectfindcmName(cmname);
                double cmmoney = chargeproject.getCmmoney();
                return cmmoney;
            }
            if (drugService.druggetBydrugdName(cmname)) {
                drug = drugService.drugfindBydrugdname(cmname);
                double drugsaleprice = drug.getDrugsaleprice();
                return drugsaleprice;
            }
            return 0;
        }
        return 0;
    }

}



