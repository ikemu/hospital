package com.hospital.controller;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hospital.pojo.Hosregister;
import com.hospital.pojo.State;
import com.hospital.service.DepartService;
import com.hospital.service.DoctorService;
import com.hospital.service.RegistrationService;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletResponse;
import javax.swing.text.AbstractDocument;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Controller
@RequestMapping("/registration")
public class RegistrationController {
    @Autowired
    private RegistrationService registrationService;
    @Autowired
    private DoctorService doctorService;
    @Autowired
    private DepartService departService;
    @RequestMapping("/index")
    public String index(Model model,String behpid,String dname,String departname,String hosrfromdate,String hosrtodate, @RequestParam(required=true,defaultValue="1")int pageNum, @RequestParam(required=true,defaultValue="5")int pageSize) {
        PageHelper.startPage(pageNum, pageSize);
        List<Hosregister> list = registrationService.getHosregister(behpid,dname,departname,hosrfromdate,hosrtodate);
        model.addAttribute("behpid",behpid);
        model.addAttribute("dname",dname);
        model.addAttribute("departname",departname);
        model.addAttribute("hosrfromdate",hosrfromdate);
        model.addAttribute("hosrtodate",hosrtodate);
        PageInfo<Hosregister> page = new PageInfo<>(list);
        model.addAttribute("hlist",list);
        model.addAttribute("page", page);
        return "/registration/index";
    }
    @RequestMapping("/add")
    public String add(Model model){
        model.addAttribute("hosregister",new Hosregister());
        model.addAttribute("depart",departService.list());
        model.addAttribute("doctor",doctorService.list());
        return "/registration/add";
    }
    @RequestMapping("/addHosregister")
    public String addHosridcard(Model model,Hosregister hosregister){
        if (registrationService.saveHosregister(hosregister)){
            return "redirect:/registration/index";
        }
        model.addAttribute("hosregister",hosregister);
        return "/registration/add";
    }
    @RequestMapping("/edit")
    public String edit(Model model,int hosrid){
        model.addAttribute("depart",departService.list());
        model.addAttribute("doctor",doctorService.list());
        model.addAttribute("hosregister",registrationService.getHosregisterById(hosrid));
        return "/registration/edit";
    }
    @RequestMapping("/editHosregister")
    public String editHosridcard(Model model,Hosregister hosregister){
        if (registrationService.updateHosregister(hosregister)){
            return "redirect:/registration/index";
        }
        model.addAttribute("hosregister",hosregister);
        return "/registration/edit";
    }
    @RequestMapping("/look")
    public String look(Model model,int hosrid){
        model.addAttribute("hosregister",registrationService.getHosregisterById(hosrid));
        return "/registration/look";
    }
    @RequestMapping("/delRegistration")
    public  String delRegistration(int hosrid,Hosregister hosregister){
        hosregister.setHosrid(hosrid);
        hosregister.setHrStateId(3);
        registrationService.updateRegistration(hosregister);
        return "redirect:/registration/index";
    }
    @RequestMapping("/delRegistrations")
    public  void delRegistrations(Model model, int[] rids, Hosregister hosregister, HttpServletResponse response) throws IOException {
        response.setContentType("text/html;charset=utf-8");
        int count=0;
        for (int hosrid: rids) {
            int sate = registrationService.getSate(hosrid);
            if (sate==1){
                hosregister.setHosrid(hosrid);
                hosregister.setHrStateId(2);
                boolean flag=registrationService.updateRegistration(hosregister);
                if (flag == true) {
                    count++;
                }
            }else if(sate==2){
//                model.addAttribute("msg","<script>alert('用户已经退号，无法再次退号');location.href='/registration/index';</script>");
//                return  "/registration/index";
                response.getWriter().write("<script>alert('用户已经退号，无法再次退号);</script>");
            }else if(sate==3){
//                model.addAttribute("msg","<script>alert('用户已经住院院，无法再次退号');location.href='/registration/index';</script>");
//                return  "/registration/index";
                response.getWriter().write("<script>alert('用户已经住院院，无法再次退号');</script>");
            }else if(sate==4){
//                model.addAttribute("msg","<script>alert('用户已经退院，无法再次退号');location.href='/registration/index';</script>");
//                return  "/registration/index";
                response.getWriter().write("<script>alert('用户已经退院，无法再次退号');</script>");
            }else if(sate==5){
//                model.addAttribute("msg","<script>alert('用户已经出院，无法再次退号');location.href='/registration/index';</script>");
//                return  "/registration/index";
                response.getWriter().write("<script>alert('用户已经出院，无法再次退号');</script>");
            }else if(sate==6){
//                model.addAttribute("msg","<script>alert('用户已经出诊，无法再次退号');location.href='/registration/index';</script>");
//                return  "/registration/index";
                response.getWriter().write("<script>alert('用户已经出诊，无法再次退号');</script>");
            }
        }
        if (count > 0&&count!=rids.length) {
//            model.addAttribute("msg","<script>alert('门诊退号成功');location.href='/registration/index';</script>");
//            return  "/registration/index";
            response.getWriter().write("<script>alert('部分门诊退号成功');location.href='/registration/index';</script>");
        }else if(count==rids.length){
            response.getWriter().write("<script>alert('所选门诊退号成功');location.href='/registration/index';</script>");
        }else  {
           /* model.addAttribute("msg","<script>alert('门诊退号失败');location.href='/registration/index';</script>");
            return  "/registration/index";*/
            response.getWriter().write("<script>alert('门诊退号失败');location.href='/registration/index';</script>");
        }

    }

    @RequestMapping("/daoexcel")
    public String doExcel(int[] rids) throws IOException {
        List<Hosregister> hos = new ArrayList<Hosregister>();
        for(int i =0;i<rids.length;i++){
            Hosregister hosr = registrationService.getRoles(rids[i]);
            hos.add(hosr);
        }
        // 要导出的数据集合
        XSSFWorkbook wb = new XSSFWorkbook();
        // 2.创建工作表对象并命名
        XSSFSheet sheet = wb.createSheet("挂号信息表");
        int rowCount = 0; // 行数
        sheet.addMergedRegion(new CellRangeAddress(0, 0, 0, 5));
        XSSFRow rowTitle = sheet.createRow(rowCount++);
        XSSFCell cellTile = rowTitle.createCell(0);
        cellTile.setCellValue("挂号信息表");
        // 4. 处理标题
        String[] title = new String[] { "门诊编号", "主治医生", "挂号时间", "挂号科室", "状态" };
        XSSFRow smallRow = sheet.createRow(rowCount++);
        for (int i = 0; i < title.length; i++) {
            XSSFCell createCell = smallRow.createCell(i);
            createCell.setCellValue(title[i]);
        }
        for (int i = 0; i < hos.size(); i++) {
            Hosregister role = hos.get(i);
            // 创建行
            XSSFRow row = sheet.createRow(rowCount++);
            // 开始创建单元格并赋值
            XSSFCell hosRId = row.createCell(0);
            hosRId.setCellValue(role.getHosrid());

            XSSFCell hR_Doc_id = row.createCell(1);
            hR_Doc_id.setCellValue(role.getDoctor().getDname());

            XSSFCell hosRDate = row.createCell(2);
            hosRDate.setCellValue(new SimpleDateFormat("yyyy-MM-dd").format(role.getHosrdate()));

            XSSFCell hR_depart_id = row.createCell(3);
            hR_depart_id.setCellValue(role.getDepartment().getDepartname());

            XSSFCell hR_state_id = row.createCell(4);
            hR_state_id.setCellValue(role.getState().getStatename());
        }
        String filename = new Date().getTime()+"挂号信息表.xlsx";
        FileOutputStream out = new FileOutputStream("c:/EXcel/"+filename);
        wb.write(out);
        out.close();
        return "redirect:/registration/index";
    }

}
