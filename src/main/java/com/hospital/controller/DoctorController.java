package com.hospital.controller;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hospital.pojo.Doctor;
import com.hospital.pojo.Education;
import com.hospital.service.DepartService;
import com.hospital.service.DoctorService;
import com.hospital.service.EducationService;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Controller
@RequestMapping("/doctor")
public class DoctorController {
    @Autowired
    private DoctorService doctorService;
    @Autowired
    private DepartService departService;
    @Autowired
    private EducationService educationService;
    @RequestMapping("/index")
    public String index(Model model,Integer did,String dname,String departname,@RequestParam(required=true,defaultValue="1")int pageNum, @RequestParam(required=true,defaultValue="5")int pageSize) {
        PageHelper.startPage(pageNum, pageSize);
        List<Doctor> list = doctorService.getDoctor(did,dname,departname);
        model.addAttribute("did",did);
        model.addAttribute("dname",dname);
        model.addAttribute("departname",departname);
        PageInfo<Doctor> page = new PageInfo<>(list);
        model.addAttribute("dlist",list);
        model.addAttribute("page", page);
        return "/doctor/index";
    }
    @RequestMapping("/look")
    public String look(Model model,int did){
       model.addAttribute("doctor",doctorService.getDoctorById(did));
        return "/doctor/look";
    }
    @RequestMapping("/edit")
    public String edit(Model model,int did){
        model.addAttribute("doctor",doctorService.getDoctorById(did));
        model.addAttribute("education",educationService.list());
        model.addAttribute("depart",departService.list());
        return "/doctor/edit";
    }
    @RequestMapping("/editDoctor")
    public String editDoctor(Model model,Doctor doctor){
        if (doctorService.updateDoctor(doctor)){
            return "redirect:/doctor/index";
        }
        model.addAttribute("doctor",doctor);
        return "/doctor/edit";
    }

    @RequestMapping("/add")
    public String add(Model model){
        model.addAttribute("doctor",new Doctor());
        model.addAttribute("dlist",departService.list());
        model.addAttribute("elist",educationService.list());
        return "/doctor/add";
    }
    @RequestMapping("/addDoctor")
    public String addDoctor(Model model,Doctor doctor){
        if (doctorService.saveDoctor(doctor)){
            return "redirect:/doctor/index";
        }
        model.addAttribute("doctor",doctor);
        return "/doctor/add";
    }
    @RequestMapping("/daoexcel")
    public String doExcel(int[] rids) throws IOException {
        List<Doctor> dos=new ArrayList<>();
        for (int i=0;i<rids.length;i++) {
            Doctor doctor=doctorService.getDoctorById(rids[i]);
            dos.add(doctor);
        }
        // 要导出的数据集合
        XSSFWorkbook wb = new XSSFWorkbook();
        // 2.创建工作表对象并命名
        XSSFSheet sheet = wb.createSheet("门诊医生信息表");
        int rowCount = 0; // 行数
        sheet.addMergedRegion(new CellRangeAddress(0, 0, 0, 4));
        XSSFRow rowTitle = sheet.createRow(rowCount++);
        XSSFCell cellTile = rowTitle.createCell(0);
        cellTile.setCellValue("门诊医生信息表");
        // 4. 处理标题
        String[] title = new String[] { "医生编号", "医生姓名", "入职时间", "所属科室" };
        XSSFRow smallRow;
        smallRow = sheet.createRow(rowCount++);
        for (int i = 0;i < title.length; i++) {
            XSSFCell createCell = smallRow.createCell(i);
            createCell.setCellValue(title[i]);
        }
        for (int i = 0; i < dos.size(); i++) {
            Doctor role = dos.get(i);
            // 创建行
            XSSFRow row = sheet.createRow(rowCount++);
            // 开始创建单元格并赋值
            XSSFCell dtdId = row.createCell(0);
            dtdId.setCellValue(role.getDid());

            XSSFCell dtName = row.createCell(1);
            dtName.setCellValue(role.getDname());

            XSSFCell dtdInnerDate = row.createCell(2);
            dtdInnerDate.setCellValue(new SimpleDateFormat("yyyy-MM-dd").format(role.getDinnerdate()));

            XSSFCell dtd_depart_id = row.createCell(3);
            dtd_depart_id.setCellValue(role.getDepartment().getDepartname());

        }
        String filename = new Date().getTime()+"门诊医生信息表.xlsx";
        FileOutputStream out = new FileOutputStream("c:/EXcel/"+filename);
        wb.write(out);
        out.close();
        return "redirect:/doctor/index";
    }
}
