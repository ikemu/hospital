package com.hospital.controller;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hospital.pojo.*;
import com.hospital.service.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Create by Administrator on 2018/12/15 16:43
 * 	住院办理 住院结算前端控制器
 */

@Controller
@RequestMapping("/beh")
public class BehospitalinfoController {
	@Autowired
	private BehospitalinfoService behospitalinfoService;//住院
	@Autowired
	private HosregisterService hosregisterService;//挂号
	@Autowired
	private DepartService departService;//科室
	@Autowired
	private DoctorService doctorService;//医生
	@Autowired
	private PricepeopleService pricepeopleService;//收费/病人

	//访问住院办理页面 支持模糊查询===========================
	@RequestMapping("/hospital")
	public String hospital(Model model,String behpid,String dname,String departname,String hosrfromdate,String hosrtodate,@RequestParam(required=true,defaultValue="1") int pageNum,
						   @RequestParam(required=false,defaultValue="5") int pageSize){
		PageHelper.startPage(pageNum,pageSize);//设定分页条件
		List<Behospitalinfo> list = behospitalinfoService.hospLists(behpid,dname,departname,hosrfromdate,hosrtodate);
		PageInfo<Behospitalinfo> page = new PageInfo<>(list);//分页信息 总记录/总页数等
		model.addAttribute("behpid",behpid);
		model.addAttribute("dname",dname);
		model.addAttribute("departname",departname);
		model.addAttribute("hosrfromdate",hosrfromdate);
		model.addAttribute("hosrtodate",hosrtodate);
		model.addAttribute("hospList", list);
		model.addAttribute("page", page);
		return  "/hospital/index";
	}

	//查询住院办理详情
	@RequestMapping("/showhospital")
	public String showhospital(Model model,String beHpId){
		Hosregister byhpid = hosregisterService.getHosreByHpId(beHpId);
		model.addAttribute("detail", byhpid);
		return "/hospital/look";
	}

	//访问住院办理更改页面
	@RequestMapping("/upd")
	public String upd(Model model,String behpid){
		Hosregister hosreByHpId = hosregisterService.getHosreByHpId(behpid);
		List<Behospitalinfo> list1 = behospitalinfoService.list();//护理人/床位号下拉列表
		List<Department> list = departService.list();//科室下拉列表
		List<Doctor> list2 = doctorService.list();//医生下拉列表
		model.addAttribute("info", hosreByHpId);
		model.addAttribute("depart", list);
		model.addAttribute("nurse", list1);
		model.addAttribute("doctor", list2);
		return "/hospital/edit";
	}

	//执行更新
	@RequestMapping("/doUpdate")
	public String doUpdate(Model model,Hosregister hosregister){
		if(hosregisterService.doUpdate(hosregister)){//更新成功
			model.addAttribute("msg", "操作成功！");
			return "/beh/hospital";
		}
		//更新失败
		model.addAttribute("msg", "操作失败！");
		model.addAttribute("hosre", hosregister);
		return "/hospital/edit";
	}

	//访问添加住院信息页面
	@RequestMapping("/goAdd")
	public String goAdd(Model model){
		List<Behospitalinfo> list1 = behospitalinfoService.list();//护理人/床位号下拉列表
		List<Department> list = departService.list();//科室下拉列表
		List<Doctor> list2 = doctorService.list();//医生下拉列表
		model.addAttribute("nurse", list1);
		model.addAttribute("depart", list);
		model.addAttribute("doctor", list2);
		Hosregister hosr = hosregisterService.getHosreByHpId("2016101100001");//初始化
		model.addAttribute("aja", hosr);
		return "/hospital/add";
	}

	//通过填写病历号自动获取信息
	@RequestMapping("/doAjax")
	public String doAjax(Model model,String behpid){
		List<Behospitalinfo> list1 = behospitalinfoService.list();//护理人/床位号下拉列表
		List<Department> list;
		list = departService.list();//科室下拉列表
		List<Doctor> list2 = doctorService.list();//医生下拉列表
		model.addAttribute("nurse", list1);
		model.addAttribute("depart", list);
		model.addAttribute("doctor", list2);
		Hosregister hosre = hosregisterService.getHosreByHpId(behpid);
		if(hosre==null){
			model.addAttribute("msg", "没有此病历号！");
			return "/hospital/add";
		}
		model.addAttribute("msg", "获取成功！");
		model.addAttribute("aja", hosre);
		return "/hospital/add";
	}

	//实现添加住院信息
	@RequestMapping("doAdd")
	public String doAdd(Model model,String bephid,Behospitalinfo behospitalinfo){
		Behospitalinfo gethos = behospitalinfoService.gethosByHpId(bephid);
		behospitalinfo.setBehpmoney(gethos.getBehpmoney() + behospitalinfo.getBehpmoney());
		behospitalinfo.setBhid(null);//主键自增
		behospitalinfo.setBehpid(gethos.getBehpid());//设置住院病历号
		behospitalinfo.setBehpname(gethos.getBehpname());//设置住院病人姓名
		behospitalinfo.setHehpDepartId(gethos.getHehpDepartId());//设置住院科室
		behospitalinfo.setBehpdate(new Date());//住院时间
		behospitalinfo.setBehpDocId(gethos.getBehpDocId());//设置住院医生编号
		behospitalinfo.setBehpStateId(gethos.getState().getStateid());//设置住院状态编号
		behospitalinfo.setHehpdel(0);//默认未结算
		if(behospitalinfoService.doAdd(behospitalinfo)){
			model.addAttribute("msg", "添加成功！");
			return "/beh/hospital";
		}
		model.addAttribute("msg", "添加失败！");
		return "/beh/goAdd";
	}

	//访问缴纳押金页面
	@RequestMapping("/goAddMany")
	public String goAddMany(Model model,String behpid){
		Hosregister byHpId = hosregisterService.getHosreByHpId(behpid);
		model.addAttribute("many", byHpId);
		return "/hospital/add_many";
	}

	//执行缴纳押金
	@RequestMapping("doAddMany")
	public String doAddMany(Model model,String behpid,Behospitalinfo behospitalinfo){
		Behospitalinfo gethos = behospitalinfoService.gethosByHpId(behpid);
		behospitalinfo.setBehpmoney(gethos.getBehpmoney()+behospitalinfo.getBehpmoney());//原押金+缴纳的押金=新押金
		if(behospitalinfoService.doUpdate(behospitalinfo)){
			return "/beh/hospital";
		}
		model.addAttribute("many", behospitalinfo);
		return "/beh/goAddMany";
	}
	//退院
	@RequestMapping("/doout")
	public String doout(Model model,String behpid){
		Behospitalinfo gethos = behospitalinfoService.gethosByHpId(behpid);
		gethos.setHehpdel(0);
		if(behospitalinfoService.doUpdate(gethos)){
			model.addAttribute("msg", "操作成功！");
			return "redirect:/";
		}
		model.addAttribute("msg", "操作失败！");
		return "/beh/hospital";
	}

	//访问住院结算页面 支持模糊查询============================
	@RequestMapping("/account")
	public String account(Model model,String behpid,String behpname,@RequestParam(required=true,defaultValue="1") int pageNum,
						  @RequestParam(required=false,defaultValue="5") int pageSize){
		PageHelper.startPage(pageNum,pageSize);//设定分页条件
		List<Pricepeople> list = pricepeopleService.pricepeoLists(behpid,behpname);
		PageInfo<Pricepeople> page = new PageInfo<>(list);
		model.addAttribute("list", list);
		model.addAttribute("page", page);
		return "/hospital/account";
	}

	//查看住院结算详情
	@RequestMapping("/showaccount")
	public String show(Model model,String beHpId){
		List<Pricepeople> list = pricepeopleService.getPricepeoByHpId(beHpId);
		Double cost = pricepeopleService.getCostByHpId(beHpId);
		Behospitalinfo yajin = behospitalinfoService.gethosByHpId(beHpId);
		model.addAttribute("showList", list);
		model.addAttribute("cost", cost);
		model.addAttribute("yajin", yajin);
		return "/hospital/account-look";
	}

	//结算
	@RequestMapping("/docount")
	public String docount(Model model,String behpid){
		Behospitalinfo byHpId = behospitalinfoService.gethosByHpId(behpid);
		byHpId.setHehpdel(1);
		behospitalinfoService.doUpdate(byHpId);
		model.addAttribute("msg", "操作成功！");
		return "/beh/account";
	}

	//批量/单个退院
	@RequestMapping("/delBehospital")
	public String delBehospital(Model model,String[] rids,Behospitalinfo behospitalinfo){
		int count=0;
		for (String hosrid: rids) {
			Behospitalinfo gethos = behospitalinfoService.gethosByHpId(hosrid);
			Integer sate = gethos.getState().getStateid();
			if (sate==7){
				behospitalinfo.setHehpdel(0);
				boolean flag = behospitalinfoService.doUpdate(behospitalinfo);
				if (flag == true) {
					count++;
				}
			}else{
				model.addAttribute("msg","该状态不能退院或者出院！");
				return  "/beh/hospital";
			}
		}
		//批量删除了给提示
		if (count == rids.length) { model.addAttribute("msg","操作成功！");return  "/beh/hospital"; }
		//失败给提示
		model.addAttribute("msg","操作失败！");
		return  "/beh/hospital";
	}

	//导出excel
	@RequestMapping("/doexcel")
	public String doExcel(Model model,String[] rids) throws IOException {
		List<Behospitalinfo> hos = new ArrayList<>();
		for(int i =0;i<rids.length;i++){
			Behospitalinfo gethos = behospitalinfoService.gethosByHpId(rids[i]);//根据病历号获取住院办理对象信息
			hos.add(gethos);//保存到List集合中
		}
		// 要导出的数据集合
		XSSFWorkbook wb = new XSSFWorkbook();
		// 2.创建工作表对象并命名
		XSSFSheet sheet = wb.createSheet("住院办理信息表");
		int rowCount = 0; // 行数
		sheet.addMergedRegion(new CellRangeAddress(0, 0, 0, 9));
		XSSFRow rowTitle = sheet.createRow(rowCount++);
		XSSFCell cellTile = rowTitle.createCell(0);
		cellTile.setCellValue("住院办理信息表");
		// 4. 处理标题
		String[] title = new String[] { "病历号", "姓名", "床位号", "联系电话", "押金", "主治医生", "入院时间", "科室", "状态"};
		XSSFRow smallRow = sheet.createRow(rowCount++);
		for (int i = 0; i < title.length; i++) {
			XSSFCell createCell = smallRow.createCell(i);
			createCell.setCellValue(title[i]);
		}
		for (int i = 0; i < hos.size(); i++) {
			Behospitalinfo role = hos.get(i);
			// 创建行
			XSSFRow row = sheet.createRow(rowCount++);
			// 开始创建单元格并赋值
			XSSFCell hpid = row.createCell(0);
			hpid.setCellValue(role.getBehpid());//病历号
			XSSFCell name = row.createCell(1);
			name.setCellValue(role.getBehpname());//姓名
			XSSFCell bed = row.createCell(2);
			bed.setCellValue(role.getBehpbed());//床位号
			XSSFCell phone = row.createCell(3);
			phone.setCellValue(role.getHosregister().getHosrphone());//联系电话
			XSSFCell money = row.createCell(4);
			money.setCellValue(role.getBehpmoney());//押金
			XSSFCell hR_state_id = row.createCell(5);
			hR_state_id.setCellValue(role.getDoctor().getDname());//主治医生
			XSSFCell date = row.createCell(6);
			date.setCellValue(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(role.getBehpdate()));//入院时间
			XSSFCell depart = row.createCell(7);
			depart.setCellValue(role.getDepartment().getDepartname());//科室
			XSSFCell state = row.createCell(8);
			state.setCellValue(role.getState().getStatename());//状态
		}
		String filename = new Date().getTime()+"住院办理信息表.xlsx";
		FileOutputStream out = new FileOutputStream("c:/"+filename);
		wb.write(out);
		out.close();
		model.addAttribute("msg", "导出成功！");
		return "/beh/hospital";
	}

}
