package com.hospital.controller;


import com.hospital.mapper.UsersMapper;
import com.hospital.pojo.Role;
import com.hospital.pojo.Users;
import com.hospital.service.RoleService;
import com.hospital.service.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@Controller
@RequestMapping("login")
public class LoginController {

    @Autowired
    private UsersService usersService;

    @Autowired
    private RoleService  roleService;

    //账户登录
    @RequestMapping
    public String login(Model model){
        model.addAttribute("user",new Users());
        return "login";
    }
    @RequestMapping("/logingo")
    public String logingo(Model model, Users user, HttpSession session) throws IOException {

        Role role = roleService.roleByroleId(usersService.getByUsername(user.getUsername()).getUserRoleId());
        Users  userNew  =usersService.getByUsername(user.getUsername());
        if(usersService.login(user)&role.getRolestate()==1&userNew.getUserstate()==1){
           session.setAttribute("USER_SESSION",userNew);
           return "redirect:/";
       }else if(role.getRolestate()==0){
            model.addAttribute("msg", "<script>alert(\"该角色已被禁用，请联系管理员!\")</script>");
            model.addAttribute("user", user);
            return "login";
       }else if(userNew.getUserstate()==0) {
            model.addAttribute("msg", "<script>alert(\"该用户已被禁用，请联系管理员!\")</script>");
            model.addAttribute("user", user);
            return "login";
        } else {
            model.addAttribute("msg", "<script>alert(\"用户名或密码输入不正确，请重新输入!\")</script>");
            model.addAttribute("user", user);
            return "login";
        }
    }

    //ajax验证
    @RequestMapping("/userByuserName")
    @ResponseBody
    public boolean userByuserName(String username){
       return usersService.userByuserName(username);

    }

}
