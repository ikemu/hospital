package com.hospital.controller;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hospital.pojo.Users;
import com.hospital.service.RoleService;
import com.hospital.service.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

/**
 * Created by Administrator on 2018/12/17.
 */
@Controller
@RequestMapping("/user")
public class UserController {
	@Autowired
	private UsersService usersService;
	@Autowired
	private RoleService roleService;

	@RequestMapping("/list")
	public String list(Model model, @RequestParam(required = true, defaultValue = "1") int pageNum, @RequestParam(required = true, defaultValue = "5") int pageSize) {
		PageHelper.startPage(pageNum, pageSize);
		List<Users> usersList = usersService.list();
		PageInfo<Users> page = new PageInfo<>(usersList);
		model.addAttribute("usersList", usersList);
		/*model.addAttribute("users",usersService.getById(userid));*/
		model.addAttribute("page", page);
		return "/user/userList";
	}

	//查询
	@RequestMapping("/listByUsername")
	public String listByUsername(Model model, String name, @RequestParam(required = true, defaultValue = "1") int pageNum, @RequestParam(required = true, defaultValue = "5") int pageSize) {
		PageHelper.startPage(pageNum, pageSize);
		PageHelper.startPage(pageNum, pageSize);
		List<Users> users = usersService.listByUsername(name);
		PageInfo<Users> page = new PageInfo<>(users);

		model.addAttribute("usersList", users);
		model.addAttribute("page", page);
		return "/user/userList";

	}

	/*添加用户*/
	@RequestMapping("/add")
	public String add(Model model) {
		model.addAttribute("rlist", roleService.list());
		return "/user/addUser";
	}

	/*保存添加到用户*/
	@RequestMapping("/addUser")
	public String addUser(Model model, Users users) {
		if (usersService.add(users)) {
			/* System.out.print("111111111111111111");*/
			return "redirect:/user/list";
		}
		model.addAttribute("users", users);
		return "/user/addUser";
	}

	/*跳转到更新页面*/
	@RequestMapping("/update")
	public String update(Model model, Integer userid) {
		model.addAttribute("rlist", roleService.list());
		model.addAttribute("users", usersService.getById(userid));
		return "/user/editUser";
	}

	/*保存更新用户信息*/
	@RequestMapping("/updateUser")
	public String updateUser(Model model, Users users) {
		if (usersService.update(users)) {
			return "redirect:/user/list";
		}
		model.addAttribute("users", users);
		return "/user/editUser";
	}

	/*删除用户*/
	@RequestMapping("/remove")
	public String remove(Integer userid) {
		usersService.remove(userid);
		return "redirect:/user/list";
	}

	/*多用户删除*/
	@RequestMapping("/removes")
	public String removes(int[] rids) {
		for (int userid : rids) {
			usersService.remove(userid);
		}
		return "redirect:/user/list";
	}

	//访问修改密码页面
	@RequestMapping("/password")
	public String password(Model model, HttpSession session) {
		Users users = (Users) session.getAttribute("USER_SESSION");
		model.addAttribute("user", users);
		return "User/password";
	}

	//修改密码
	@RequestMapping("/updatePassword")
	public String updatePassword(Model model, HttpSession session, HttpServletResponse response, String oldPassword, String newPassword) throws IOException {
		response.setCharacterEncoding("utf-8");
		PrintWriter writer = response.getWriter();
		Users users = (Users) session.getAttribute("USER_SESSION");
		if (users.getPassword().equals(oldPassword)) {//旧密码和登录对象的密码一致则执行以下操作
			if (users.getPassword().equals(newPassword)) {//旧密码和密码相同不实现修改
				model.addAttribute("msg", "旧密码不能和新密码相同！");
				return "User/password";
			}
			users.setPassword(newPassword);//否则 把新的密码更新到当前对象
			if (usersService.updateUsers(users)) {//更新成功 退出系统
				writer.print("<script>alert('密码修改成功，请重新登录！');parent.location.href=\"/login\";</script>");
				return null;
			}
		}
		//旧密码和登录对象的密码不一致则执行以下操作
		model.addAttribute("msg", "原密码不正确！");
		return "User/password";
	}
}

