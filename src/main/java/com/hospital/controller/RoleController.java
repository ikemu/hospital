package com.hospital.controller;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hospital.pojo.Role;
import com.hospital.service.MenuService;
import com.hospital.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Controller
@RequestMapping("/role")
public class RoleController {
    @Autowired
    private RoleService roleService;
    @Autowired
    private MenuService menuService;
    @RequestMapping("/list")
    public String list(Model model, @RequestParam(required = true,defaultValue = "1")int pageNum,@RequestParam(required = true,defaultValue ="5" )int pageSize){
        PageHelper.startPage(pageNum,pageSize);
        List<Role> roleList=roleService.list();
        PageInfo<Role> page=new PageInfo<>(roleList);
        model.addAttribute("roleList",roleList);
        model.addAttribute("page",page);
        return  "/role/roleList";
    }
    /*跳转到更新页面*/
    @RequestMapping("/update")
    public String update(Model model,Integer roleid){
        model.addAttribute("role",roleService.getById(roleid));
        model.addAttribute("mlist",menuService.list());
        return  "/role/editRole";
    }
    /*保存更新*/
    @RequestMapping("/updateRole")
    public String updateRole(Model model,Role role){
        if(roleService.update(role)){
            return  "redirect:/role/list";
        }
        model.addAttribute("role",role);
        return  "/role/editRole";
    }

    /*跳转到添加页面*/
    @RequestMapping("/add")
    public String add(Model model){
        model.addAttribute("mlist",menuService.list());
        return  "/role/addRole";
    }
    /*添加角色*/
    @RequestMapping("/addeRole")
    public String addeRole(Model model,Role role){
        if(roleService.add(role)){
            return  "redirect:/role/list";
        }
        model.addAttribute("role",role);
        return  "/role/addRole";
    }
    /*删除角色*/
    @RequestMapping("/remove")
    public String remove(Integer roleid){
        roleService.remove(roleid);
        return  "redirect:/role/list";
    }

}
