package com.hospital.controller;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hospital.pojo.Menu;
import com.hospital.service.MenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Controller
@RequestMapping("/menu")
public class MenuController {
    @Autowired
    private MenuService menuService;

    @RequestMapping("/list")
    public String list(Model model,@RequestParam(required=true,defaultValue="1")int pageNum, @RequestParam(required=true,defaultValue="5")int pageSize) {
        PageHelper.startPage(pageNum, pageSize);
        List<Menu> menuList=menuService.list();
        PageInfo<Menu> page = new PageInfo<>(menuList);
        model.addAttribute("menuList",menuList);
        model.addAttribute("page",page);
        return  "/resource/resourceList";
    }
    //查询
    @RequestMapping("/listByName")
    public String listByUsername(Model model,String menuname,@RequestParam(required=true,defaultValue="1")int pageNum, @RequestParam(required=true,defaultValue="5")int pageSize) {
        PageHelper.startPage(pageNum, pageSize);
        List<Menu> menus= menuService.listByName(menuname);
        PageInfo<Menu> page = new PageInfo<>(menus);

        model.addAttribute("menuList",menus);
        model.addAttribute("page",page);
        return  "/resource/resourceList";

    }
    /*添加用户*/
    @RequestMapping("/add")
    public String add(Model model){

        return  "/resource/addResource";
    }
    /*保存添加到用户*/
    @RequestMapping("/addResource")
    public String addResource(Model model,Menu menu){
        if(menuService.add(menu)){

            return  "redirect:/menu/list";
        }
        model.addAttribute("menu",menu);
        return  "/resource/addResource";
    }
    /*跳转到更新页面*/
    @RequestMapping("/update")
    public String update(Model model,Integer menuid){
        /*model.addAttribute("rlist",roleService.list());*/
        model.addAttribute("users",menuService.getById(menuid));
        return "/resource/editResource";
    }
    /*保存更新用户信息*/
    @RequestMapping("/editResource")
    public String editResource(Model model,Menu menu){
        if (menuService.update(menu)){
            return  "redirect:/menu/list";
        }
        model.addAttribute("menu",menu);
        return "/resource/editResource";
    }

    /*删除用户*/
    @RequestMapping("/remove")
    public String remove(Integer menuid){
        menuService.remove(menuid);
        return  "redirect:/menu/list";
    }
    /*多用户删除*/
    @RequestMapping("/removes")
    public String removes(int[] rids){
        for (int menuid:rids ){
            menuService.remove(menuid);
        }
        return  "redirect:/menu/list";
    }
}
