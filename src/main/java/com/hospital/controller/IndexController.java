package com.hospital.controller;

import com.hospital.pojo.Menu;
import com.hospital.pojo.Users;
import com.hospital.service.MenuService;
import com.hospital.service.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * Create by Administrator on 2018/12/13 20:28
 */

@Controller
@RequestMapping
public class IndexController {

	@Autowired
	private UsersService usersService;

    @Autowired
	private MenuService MenuService;

	@RequestMapping
	public String index(Model model,HttpSession session){
		Users user = (Users) session.getAttribute("USER_SESSION");
		List<Menu> menuList = MenuService.MenuByroleId(user.getUserRoleId());
		model.addAttribute("name", user.getName());
        model.addAttribute("menuList",menuList);
		return "index";
	}
	//退出登录
	@RequestMapping("loginOut")
	public String loginOut(Model model,HttpSession session){
		session.setAttribute("USER_SESSION",null);
		return "redirect:/login";
	}


}
