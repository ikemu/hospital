package com.hospital.controller;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.hospital.util.VerificationCode;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/code")
public class CodeController {

    @RequestMapping("/verificationCode.jpg")
    public void createCode(HttpServletRequest request, HttpServletResponse response) {
        VerificationCode.getVerificationCode(request, response);
    }
    //验证
    @RequestMapping("/checkCode")
    @ResponseBody
    public boolean checkCode(String userCode,HttpSession session) {
        String sysCo=(String) session.getAttribute("CODE");
        if(sysCo.equalsIgnoreCase(userCode)) {
            return true;
        }
        return false;
    }
}