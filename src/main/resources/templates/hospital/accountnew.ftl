<!DOCTYPE html>
<html>
<head>
    <title>结算详细--中软高科-2015</title>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="/static/Css/bootstrap.css" />
    <link rel="stylesheet" type="text/css" href="/static/Css/bootstrap-responsive.css" />
    <link rel="stylesheet" type="text/css" href="/static/Css/style.css" />
    <script type="text/javascript" src="/static/Js/jquery.js"></script>
    <script type="text/javascript" src="/static/Js/jquery.sorted.js"></script>
    <script type="text/javascript" src="/static/Js/bootstrap.js"></script>
    <script type="text/javascript" src="/static/Js/ckform.js"></script>
    <script type="text/javascript" src="/static/Js/common.js"></script>

    <style type="text/css">
        body {
            padding-bottom: 40px;
        }
        .sidebar-nav {
            padding: 9px 0;
        }

        @media (max-width: 980px) {
            /* Enable use of floated navbar text */
            .navbar-text.pull-right {
                float: none;
                padding-left: 5px;
                padding-right: 5px;
            }
        }


    </style>
    <script type="text/javascript">
        $(function () {
            $('#newNav').click(function(){
                window.location.href="add.html";
            });
        });

        function checkall(){
            var alls=document.getElementsByName("check");
            var ch=document.getElementById("checkall");
            if(ch.checked){
                for(var i=0;i<alls.length;i++){
                    alls[i].checked=true;
                }
            }else{
                for(var i=0;i<alls.length;i++){
                    alls[i].checked=false;
                }
            }
        }
        function delAll(){
            var alls=document.getElementsByName("check");
            var ids=new Array();
            for(var i=0;i<alls.length;i++){
                if(alls[i].checked){
                    ids.push(alls[i].value);
                }
            }
            if(ids.length>0){
                if(confirm("确认操作?")){
                    alert("成功!");
                }
            }else{
                alert("请选中要操作的项");
            }
        }
        $(function () {
            $('#backid').click(function(){
                window.location.href="account.html";
            });
        });



        $(function () {
            $('#but').click(function () {
                var  behpid=$('#inp').val();
                window.location.href="/exceldownload?behpid="+behpid;
            });
        });

    </script>
</head>
<body>

<table class="table table-bordered table-hover definewidth m10" >
    <thead>
    <tr>
        <th>病历号</th>
        <th>姓名</th>
        <th>收费项目</th>
        <th>收费金额</th>
        <th>收费日期</th>
    </tr>
    </thead>
    <#if hrcheckchargeList??>
        <#list  hrcheckchargeList as hrcheckcharge>
            <tr >
            <td style="vertical-align:middle;"><#if hrcheckcharge.behpid??>${hrcheckcharge.behpid}<#else>该病人未挂号</#if></td>
            <td style="vertical-align:middle;"><#if hrcheckcharge.behpname??>${hrcheckcharge.behpname}<#else>无</#if></td>
            <td style="vertical-align:middle;"><#if hrcheckcharge.cmname??>${hrcheckcharge.cmname}<#else>无</#if></td>
            <td style="vertical-align:middle;"><#if hrcheckcharge.cmmoney??>${hrcheckcharge.cmmoney}<#else>无</#if></td>
            <td style="vertical-align:middle;"><#if hrcheckcharge.cmdate??>${hrcheckcharge.cmdate?string("yyyy-MM-dd HH:mm:ss")}<#else>无</#if></td>

            </tr>
        </#list>
    </#if>
</table>

<table class="table table-bordered table-hover definewidth m10" >
    <tr><th colspan="5">  <div class="inline pull-right page">
                <#if page.pageNum!=1>
                    <a href='/hrcheckcharge/checkChargedetails?pageNum=1' >首页</a>
                    <a href="/hrcheckcharge/checkCharge?pageNum=${page.pageNum-1}">上一页</a>
                <#else >
                    <a href="/hrcheckcharge/checkChargedetails?pageNum1">首页</a>
                </#if>
                <#if page.pages lte 5 >
                    <#list 1..page.pages as i>
                        <a <#if page.pageNum==i>class='current' </#if> href="/hrcheckcharge/checkChargedetails?pageNum=${i}" >${i}</a>
                    </#list>
                </#if>
                <#if page.pageNum lte 3 && page.pages gte 5>
                    <#list 1..5 as i>
                        <a <#if page.pageNum==i>class='current'</#if>  href="/hrcheckcharge/checkChargedetails?pageNum=${i}" >${i}</a>
                    </#list>
                </#if>
                <#if page.pageNum gt 3 && page.pageNum+2 lte page.pages && page.pages gte 5>
                    <#list page.pageNum-2..page.pageNum+2 as i>
                        <a <#if page.pageNum==i>class='current'</#if> href="/hrcheckcharge/checkChargedetails?pageNum=${i}"  >${i}</a>
                    </#list>
                </#if>
                <#if  page.pageNum gt 3 && page.pageNum+2 gt page.pages && page.pages gte 5>
                    <#list page.pages-4..page.pages as i>
                        <a <#if page.pageNum==i>class='current'</#if> href="/hrcheckcharge/checkChargedetails?pageNum=${i}"  >${i}</a>
                    </#list>
                </#if>

                <#if page.pageNum!=page.pages>
                    <a href='/hrcheckcharge/checkChargedetails?pageNum=${page.pageNum+1}' >下一页</a>
                    <a href="/hrcheckcharge/checkChargedetails?pageNum=${page.pages}">尾页</a>
                <#else >
                    <a href="/hrcheckcharge/checkChargedetails?pageNum=${page.pages}">尾页</a>
                </#if>
                &nbsp;&nbsp;&nbsp;共<span class='current'>${page.total}</span>条记录<span class='current'> ${page.pageNum}/${page.pages} </span>页

            </div>
            <div>
                <button type="button" class="btn btn-success" onclick="history.back(-1)">返回列表</button>
                <button type="button" class="btn btn-success"  id="but">导出excel</button>
            </div>

        </th></tr>
</table>


<table class="table table-bordered table-hover definewidth m10">
    <input  type="hidden" id="inp" name="behpid" value="<#if hrcheckcharge.behpid??>${hrcheckcharge.behpid}</#if>">
    <tr>
        <td width="10%" class="tableleft">总花费：</td>
        <td><#if hrcheckcharge.cmmoney??>${hrcheckcharge.cmmoney}元<#else>无</#if></td>
    </tr>
    <tr>
        <td width="10%" class="tableleft">押金：</td>
        <td><#if behospitalinfo.behpmoney??>${behospitalinfo.behpmoney}元<#else>无</#if></td>
    </tr>
    <tr>
        <td width="10%" class="tableleft">余额：</td>
        <td><#if balance??>${balance}元<#else>无</#if></td>
    </tr>
</table>
</body>
<#if msg??>${msg}<#else></#if>

</html>
