<!DOCTYPE html>
<html>
<head>
    <title>查看--中软高科-2015</title>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="/static/Css/bootstrap.css" />
    <link rel="stylesheet" type="text/css" href="/static/Css/bootstrap-responsive.css" />
    <link rel="stylesheet" type="text/css" href="/static/Css/style.css" />
    <script type="text/javascript" src="/static/Js/jquery.js"></script>
    <script type="text/javascript" src="/static/Js/jquery.sorted.js"></script>
    <script type="text/javascript" src="/static/Js/bootstrap.js"></script>
    <script type="text/javascript" src="/static/Js/ckform.js"></script>
    <script type="text/javascript" src="/static/Js/common.js"></script>
    <script type="text/javascript" src="/static/Js/ckeditor/ckeditor.js"></script>
 
    <style type="text/css">
        body {
            padding-bottom: 40px;
        }
        .sidebar-nav {
            padding: 9px 0;
        }

        @media (max-width: 980px) {
            /* Enable use of floated navbar text */
            .navbar-text.pull-right {
                float: none;
                padding-left: 5px;
                padding-right: 5px;
            }
        }


    </style>
    <script type="text/javascript">
    $(function () {       
		$('#backid').click(function(){
				window.location.href="index.html";
		 });
    });
    </script>
</head>
<body>
<form action="index.html" method="post" class="definewidth m20">
<table class="table table-bordered table-hover definewidth m10">
    <tr>
        <td width="10%" class="tableleft">姓名</td>
        <td><#if detail.hosrname??>${detail.hosrname}</#if></td><#--<#if ??></#if>-->
    </tr>
    <tr>
        <td width="10%" class="tableleft">证件类型</td>
        <td>身份证</td>
    </tr>
    <tr>
        <td width="10%" class="tableleft">证件号</td>
        <td><#if detail.hosridcard??>${detail.hosridcard}</#if></td>
    </tr>
    <tr>
        <td width="10%" class="tableleft">社保号</td>
        <td><#if detail.hosrssnum??>${detail.hosrssnum}</#if></td>
    </tr>
    <tr>
        <td width="10%" class="tableleft">联系电话</td>
        <td><#if detail.hosrphone??>${detail.hosrphone}</#if></td>
    </tr>
    <tr>
        <td width="10%" class="tableleft">是否自费</td>
        <td><#if detail.hosrvisit==1>是</#if><#if detail.hosrvisit==0>否</#if></td>
    </tr>
    <tr>
        <td width="10%" class="tableleft">性别</td>
        <td><#if detail.hosrsex==1>男<#else>女</#if></td>
    </tr>
    <tr>
        <td width="10%" class="tableleft">年龄</td>
        <td><#if detail.hosrage??>${detail.hosrage}</#if></td>
    </tr>
    <tr>
        <td width="10%" class="tableleft">职业</td>
        <td><#if detail.hosrjob??>${detail.hosrjob}</#if></td>
    </tr>
    <tr>
        <td width="10%" class="tableleft">初复诊</td>
        <td><#if detail.hosrLookdoctor==0>初诊<#else>复诊</#if></td>
    </tr>
    <tr>
        <td width="10%" class="tableleft">所挂科室</td>
        <td><#if detail.department.departname??>${detail.department.departname}</#if></td>
    </tr>
    <tr>
        <td width="10%" class="tableleft">指定医生</td>
        <td><#if detail.doctor.dname??>${detail.doctor.dname}</#if></td>
    </tr>
	 <tr>
        <td width="10%" class="tableleft">押金余额</td>
        <td><#if detail.behospitalinfo.behpmoney??>${detail.behospitalinfo.behpmoney}元</#if></td>
    </tr>
	
	<tr>
        <td width="10%" class="tableleft">备注</td>
        <td><#if detail.hosrremark??>${detail.hosrremark}</#if></td>
	</tr>
    <tr>
        <td colspan="2">
			<center>
				<button type="button" onclick="history.back(-1)" class="btn btn-success">返回列表</button><#-- name="backid" id="backid"-->
			</center>
		</td>
    </tr>
</table>
</form>
</body>
</html>