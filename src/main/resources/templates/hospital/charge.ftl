<!DOCTYPE html>
<html>
<head>
    <title>收费项目登记</title>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="/static/Css/bootstrap.css" />
    <link rel="stylesheet" type="text/css" href="/static/Css/bootstrap-responsive.css" />
    <link rel="stylesheet" type="text/css" href="/static/Css/style.css" />






    <script type="text/javascript" src="/static/Js/jquery.js"></script>
   <script type="text/javascript" src="/static/Js/jquery.sorted.js"></script>
    <script type="text/javascript" src="/static/Js/bootstrap.js"></script>
    <script type="text/javascript" src="/static/Js/ckform.js"></script>
    <script type="text/javascript" src="/static/Js/common.js"></script>




    <style type="text/css">

        body {
            padding-bottom: 40px;
        }
        .sidebar-nav {
            padding: 9px 0;
        }

        @media (max-width: 980px) {
            /* Enable use of floated navbar text */
            .navbar-text.pull-right {
                float: none;
                padding-left: 5px;
                padding-right: 5px;
            }
        }


    </style>
    <script type="text/javascript">

        window.onload=function () {//页面加载完之后触发事件执行函数


        $('#but').click(function (){
            $("#inp").attr("value","");
            window.location.href="/chargeproject/chargeManage";
        });




		$('#newNav').click(function(){
				window.location.href="/chargeproject/chargeSave";
		 });


    	function checkall(){
			var alls=document.getElementsByName("check");
			var ch=document.getElementById("checkall");
			if(ch.checked){
				for(var i=0;i<alls.length;i++){
					alls[i].checked=true;
				}
			}else{
				for(var i=0;i<alls.length;i++){
					alls[i].checked=false;
				}
			}
		}
		function delAll(){
			var alls=document.getElementsByName("check");
			var ids=new Array();
			for(var i=0;i<alls.length;i++){
				if(alls[i].checked){
					ids.push(alls[i].value);
				}
			}
			if(ids.length>0){
				if(confirm("确认操作?")){
					alert("成功!");
				}
			}else{
				alert("请选中要操作的项");
			}
		}
    $(function () {
		$('#backid').click(function(){
				window.location.href="dispensing.html";
		 });
    });
        }
    </script>
</head>
<body>
   
<form action="/chargeproject/chargeManage" method="post" class="definewidth m20">
<table class="table table-bordered table-hover definewidth m10">
    <tr>
        <td width="10%" class="tableleft">收费项目名称：</td>
        <td width="10%"><input type="text" id="inp" name="cmname"  value="<#if chargeproject.cmname??>${chargeproject.cmname}</#if>"/></td>
		
        <td>
		<button type="submit" class="btn btn-primary" type="button">查询</button> 
			<button type="reset" class="btn btn-primary" id="but" >清空</button> </td>
    </tr>
   
</table>

</form>
<table class="table table-bordered table-hover definewidth m10" >
   <thead>
    <tr>
        <th>编号</th>
        <th>收费项目名称</th>        
        <th>收费金额</th>
        <th>创建日期</th>
        <th>操作</th>
    </tr>
    </thead>

    <#list chargeprojectList as chargeproject>
	     <tr>
            <td style="vertical-align:middle;">${chargeproject.cmid}</td>
            <td style="vertical-align:middle;">${chargeproject.cmname}</td>
            <td style="vertical-align:middle;">${chargeproject.cmmoney}</td>
            <td style="vertical-align:middle;">${chargeproject.cmdate?string("yyyy-MM-dd HH:mm:ss")}</td>
            <td style="vertical-align:middle;">
				<a href="/chargeproject/chargeUpdate?cmid=${chargeproject.cmid}">修改</a>
				<a href="/chargeproject/chargeRemove?cmid=${chargeproject.cmid}">删除</a>
				<a  id="detail" href="/chargeproject/chargeDetails?cmid=${chargeproject.cmid}">查看详情</a>
			</td>
        </tr>
    </#list>
  </table>
  
  <table class="table table-bordered table-hover definewidth m10" >
  	<tr><th colspan="5">  <div class="inline pull-right page">


                <#if page.pageNum!=1>
                    <a href='/chargeproject/chargeManage?pageNum=1<#if chargeproject.cmname??>&cmname=${chargeproject.cmname}</#if>' >首页</a>
                    <a href="/chargeproject/chargeManage?pageNum=${page.pageNum-1}<#if chargeproject.cmname??>&cmname=${chargeproject.cmname}</#if>">上一页</a>
                <#else >
                    <a href="/chargeproject/chargeManage?pageNum1<#if chargeproject.cmname??>&cmname=${chargeproject.cmname}</#if>">首页</a>
                </#if>
                <#if page.pages lte 5 >
                    <#list 1..page.pages as i>
                        <a <#if page.pageNum==i>class='current' </#if> href="/chargeproject/chargeManage?pageNum=${i}<#if chargeproject.cmname??>&cmname=${chargeproject.cmname}</#if>"  >${i}</a>
                    </#list>
                </#if>
                <#if page.pageNum lte 3 && page.pages gte 5>
                    <#list 1..5 as i>
                        <a <#if page.pageNum==i>class='current'</#if>  href="/chargeproject/chargeManage?pageNum=${i}<#if chargeproject.cmname??>&cmname=${chargeproject.cmname}</#if>"  >${i}</a>
                    </#list>
                </#if>
                <#if page.pageNum gt 3 && page.pageNum+2 lte page.pages && page.pages gte 5>
                    <#list page.pageNum-2..page.pageNum+2 as i>
                        <a <#if page.pageNum==i>class='current'</#if> href="/chargeproject/chargeManage?pageNum=${i}<#if chargeproject.cmname??>&cmname=${chargeproject.cmname}</#if>"  >${i}</a>
                    </#list>
                </#if>
                <#if  page.pageNum gt 3 && page.pageNum+2 gt page.pages && page.pages gte 5>
                    <#list page.pages-4..page.pages as i>
                        <a <#if page.pageNum==i>class='current'</#if> href="/chargeproject/chargeManage?pageNum=${i}<#if chargeproject.cmname??>&cmname=${chargeproject.cmname}</#if>"  >${i}</a>
                    </#list>
                </#if>

                <#if page.pageNum!=page.pages>
                    <a href='/chargeproject/chargeManage?pageNum=${page.pageNum+1}<#if chargeproject.cmname??>&cmname=${chargeproject.cmname}</#if>' >下一页</a>
                    <a href="/chargeproject/chargeManage?pageNum=${page.pages}<#if chargeproject.cmname??>&cmname=${chargeproject.cmname}</#if>">尾页</a>
                <#else >
                    <a href="/chargeproject/chargeManage?pageNum=${page.pages}<#if chargeproject.cmname??>&cmname=${chargeproject.cmname}</#if>">尾页</a>
                </#if>
                &nbsp;&nbsp;&nbsp;共<span class='current'>${page.total}</span>条记录<span class='current'> ${page.pageNum}/${page.pages} </span>页


		 </div>



		 <div><button type="button" class="btn btn-success" id="newNav">添加收费项目</button>
		 
		 </div>
		 
		 </th></tr>
  </table>
<div id="msg" style="width: 100px;height:30px;background: rgba(0,0,0,0.6);position: fixed;left:500px;top:230px;border-radius: 5px;
        text-align: center;line-height: 30px;color: white;display: none;">
    <#if msg??><div style="margin-left: 5px;">${msg}</div></#if>欢迎光临！
</div>
</body>
<script>
    $(function () {
        if($("#msg").innerHTML != ""){
            $("#msg").css("display","block").slideDown();
            timerMove($("#msg"));
        }
        //定时器
        function timerMove($this){
            var time=1;
            var timer=setInterval(function(){
                time--;
                if(time<0){
                    clearInterval(timer);
                    time=1;
                    $("#msg").css("display","none").slideUp();
                }
            },1000);
        }
    })
</script>
</html>
