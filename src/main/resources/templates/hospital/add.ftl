<!DOCTYPE html>
<html>
<head>
    <title>录入住院信息-2015</title>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="/static/Css/bootstrap.css" />
    <link rel="stylesheet" type="text/css" href="/static/Css/bootstrap-responsive.css" />
    <link rel="stylesheet" type="text/css" href="/static/Css/style.css" />
    <script type="text/javascript" src="/static/Js/jquery.js"></script>
    <script type="text/javascript" src="/static/Js/jquery.sorted.js"></script>
    <script type="text/javascript" src="/static/Js/bootstrap.js"></script>
    <script type="text/javascript" src="/static/Js/ckform.js"></script>
    <script type="text/javascript" src="/static/Js/common.js"></script>
    <script type="text/javascript" src="/static/Js/ckeditor/ckeditor.js"></script>
    <style type="text/css">
        body {
            padding-bottom: 40px;
        }
        .sidebar-nav {
            padding: 9px 0;
        }

        @media (max-width: 980px) {
            /* Enable use of floated navbar text */
            .navbar-text.pull-right {
                float: none;
                padding-left: 5px;
                padding-right: 5px;
            }
        }
    </style>
    <script type="text/javascript">
    $(function () {       
		$("#bephid").keyup(function(){
            var behpid = $(this).val();
            if(behpid.length>=12){
                location.href="/beh/doAjax?behpid="+behpid;
            }
		 });
		$("#btn").click(function () {
            if($("#behpmoney").val()==""){
                alert("金额不能为空！");
                return false;
            }
        });
    });
    </script>
</head>
<body>
<form action="/beh/doAdd" method="post" class="definewidth m20">
<table class="table table-bordered table-hover definewidth m10">
    <tr>
        <input type="hidden" name="bhid" value="${aja.behospitalinfo.bhid}" />
        <td width="10%" class="tableleft"><font color="red">*</font>病历号 <span style="color: red;"><#if msg??>${msg}</#if></span></td>
        <td><input type="text" name="bephid" id="bephid" value="${aja.behpid}" placeholder="输入病历号"/></td>
    </tr>
    <tr>
        <td width="10%" class="tableleft">姓名</td>
        <td>
            <#if aja.hosrname??>${aja.hosrname}<#else>无</#if>
        </td>
    </tr>
    <tr>
        <td width="10%" class="tableleft">证件类型</td>
        <td>身份证</td>
    </tr>
    <tr>
        <td width="10%" class="tableleft">证件号</td>
        <td><#if aja.hosridcard??>${aja.hosridcard}<#else>无</#if></td>
    </tr>
    <tr>
        <td width="10%" class="tableleft">社保号</td>
        <td><#if aja.hosrssnum??>${aja.hosrssnum}<#else>无</#if></td>
    </tr>
    <tr>
        <td width="10%" class="tableleft">联系电话</td>
        <td><#if aja.hosrphone??>${aja.hosrphone}<#else>无</#if></td>
    </tr>
    <tr>
        <td width="10%" class="tableleft">是否自费</td>
        <td>
            <input type="radio" name="hosrvisit" value="${aja.hosrvisit}" <#if aja.hosrvisit==1>checked</#if>/>否&nbsp;&nbsp;&nbsp;
            <input type="radio" name="hosrvisit" value="${aja.hosrvisit}" <#if aja.hosrvisit==0>checked</#if>/>是
        </td>
    </tr>
    <tr>
        <td width="10%" class="tableleft">性别</td>
        <td>
            <input type="radio" name="hosrsex" value="${aja.hosrsex}" <#if aja.hosrsex==1>checked</#if>/>男&nbsp;&nbsp;&nbsp;
            <input type="radio" name="hosrsex" value="${aja.hosrsex}" <#if aja.hosrsex==0>checked</#if>/>女
        </td>
    </tr>
    <tr>
        <td width="10%" class="tableleft">年龄</td>
        <td><#if aja.hosrage??>${aja.hosrage}<#else>无</#if></td>
    </tr>
    <tr>
        <td width="10%" class="tableleft">初复诊</td>
        <td>
            <input type="radio" name="hosrLookdoctor" value="${aja.hosrLookdoctor}" <#if aja.hosrLookdoctor==0>checked</#if>/>初诊&nbsp;&nbsp;&nbsp;
            <input type="radio" name="hosrLookdoctor" value="${aja.hosrLookdoctor}" <#if aja.hosrLookdoctor==1>checked</#if>/>复诊</td>
    </tr>
    <tr>
        <td width="10%" class="tableleft">所挂科室</td>
        <td>
            <input type="text" readonly="readonly" name="" id="" value="${aja.department.departname}" />
        </td>
    </tr>
    <tr>
        <td width="10%" class="tableleft">指定医生</td>
        <td>
            <input type="text" readonly="readonly" name="" id="" value="${aja.doctor.dname}" />
        </td>
    </tr>
	<tr>
        <td width="10%" class="tableleft">备注</td>
        <td><#if aja.hosrremark??>${aja.hosrremark}</#if></td>
	</tr>
    <tr>
        <td width="10%" class="tableleft">护理</td>
        <td>
            <select name="hehpnurse">
                <#list nurse as n>
                    <option value="${n.hehpnurse}" <#--<#if n.behpid==aja.behpid>selected="true"</#if>--> >${n.hehpnurse}</option>
                </#list>
            </select>
        </td>
    </tr>
    <tr>
        <td width="10%" class="tableleft">床位号</td>
        <td>
            <select name="behpbed">
                <#list nurse as n>
                    <option value="${n.behpbed}" <#--<#if n.behpid==aja.behpid>selected="true"</#if>--> >${n.behpbed}</option>
                </#list>
            </select>
        </td>
    </tr>
    <tr>
        <td width="10%" class="tableleft">缴费押金</td>
        <td>
            <input type="text" name="behpmoney" id="behpmoney" value=""/>
        </td>
    </tr>
    <tr>
        <td width="10%" class="tableleft">病情</td>
        <td><textarea name="behpillness">请填写病情...</textarea></td>
    </tr>
    <tr>
        <td colspan="2">
			<center>
				<button type="submit" class="btn btn-primary" id="btn" >保存</button> &nbsp;&nbsp;
                <button type="button" class="btn btn-success" onclick="history.back(-1)" id="backid">返回列表</button>
			</center>
		</td>
    </tr>
</table>
</form>
<div id="msg" style="width: 100px;height:30px;position: fixed;left:500px;top:0px;
    text-align: center;line-height: 30px;color: blueviolet;display: none;font-size: 18px;font-weight: bold;">
    <#if msg??>${msg}</#if>
</div>
</body>
<script>
    $(function () {
        if($("#msg").innerHTML != ""){
            $("#msg").css("display","block").slideDown();
            timerMove($("#msg"));
        }
        //定时器
        function timerMove($this){
            var time=3;
            var timer=setInterval(function(){
                time--;
                if(time<0){
                    clearInterval(timer);
                    time=3;
                    $("#msg").css("display","none").slideUp();
                }
            },1000);
        }
    })
</script>
</html>