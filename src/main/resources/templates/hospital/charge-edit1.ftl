<!DOCTYPE html>
<html>
<head>
    <title>修改收费项目</title>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="/static/Css/bootstrap.css" />
    <link rel="stylesheet" type="text/css" href="/static/Css/bootstrap-responsive.css" />
    <link rel="stylesheet" type="text/css" href="/static/Css/style.css" />
    <script type="text/javascript" src="/static/Js/jquery.js"></script>
    <script type="text/javascript" src="/static/Js/jquery.sorted.js"></script>
    <script type="text/javascript" src="/static/Js/bootstrap.js"></script>
    <script type="text/javascript" src="/static/Js/ckform.js"></script>
    <script type="text/javascript" src="/static/Js/common.js"></script>
    <script type="text/javascript" src="/static/Js/ckeditor/ckeditor.js"></script>
 

    <style type="text/css">
        body {
            padding-bottom: 40px;
        }
        .sidebar-nav {
            padding: 9px 0;
        }

        @media (max-width: 980px) {
            /* Enable use of floated navbar text */
            .navbar-text.pull-right {
                float: none;
                padding-left: 5px;
                padding-right: 5px;
            }
        }


    </style>
    <script type="text/javascript">
        window.onload=function () {//页面加载完之后触发事件执行函数

      /*      $('#backid').click(function () {
                window.location.href = "/chargeproject/chargeManage";
            });*/

            var inp=document.getElementsByTagName("input");
            var inp1=inp[1];//名称
            var inp2=inp[2];//金额

            inp2.onblur=function () {
                var cmmoney=this.value;
                var reg=/^\d+(\.\d{2})?$/;
                var falg=reg.test(cmmoney);
                if(cmmoney&&!falg){
                    alert("金额只能是数字~，最多保留两位小数");
                    this.value="";
                    this.focus();
                }
            }

            $('#but').click(function () {
                if( $('#inp').val()===""){
                    alert("项目名称不能为空~，请重新输入！");
                    $('#inp').focus();
                    return false;
                }else if($('#inp1').val()===""){
                    alert("金额不能为空~，请重新输入！");
                    $('#inp1').focus();
                    return false;
                }else {
                    alert("修改成功！");
                }

            });
        }
    </script>
</head>
<body>
<form action="/chargeproject/updateCharge/" method="post" class="definewidth m20">
<table class="table table-bordered table-hover definewidth m10">

    <tr>
        <td width="10%" class="tableleft">编号</td>
        <td>${chargeproject.cmid}</td>
        <input type="hidden" name="cmid" value="${chargeproject.cmid}">
    </tr>
    <tr>
        <td width="10%" class="tableleft"><font color="red">*</font>收费项目名称</td>
        <td><input type="text"  name="cmname"   id="inp"  value="${chargeproject.cmname}" /></td>
    </tr>

    <tr>
        <td width="10%" class="tableleft"><font color="red">*</font>收费金额</td>
        <td><input type="text" name="cmmoney"   id="inp1" value="${chargeproject.cmmoney}"/></td>
    </tr>
    <tr>
        <td colspan="2">
			<center>
				<button type="submit" class="btn btn-priinpmary" id="but" type="button">保存</button> &nbsp;&nbsp;<button type="button" class="btn btn-success" onclick="history.back(-1)">返回列表</button>
			</center>
		</td>
    </tr>

</table>
</form>
</body>
</html>