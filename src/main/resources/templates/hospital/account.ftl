<!DOCTYPE html>
<html>
<head>
    <title>住院结算--2018</title>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="/static/Css/bootstrap.css"/>
    <link rel="stylesheet" type="text/css" href="/static/Css/bootstrap-responsive.css"/>
    <link rel="stylesheet" type="text/css" href="/static/Css/style.css"/>
    <script type="text/javascript" src="/static/Js/jquery.js"></script>
    <script type="text/javascript" src="/static/Js/jquery.sorted.js"></script>
    <script type="text/javascript" src="/static/Js/bootstrap.js"></script>
    <script type="text/javascript" src="/static/Js/ckform.js"></script>
    <script type="text/javascript" src="/static/Js/common.js"></script>
    <style type="text/css">
        body {
            padding-bottom: 40px;
        }
        .sidebar-nav {
            padding: 9px 0;
        }
        @media (max-width: 980px) {
            /* Enable use of floated navbar text */
            .navbar-text.pull-right {
                float: none;
                padding-left: 5px;
                padding-right: 5px;
            }
        }
    </style>
    <script type="text/javascript">
        $(function () {
            $('#newNav').click(function () {
                window.location.href = "dispensing-gives.html";
            });
           /* $("#btn").click(function () {
                alert($("#behpid").val());
                alert($("#behpname").val());
            })*/
            
        });
        function checkall() {
            var alls = document.getElementsByName("check");
            var ch = document.getElementById("checkall");
            if (ch.checked) {
                for (var i = 0; i < alls.length; i++) {
                    alls[i].checked = true;
                }
            } else {
                for (var i = 0; i < alls.length; i++) {
                    alls[i].checked = false;
                }
            }
        }
        function delAll() {
            var alls = document.getElementsByName("check");
            var ids = new Array();
            for (var i = 0; i < alls.length; i++) {
                if (alls[i].checked) {
                    ids.push(alls[i].value);
                }
            }
            if (ids.length > 0) {
                if (confirm("确认操作?")) {
                    alert("成功!");
                }
            } else {
                alert("请选中要操作的项");
            }
        }
    </script>
</head>
<body>

<form action="/beh/account" method="post" class="definewidth m20">
    <table class="table table-bordered table-hover definewidth m10">
        <tr>
            <td width="10%" class="tableleft">病例号：</td>
            <td><input type="text" name="behpid" id="behpid" value="<#if behpid??>${behpid}</#if>"/></td>
            <td width="10%" class="tableleft">姓名：</td>
            <td><input type="text" name="behpname" id="behpname" value="<#if behpname??>${behpname}</#if>"/></td>
        </tr>
        <tr>
            <td colspan="4">
                <center>
                    <button type="submit" class="btn btn-primary" type="button">查询</button>
                    <button class="btn btn-primary" type="reset" id="btn" >清空</button>
                </center>
            </td>
        </tr>
    </table>
</form>

<table class="table table-bordered table-hover definewidth m10">
    <thead>
    <tr>
        <th><input type="checkbox" id="checkall" onChange="checkall();"></th>
        <th>病历号</th>
        <th>姓名</th>
        <th>押金</th>
        <th>当前余额</th>
        <th>状态</th>
        <th>操作</th>
    </tr>
    </thead>
    <#list list as li>
        <tr>
            <input type="hidden" name="bhid" value="${li.behId}" />
            <td style="vertical-align:middle;"><input type="checkbox" name="check" value="${li.behId}"></td>
            <td style="vertical-align:middle;"><#if li.behospitalinfo.behpid??>${li.behospitalinfo.behpid}</#if></td>
            <td style="vertical-align:middle;"><#if li.behospitalinfo.behpname??>${li.behospitalinfo.behpname}</#if></td>
            <td style="vertical-align:middle;"><#if li.behospitalinfo.behpmoney??>${li.behospitalinfo.behpmoney}元</#if></td>
            <td style="vertical-align:middle;"><#if li.behospitalinfo.behpTcid??>${li.behospitalinfo.behpTcid}元</#if></td><#--<#if li.behpmoney??>${li.behpmoney}元</#if>-->
            <td style="vertical-align:middle;"><#if li.behospitalinfo.hehpdel==1>已结算<#else>未结算</#if></td>
            <td style="vertical-align:middle;">
                <a href="/beh/showaccount?beHpId=${li.behId}">详细信息</a>&nbsp;&nbsp;&nbsp;
                <#if li.behospitalinfo.hehpdel==0>
                    <a href="/beh/docount?behpid=${li.behospitalinfo.behpid}&&bhid=${li.behospitalinfo.bhid}">结算</a>
                </#if>
            </td>
        </tr>
    </#list>
</table>

<table class="table table-bordered table-hover definewidth m10">
    <tr>
        <th colspan="5">
            <div class="inline pull-right page">

                <#if page.pageNum!=1>
                    <a href='/beh/account?pageNum=1<#if did??>&behpid=${behpid}</#if><#if behpname??>&dname=${behpname}</#if>' >首页</a>
                    <a href="/beh/account?pageNum=${page.pageNum-1}<#if did??>&behpid=${behpid}</#if><#if behpname??>&dname=${behpname}</#if>">上一页</a>
                <#else>
                    <a href="/beh/account?pageNum1<#if did??>&behpid=${behpid}</#if><#if behpname??>&dname=${behpname}</#if>">首页</a>
                </#if>
                <#if page.pages lte 5 >
                    <#list 1..page.pages as i>
                        <a <#if page.pageNum==i>class='current' </#if> href="/beh/account?pageNum=${i}<#if did??>&behpid=${behpid}</#if><#if behpname??>&dname=${behpname}</#if>"  >${i}</a>
                    </#list>
                </#if>
                <#if page.pageNum lte 3 && page.pages gte 5>
                    <#list 1..5 as i>
                        <a <#if page.pageNum==i>class='current'</#if>  href="/beh/account?pageNum=${i}<#if did??>&behpid=${behpid}</#if><#if behpname??>&dname=${behpname}</#if>"  >${i}</a>
                    </#list>
                </#if>
                <#if page.pageNum gt 3 && page.pageNum+2 lte page.pages && page.pages gte 5>
                    <#list page.pageNum-2..page.pageNum+2 as i>
                        <a <#if page.pageNum==i>class='current'</#if> href="/beh/account?pageNum=${i}<#if did??>&behpid=${behpid}</#if><#if behpname??>&dname=${behpname}</#if>"  >${i}</a>
                    </#list>
                </#if>
                <#if  page.pageNum gt 3 && page.pageNum+2 gt page.pages && page.pages gte 5>
                    <#list page.pages-4..page.pages as i>
                        <a <#if page.pageNum==i>class='current'</#if> href="/beh/account?pageNum=${i}<#if did??>&behpid=${behpid}</#if><#if behpname??>&dname=${behpname}</#if>"  >${i}</a>
                    </#list>
                </#if>

                <#if page.pageNum!=page.pages>
                    <a href='/beh/account?pageNum=${page.pageNum+1}<#if did??>&behpid=${behpid}</#if><#if behpname??>&dname=${behpname}</#if>' >下一页</a>
                    <a href="/beh/account?pageNum=${page.pages}<#if did??>&behpid=${behpid}</#if><#if behpname??>&dname=${behpname}</#if>">尾页</a>
                <#else >
                    <a href="/beh/account?pageNum=${page.pages}<#if did??>&behpid=${behpid}</#if><#if behpname??>&dname=${behpname}</#if>">尾页</a>
                </#if>
                &nbsp;&nbsp;&nbsp;共<span class='current'>${page.total}</span>条记录<span class='current'> ${page.pageNum}/${page.pages} </span>页

            </div>
            <div>
                <button type="button" class="btn btn-success" id="delPro">导出Excel</button>
            </div>

        </th>
    </tr>
</table>
<div id="msg" style="width: 100px;height:30px;position: fixed;left:500px;top:0px;
    text-align: center;line-height: 30px;color: blueviolet;display: none;font-size: 18px;font-weight: bold;">
    <#if msg??>${msg}</#if>
</div>
</body>
<script>
    $(function () {
        if($("#msg").innerHTML != ""){
            $("#msg").css("display","block").slideDown();
            timerMove($("#msg"));
        }
        //定时器
        function timerMove($this){
            var time=3;
            var timer=setInterval(function(){
                time--;
                if(time<0){
                    clearInterval(timer);
                    time=3;
                    $("#msg").css("display","none").slideUp();
                }
            },1000);
        }
    })
</script>
</html>
