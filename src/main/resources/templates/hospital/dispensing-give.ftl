<!DOCTYPE html>
<html>
<head>
    <title>发药--中软高科-2015</title>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="/static/Css/bootstrap.css" />
    <link rel="stylesheet" type="text/css" href="/static/Css/bootstrap-responsive.css" />
    <link rel="stylesheet" type="text/css" href="/static/Css/style.css" />
    <script type="text/javascript" src="/static/Js/jquery.js"></script>
    <script type="text/javascript" src="/static/Js/jquery.sorted.js"></script>
    <script type="text/javascript" src="/static/Js/bootstrap.js"></script>
    <script type="text/javascript" src="/static/Js/ckform.js"></script>
    <script type="text/javascript" src="/static/Js/common.js"></script>
    <script type="text/javascript" src="/static/Js/ckeditor/ckeditor.js"></script>
 

    <style type="text/css">
        body {
            padding-bottom: 40px;
        }
        .sidebar-nav {
            padding: 9px 0;
        }

        @media (max-width: 980px) {
            /* Enable use of floated navbar text */
            .navbar-text.pull-right {
                float: none;
                padding-left: 5px;
                padding-right: 5px;
            }
        }


    </style>
    <script type="text/javascript">
    $(function () {       
		$('#backid').click(function(){
				window.location.href="dispensing.html";
		 });
    });
    </script>
</head>
<body>
<form action="/dispensing/dispatch" method="post" class="definewidth m20">
<table class="table table-bordered table-hover definewidth m10">
    <tr>
        <td width="10%" class="tableleft">病历号</td>
        <td id="hosrid"><#list hosregisterList as list>${list.hosrid}</#list></td>
    </tr>
    <tr>
        <td width="10%" class="tableleft">姓名</td>
        <td><#list hosregisterList as list>${list.hosrname}</#list></td>
    </tr>
    <tr>
        <td width="10%" class="tableleft"><font color="red">*</font>药品名称</td>
        <td>
            <select id="drId" name="id">
                <#list hosregisterList as list>
                    <#list list.drugpeople as drug>
                        <option value="${drug.drId}">${drug.drug.drugname}</option>
                    </#list>
                </#list>
            </select>
        </td>
    </tr>
    <tr>
        <td width="10%" class="tableleft"><font color="red">*</font>发药数量</td>
        <td ><input id="num" type="text" name="pname" value=""/></td>
    </tr>
    <tr>
        <td colspan="2">
			<center>
				<button type="button" id="dispatch" class="btn btn-primary">保存</button> &nbsp;&nbsp;<a href="/dispensing"><button type="button" class="btn btn-success">返回列表</button></a>
			</center
		</td>
    </tr>
</table>
</form>
</body>
<script src="/static/Js/jquery-1.8.2.min.js"></script>
<script>
    $("#dispatch").click(function () {
        var brid = parseInt($("#hosrid").text());
        var id = parseInt($("#drId").val());
        var num = parseInt($("#num").val());
        $.post("/dispensing/dispatch",{"brid":brid,"id":id,"num":num},function (result) {
            if (result == "true"){
                alert("发药成功!");
            } else {
                alert("发药失败,可能是库存没药!!")
            }
        });
    });
</script>
</html>