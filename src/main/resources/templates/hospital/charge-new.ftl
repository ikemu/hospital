<!DOCTYPE html>
<html>
<head>
    <title>添加收费项目--中软高科-2015</title>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="/static/Css/bootstrap.css" />
    <link rel="stylesheet" type="text/css" href="/static/Css/bootstrap-responsive.css" />
    <link rel="stylesheet" type="text/css" href="/static/Css/style.css" />
    <script type="text/javascript" src="/static/Js/jquery.js"></script>
    <script type="text/javascript" src="/static/Js/jquery.sorted.js"></script>
    <script type="text/javascript" src="/static/Js/bootstrap.js"></script>
    <script type="text/javascript" src="/static/Js/ckform.js"></script>
    <script type="text/javascript" src="/static/Js/common.js"></script>
    <script type="text/javascript" src="/static/Js/ckeditor/ckeditor.js"></script>


    <style type="text/css">
        body {
            padding-bottom: 40px;
        }
        .sidebar-nav {
            padding: 9px 0;
        }

        @media (max-width: 980px) {
            /* Enable use of floated navbar text */
            .navbar-text.pull-right {
                float: none;
                padding-left: 5px;
                padding-right: 5px;
            }
        }


    </style>
    <script type="text/javascript">
        window.onload=function () {//页面加载完之后触发事件执行函数

            $(function () {
                $('#inp1').blur(function () {
                   var cmname= $('#inp1').val();
                   $.post("/hrcheckcharge/checkChargeVerifier",{cmname:cmname},function (result) {
                       if(result!=0){
                           $('#spa').html("");
                           $('#but').removeAttr("disabled");
                           $('#inp2').attr("value",result);
                       }else {
                           $('#but').attr("disabled","disabled");
                           $('#spa').html("<i>该项目名称不存在，请重新输入</i>").css("color","red");
                           $('#inp2').attr("value","");
                       };
                    });
                });
            });


        };

    </script>
</head>
<body>
<form action="/hrcheckcharge/savecheckCharge" method="post" class="definewidth m20">
<table class="table table-bordered table-hover definewidth m10">
    <input type="hidden" name="behpid" value="<#if hrcheckcharge.behpid??>${hrcheckcharge.behpid}</#if>">
    <input type="hidden" name="behpname" value="<#if hrcheckcharge.behpname??>${hrcheckcharge.behpname}</#if>">

    <tr>
        <td width="10%" class="tableleft">病历号</td>
        <td><#if hrcheckcharge.behpid??>${hrcheckcharge.behpid}</#if></td>
    </tr>
    <tr>
        <td width="10%" class="tableleft">姓名</td>
        <td><#if hrcheckcharge.behpname??>${hrcheckcharge.behpname}</#if></td>
    </tr>
    <tr>
        <td width="10%" class="tableleft"><font color="red">*</font>收费项目</td>
        <td><input   type="text" id="inp1" name="cmname" value="<#if hrcheckcharge.cmname??>${hrcheckcharge.cmname}</#if>"/><span id="spa"></span></td>
    </tr>
    <tr>
        <td width="10%" class="tableleft"><font color="red">*</font>收费金额</td>
        <td><input   type="text" id="inp2" name="cmmoney" value="<#if hrcheckcharge.cmmoney??>${hrcheckcharge.cmmoney}</#if>" /></td>
    </tr>
    <tr>
        <td colspan="2">
			<center>
				<button type="submit" class="btn btn-primary"  disabled="disabled" id="but" type="button">保存</button> &nbsp;&nbsp;<button type="button" class="btn btn-success" onclick="history.back(-1)">返回列表</button>
			</center
		</td>
    </tr>
</table>
</form>
</body>
</html>