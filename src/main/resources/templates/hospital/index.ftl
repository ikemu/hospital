<!DOCTYPE html>
<html>
<head>
    <title>入院办理--2015</title>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="/static/Css/bootstrap.css" />
    <link rel="stylesheet" type="text/css" href="/static/Css/bootstrap-responsive.css" />
    <link rel="stylesheet" type="text/css" href="/static/Css/style.css" />
    <script type="text/javascript" src="/static/Js/jquery-1.12.4.js"></script>
    <script type="text/javascript" src="/static/Js/jquery.sorted.js"></script>
    <script type="text/javascript" src="/static/Js/bootstrap.js"></script>
    <script type="text/javascript" src="/static/Js/ckform.js"></script>
    <script type="text/javascript" src="/static/Js/common.js"></script>
    <style type="text/css">
        body {
            padding-bottom: 40px;
        }
        .sidebar-nav {
            padding: 9px 0;
        }
        @media (max-width: 980px) {
            /* Enable use of floated navbar text */
            .navbar-text.pull-right {
                float: none;
                padding-left: 5px;
                padding-right: 5px;
            }
        }
    </style>
    <script type="text/javascript">
        //全选反选
    	function checkall(){
			var alls=document.getElementsByName("check");
			var ch=document.getElementById("checkall");
			if(ch.checked){
				for(var i=0;i<alls.length;i++){
					alls[i].checked=true;
				}
			}else{
				for(var i=0;i<alls.length;i++){
					alls[i].checked=false;
				}
			}
		}
        //退院出院
		function delAll(){
			var alls=document.getElementsByName("check");
			var ids=new Array();
			for(var i=0;i<alls.length;i++){
				if(alls[i].checked){
					ids.push(alls[i].value);//此值为病历号
				}
			}
			if(ids.length>0){
				if(confirm("确认操作?")){
                    //alert(ids);
                    window.location.href="/beh/delBehospital?rids="+ids;
				}
			}else{
				alert("请选中要操作的项");
			}
		}
        //导出excel表格
         function doExcel(){
             var alls=document.getElementsByName("check");
             var ids=new Array();
             for(var i=0;i<alls.length;i++){
                 if(alls[i].checked){
                     ids.push(alls[i].value);//此值为病历号
                 }
             }
             if(ids.length>0){
                 if(confirm("确认操作?")){
                     //alert(ids);
                     window.location.href="/beh/doexcel?rids="+ids;
                 }
             }else{
                 alert("请选中要操作的项");
             }
         }
    </script>
</head>
<body>
<form action="/beh/hospital" method="post" class="definewidth m20">
<table class="table table-bordered table-hover definewidth m10">
    <tr>
        <td width="10%" class="tableleft">病例号：</td>
        <td><input type="text" name="behpid" value="<#if behpid??>${behpid}</#if>"/></td>

        <td width="10%" class="tableleft">主治医生：</td>
        <td><input type="text" name="dname" value="<#if dname??>${dname}</#if>"/></td>

        <td width="10%" class="tableleft">科室：</td>
        <td><input type="text" name="departname" value="<#if departname??>${departname}</#if>"/></td>
    </tr>
    <tr>
        <td width="10%" class="tableleft">住院时间：</td>

        <td colspan="5">
            <input type="text" name="hosrfromdate" value="<#if hosrfromdate??>${hosrfromdate}</#if>"/>&nbsp;至&nbsp;
            <input type="text" name="hosrtodate" value="<#if hosrtodate??>${hosrtodate}</#if>"/>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <button type="submit" class="btn btn-primary" type="button">查询</button>
            <button class="btn btn-primary" type="reset">清空</button>

        </td>
    </tr>
</table>
</form>

<table class="table table-bordered table-hover definewidth m10" >
   <thead>
    <tr>
    	<th><input type="checkbox" id="checkall" onChange="checkall();"></th>
        <th>病历号</th>
        <th>姓名</th>
        <th>床位号</th>
        <th>联系电话</th>
        <th>押金</th>
        <th>主治医生</th>
        <th>入院时间</th>
        <th>科室</th>
        <th>状态</th>
        <th>操作</th>
    </tr>
    </thead>
        <#list hospList as list>
            <#if list.hehpdel==1>
             <tr >
                <td style="vertical-align:middle;"><input type="checkbox" name="check" value="${list.behpid}"></td>
                <td style="vertical-align:middle;"><#if list.behpbed??>${list.behpid}</#if></td><#--<#if ??></#if>-->
                <td style="vertical-align:middle;"><#if list.behpname??>${list.behpname}</#if></td>
                <td style="vertical-align:middle;"><#if list.behpbed??>${list.behpbed}</#if></td>
                <td style="vertical-align:middle;"><#if list.hosregister.hosrphone??>${list.hosregister.hosrphone}</#if></td>
                <td style="vertical-align:middle;"><#if list.behpmoney??>${list.behpmoney}</#if>元</td>
                <td style="vertical-align:middle;"><#if list.doctor.dname??>${list.doctor.dname}</#if></td>
                <td style="vertical-align:middle;">${list.behpdate?string("yyyy/MM/dd HH:mm:ss")}</td>
                <td style="vertical-align:middle;"><#if list.department.departname??>${list.department.departname}</#if>血液科</td>
                <td style="vertical-align:middle;">
                    <#if list.state.stateid==1>已挂号</#if>
                    <#if list.state.stateid==2>已退号</#if>
                    <#if list.state.stateid==3>已住院</#if>
                    <#if list.state.stateid==4>已退院</#if>
                    <#if list.state.stateid==5>已出院</#if>
                    <#if list.state.stateid==6>已出诊</#if>
                    <#if list.state.stateid==7>已结算</#if>
                    <#if list.state.stateid==8>待取药</#if>
                    <#if list.state.stateid==9>未结算</#if>
                    <#if list.state.stateid==10>待取药</#if>
                </td>
                <td style="vertical-align:middle;">
                    <a href="/beh/showhospital?beHpId=${list.behpid}">详情>></a>&nbsp;&nbsp;&nbsp;
                <#if list.state.stateid==7>
                    <#else><a href="/beh/upd?behpid=${list.behpid}">更改</a>&nbsp;&nbsp;&nbsp;
                </#if>
                    <#if list.behpmoney lte 0 && list.state.stateid != 7>
                        <a href="/beh/goAddMany?behpid=${list.behpid}">缴纳押金</a>&nbsp;&nbsp;&nbsp;
                    </#if>
                    <#if list.state.stateid==7>
                        <a href="/beh/doout?behpid=${list.behpid}">退院</a>&nbsp;&nbsp;&nbsp;
                        <a href="/beh/doout?behpid=${list.behpid}">出院</a>&nbsp;&nbsp;&nbsp;
                    </#if>
                </td>
            </tr>
           </#if>
       </#list>
  </table>

  <table class="table table-bordered table-hover definewidth m10" >
  	<tr><th colspan="5">
          <div class="inline pull-right page">

              <#if page.pageNum!=1>
                  <a href='/beh/hospital?pageNum=1<#if behpid??>&behpid=${behpid}</#if><#if dname??>&dname=${dname}</#if><#if departname??>&departname=${departname}</#if><#if hosrfromdate??>&hosrfromdate=${hosrfromdate}</#if><#if hosrfromdate??>&hosrfromdate=${hosrfromdate}</#if>' >首页</a>
                  <a href="/beh/hospital?pageNum=${page.pageNum-1}<#if behpid??>&behpid=${behpid}</#if><#if dname??>&dname=${dname}</#if><#if departname??>&departname=${departname}</#if><#if hosrfromdate??>&hosrfromdate=${hosrfromdate}</#if><#if hosrfromdate??>&hosrfromdate=${hosrfromdate}</#if>">上一页</a>
              <#else>
                  <a href="/beh/hospital?pageNum=1<#if behpid??>&behpid=${behpid}</#if><#if dname??>&dname=${dname}</#if><#if departname??>&departname=${departname}</#if><#if hosrfromdate??>&hosrfromdate=${hosrfromdate}</#if><#if hosrfromdate??>&hosrfromdate=${hosrfromdate}</#if>">首页</a>
              </#if>
              <#if page.pages lte 5 >
                  <#list 1..page.pages as i>
                      <a <#if page.pageNum==i>class='current' </#if> href="/beh/hospital?pageNum=${i}<#if behpid??>&behpid=${behpid}</#if><#if dname??>&dname=${dname}</#if><#if departname??>&departname=${departname}</#if><#if hosrfromdate??>&hosrfromdate=${hosrfromdate}</#if><#if hosrfromdate??>&hosrfromdate=${hosrfromdate}</#if>"  >${i}</a>
                  </#list>
              </#if>
              <#if page.pageNum lte 3 && page.pages gte 5>
                  <#list 1..5 as i>
                      <a <#if page.pageNum==i>class='current'</#if>  href="/beh/hospital?pageNum=${i}<#if behpid??>&behpid=${behpid}</#if><#if dname??>&dname=${dname}</#if><#if departname??>&departname=${departname}</#if><#if hosrfromdate??>&hosrfromdate=${hosrfromdate}</#if><#if hosrfromdate??>&hosrfromdate=${hosrfromdate}</#if>"  >${i}</a>
                  </#list>
              </#if>
              <#if page.pageNum gt 3 && page.pageNum+2 lte page.pages && page.pages gte 5>
                  <#list page.pageNum-2..page.pageNum+2 as i>
                      <a <#if page.pageNum==i>class='current'</#if> href="/beh/hospital?pageNum=${i}<#if behpid??>&behpid=${behpid}</#if><#if dname??>&dname=${dname}</#if><#if departname??>&departname=${departname}</#if><#if hosrfromdate??>&hosrfromdate=${hosrfromdate}</#if><#if hosrfromdate??>&hosrfromdate=${hosrfromdate}</#if>"  >${i}</a>
                  </#list>
              </#if>
              <#if  page.pageNum gt 3 && page.pageNum+2 gt page.pages && page.pages gte 5>
                  <#list page.pages-4..page.pages as i>
                      <a <#if page.pageNum==i>class='current'</#if> href="/beh/hospital?pageNum=${i}<#if behpid??>&behpid=${behpid}</#if><#if dname??>&dname=${dname}</#if><#if departname??>&departname=${departname}</#if><#if hosrfromdate??>&hosrfromdate=${hosrfromdate}</#if><#if hosrfromdate??>&hosrfromdate=${hosrfromdate}</#if>"  >${i}</a>
                  </#list>
              </#if>

              <#if page.pageNum!=page.pages>
                  <a href='/beh/hospital?pageNum=${page.pageNum+1}<#if behpid??>&behpid=${behpid}</#if><#if dname??>&dname=${dname}</#if><#if departname??>&departname=${departname}</#if><#if hosrfromdate??>&hosrfromdate=${hosrfromdate}</#if><#if hosrfromdate??>&hosrfromdate=${hosrfromdate}</#if>' >下一页</a>
                  <a href="/beh/hospital?pageNum=${page.pages}<#if behpid??>&behpid=${behpid}</#if><#if dname??>&dname=${dname}</#if><#if departname??>&departname=${departname}</#if><#if hosrfromdate??>&hosrfromdate=${hosrfromdate}</#if><#if hosrfromdate??>&hosrfromdate=${hosrfromdate}</#if>">尾页</a>
              <#else >
                  <a href="/beh/hospital?pageNum=${page.pages}<#if behpid??>&behpid=${behpid}</#if><#if dname??>&dname=${dname}</#if><#if departname??>&departname=${departname}</#if><#if hosrfromdate??>&hosrfromdate=${hosrfromdate}</#if><#if hosrfromdate??>&hosrfromdate=${hosrfromdate}</#if>">尾页</a>
              </#if>
              &nbsp;&nbsp;&nbsp;共<span class='current'>${page.total}</span>条记录<span class='current'> ${page.pageNum}/${page.pages} </span>页

		  </div>

		 <div><a href="/beh/goAdd" class="btn btn-success" style="color: white;text-decoration: none;">添加住院信息</a>&nbsp;&nbsp;&nbsp;
		 <button type="button" class="btn btn-success" id="delPro" onClick="delAll();">出院</button>&nbsp;&nbsp;&nbsp;
		 <button type="button" class="btn btn-success" id="delPro" onClick="delAll();">退院</button>&nbsp;&nbsp;&nbsp;
		 <button type="button" class="btn btn-success" id="delPro" onClick="doExcel()">导出Excel</button>
		 </div>

		 </th>
    </tr>
  </table>
    <div id="msg" style="width: 100px;height:30px;position: fixed;left:500px;top:0px;
    text-align: center;line-height: 30px;color: blueviolet;display: none;font-size: 18px;font-weight: bold;">
        <#if msg??>${msg}</#if>
    </div>
</body>
<script>
    $(function () {
        if($("#msg").innerHTML != ""){
            $("#msg").css("display","block").slideDown();
            timerMove($("#msg"));
        }
        //定时器
        function timerMove($this){
            var time=3;
            var timer=setInterval(function(){
                time--;
                if(time<0){
                    clearInterval(timer);
                    time=3;
                    $("#msg").css("display","none").slideUp();
                }
            },1000);
        }
    })
</script>
</html>
