<!DOCTYPE html>
<html>
<head>
    <title>收费项目管理</title>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="/static/Css/bootstrap.css" />
    <link rel="stylesheet" type="text/css" href="/static/Css/bootstrap-responsive.css" />
    <link rel="stylesheet" type="text/css" href="/static/Css/style.css" />
    <script type="text/javascript" src="/static/Js/jquery.js"></script>
    <script type="text/javascript" src="/static/Js/jquery.sorted.js"></script>
    <script type="text/javascript" src="/static/Js/bootstrap.js"></script>
    <script type="text/javascript" src="/static/Js/ckform.js"></script>
    <script type="text/javascript" src="/static/Js/common.js"></script>

    <style type="text/css">
        body {
            padding-bottom: 40px;
        }
        .sidebar-nav {
            padding: 9px 0;
        }

        @media (max-width: 980px) {
            /* Enable use of floated navbar text */
            .navbar-text.pull-right {
                float: none;
                padding-left: 5px;
                padding-right: 5px;
            }
        }


    </style>
    <script type="text/javascript">

        window.onload=function () {//页面加载完之后触发事件执行函数
            $(function () {
                $('#newNav').click(function () {
                    window.location.href = "charge-new.ftl";
                });
            });


            $('#but').click(function () {
                $("#inp1").attr("value", "");
                $("#inp2").attr("value", "");
                window.location.href = "/hrcheckcharge/checkCharge";
            });


            function checkall() {
                var alls = document.getElementsByName("check");
                var ch = document.getElementById("checkall");
                if (ch.checked) {
                    for (var i = 0; i < alls.length; i++) {
                        alls[i].checked = true;
                    }
                } else {
                    for (var i = 0; i < alls.length; i++) {
                        alls[i].checked = false;
                    }
                }
            }

            function delAll() {
                var alls = document.getElementsByName("check");
                var ids = new Array();
                for (var i = 0; i < alls.length; i++) {
                    if (alls[i].checked) {
                        ids.push(alls[i].value);
                    }
                }
                if (ids.length > 0) {
                    if (confirm("确认操作?")) {
                        alert("成功!");
                    }
                } else {
                    alert("请选中要操作的项");
                }
            }

            $(function () {
                $('#backid').click(function () {
                    window.location.href = "dispensing.html";
                });
            });
        }
    </script>
</head>
<body>
   
<form action="/hrcheckcharge/checkCharge" method="post" class="definewidth m20">
<table class="table table-bordered table-hover definewidth m10">
    <tr>
        <td width="10%" class="tableleft">病历号：</td>
        <td><input type="text" id="inp1" name="behpid" value="<#if hrcheckcharge.behpid??>${hrcheckcharge.behpid}</#if>"/></td>
		
        <td width="10%" class="tableleft">姓名：</td>
        <td><input type="text"  id="inp2" name="behpname" value="<#if hrcheckcharge.behpname??>${hrcheckcharge.behpname}</#if>"/></td>
    </tr>
    <tr>
		
		
		  <td colspan="4">
			<center>
				<button type="submit" class="btn btn-primary" type="button">查询</button> 
				<button type="reset" class="btn btn-primary" id="but" type="button">清空</button>
			</center>
        </td>
    </tr>
</table>
</form>
<table class="table table-bordered table-hover definewidth m10" >
   <thead>
    <tr>
        <th>病历号</th>
        <th>姓名</th>
        <th>住院日期</th>        
        <th>操作</th>
    </tr>
    </thead>

    <#list  hrcheckchargeList as hrcheckcharge>
	     <tr>
            <td style="vertical-align:middle;">${hrcheckcharge.behpid}</td>
            <td style="vertical-align:middle;">${hrcheckcharge.behpname}</td>
            <td style="vertical-align:middle;">${hrcheckcharge.cmdate?string("yyyy-MM-dd HH:mm:ss")}</td>
            <td style="vertical-align:middle;">
				<a href="/hrcheckcharge/checkChargedetails?behpid=${hrcheckcharge.behpid}">详情</a>
				<a href="/hrcheckcharge/checkChargeSave?behpid=${hrcheckcharge.behpid}">添加收费项</a>
			</td>
        </tr>
    </#list>
  </table>
  
  <table class="table table-bordered table-hover definewidth m10" >
  	<tr><th colspan="5">  <div class="inline pull-right page">

                <#if page.pageNum!=1>
                    <a href='/hrcheckcharge/checkCharge?pageNum=1<#if hrcheckcharge.behpid??>&behpid=${hrcheckcharge.behpid}</#if>' >首页</a>
                    <a href="/hrcheckcharge/checkCharge?pageNum=${page.pageNum-1}<#if hrcheckcharge.behpid??>&behpid=${hrcheckcharge.behpid}</#if>">上一页</a>
                <#else >
                    <a href="/hrcheckcharge/checkCharge?pageNum1<#if hrcheckcharge.behpid??>&behpid=${hrcheckcharge.behpid}</#if>">首页</a>
                </#if>
                <#if page.pages lte 5 >
                    <#list 1..page.pages as i>
                        <a <#if page.pageNum==i>class='current' </#if> href="/hrcheckcharge/checkCharge?pageNum=${i}<#if hrcheckcharge.behpid??>&behpid=${hrcheckcharge.behpid}</#if>" >${i}</a>
                    </#list>
                </#if>
                <#if page.pageNum lte 3 && page.pages gte 5>
                    <#list 1..5 as i>
                        <a <#if page.pageNum==i>class='current'</#if>  href="/hrcheckcharge/checkCharge?pageNum=${i}<#if hrcheckcharge.behpid??>&behpid=${hrcheckcharge.behpid}</#if>" >${i}</a>
                    </#list>
                </#if>
                <#if page.pageNum gt 3 && page.pageNum+2 lte page.pages && page.pages gte 5>
                    <#list page.pageNum-2..page.pageNum+2 as i>
                        <a <#if page.pageNum==i>class='current'</#if> href="/hrcheckcharge/checkCharge?pageNum=${i}<#if hrcheckcharge.behpid??>&behpid=${hrcheckcharge.behpid}</#if>"  >${i}</a>
                    </#list>
                </#if>
                <#if  page.pageNum gt 3 && page.pageNum+2 gt page.pages && page.pages gte 5>
                    <#list page.pages-4..page.pages as i>
                        <a <#if page.pageNum==i>class='current'</#if> href="/hrcheckcharge/checkCharge?pageNum=${i}<#if hrcheckcharge.behpid??>&behpid=${chargeproject.behpid}</#if>"  >${i}</a>
                    </#list>
                </#if>

                <#if page.pageNum!=page.pages>
                    <a href='/hrcheckcharge/checkCharge?pageNum=${page.pageNum+1}<#if hrcheckcharge.behpid??>&behpid=${hrcheckcharge.behpid}</#if>' >下一页</a>
                    <a href="/hrcheckcharge/checkCharge?pageNum=${page.pages}<#if hrcheckcharge.behpid??>&behpid=${hrcheckcharge.behpid}</#if>">尾页</a>
                <#else >
                    <a href="/hrcheckcharge/checkCharge?pageNum=${page.pages}<#if hrcheckcharge.behpid??>&behpid=${hrcheckcharge.behpid}</#if>">尾页</a>
                </#if>
                &nbsp;&nbsp;&nbsp;共<span class='current'>${page.total}</span>条记录<span class='current'> ${page.pageNum}/${page.pages} </span>页

            </div>

		 
		 </th></tr>
  </table>
  
</body>
<#if msg??>${msg}</#if>
</html>
