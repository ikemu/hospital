<!DOCTYPE html>
<html>
<head>
    <title>结算详细--2015</title>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="/static/Css/bootstrap.css" />
    <link rel="stylesheet" type="text/css" href="/static/Css/bootstrap-responsive.css" />
    <link rel="stylesheet" type="text/css" href="/static/Css/style.css" />
    <script type="text/javascript" src="/static/Js/jquery.js"></script>
    <script type="text/javascript" src="/static/Js/jquery.sorted.js"></script>
    <script type="text/javascript" src="/static/Js/bootstrap.js"></script>
    <script type="text/javascript" src="/static/Js/ckform.js"></script>
    <script type="text/javascript" src="/static/Js/common.js"></script>
    <style type="text/css">
        body {
            padding-bottom: 40px;
        }
        .sidebar-nav {
            padding: 9px 0;
        }
        @media (max-width: 980px) {
            /* Enable use of floated navbar text */
            .navbar-text.pull-right {
                float: none;
                padding-left: 5px;
                padding-right: 5px;
            }
        }

    </style>
    <script type="text/javascript">
        $(function () {
            $('#newNav').click(function(){
                window.location.href="add.html";
            });
        });

        function checkall(){
            var alls=document.getElementsByName("check");
            var ch=document.getElementById("checkall");
            if(ch.checked){
                for(var i=0;i<alls.length;i++){
                    alls[i].checked=true;
                }
            }else{
                for(var i=0;i<alls.length;i++){
                    alls[i].checked=false;
                }
            }
        }
        function delAll(){
            var alls=document.getElementsByName("check");
            var ids=new Array();
            for(var i=0;i<alls.length;i++){
                if(alls[i].checked){
                    ids.push(alls[i].value);
                }
            }
            if(ids.length>0){
                if(confirm("确认操作?")){
                    alert("成功!");
                }
            }else{
                alert("请选中要操作的项");
            }
        }
        $(function () {
            $('#backid').click(function(){
                window.location.href="showaccount.html";
            });
        });
    </script>
</head>
<body>

<table class="table table-bordered table-hover definewidth m10" >
    <thead>
    <tr>
        <th>病历号</th>
        <th>姓名</th>
        <th>收费项目</th>
        <th>收费金额</th>
        <th>收费日期</th>
    </tr>
    </thead>
    <#list showList as li>
        <tr >
        <td style="vertical-align:middle;"><#if li.behId??>${li.behId}</#if></td>
        <td style="vertical-align:middle;"><#if li.behospitalinfo.behpname??>${li.behospitalinfo.behpname}</#if></td>
        <td style="vertical-align:middle;"><#if li.chargeproject.cmname??>${li.chargeproject.cmname}</#if></td>
        <td style="vertical-align:middle;"><#if li.chargeproject.cmmoney??>${li.chargeproject.cmmoney}</#if></td>
        <td style="vertical-align:middle;">${li.chargeproject.cmdate?string("yyyy/MM/dd HH:mm:ss")}</td>
        </tr>
    </#list>
</table>

<table class="table table-bordered table-hover definewidth m10" >
    <tr><th colspan="5">
            <div class="inline pull-right page">



            </div>

            <div>
                <button type="button" class="btn btn-success" onclick="history.back(-1)" id="backid">返回列表</button>
                <button type="button" class="btn btn-success" >导出excel</button>
            </div>

        </th></tr>
</table>


<table class="table table-bordered table-hover definewidth m10">
    <tr>
        <td width="10%" class="tableleft">总花费(元)：</td>
        <td id="cost">${cost}</td>
    </tr>
    <tr>
        <td width="10%" class="tableleft">押金(元)：</td>
        <td id="yj">${yajin.behpmoney}</td>
    </tr>
    <tr>
        <td width="10%" class="tableleft">余额(元)：</td>
        <td id="ye"></td>
    </tr>
</table>
<script>
    $(function () {
        var a = parseInt($("#cost").html());
        var b2 = parseInt(String($("#yj").html()).replace(',',''));
        var b3 = b2-a;
        $("#ye").html(b3);
    });

</script>
</body>
</html>