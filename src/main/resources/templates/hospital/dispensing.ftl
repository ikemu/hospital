<!DOCTYPE html>
<html>
<head>
    <title>发药--中软高科-2015</title>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="/static/Css/bootstrap.css" />
    <link rel="stylesheet" type="text/css" href="/static/Css/bootstrap-responsive.css" />
    <link rel="stylesheet" type="text/css" href="/static/Css/style.css" />
    <script type="text/javascript" src="/static/Js/jquery.js"></script>
    <script type="text/javascript" src="/static/Js/jquery.sorted.js"></script>
    <script type="text/javascript" src="/static/Js/bootstrap.js"></script>
    <script type="text/javascript" src="/static/Js/ckform.js"></script>
    <script type="text/javascript" src="/static/Js/common.js"></script>

    <style type="text/css">
        body {
            padding-bottom: 40px;
        }
        .sidebar-nav {
            padding: 9px 0;
        }

        @media (max-width: 980px) {
            /* Enable use of floated navbar text */
            .navbar-text.pull-right {
                float: none;
                padding-left: 5px;
                padding-right: 5px;
            }
        }


    </style>
    <script type="text/javascript">
	 $(function () {
		$('#newNav').click(function(){
				window.location.href="dispensing-gives.html";
		 });
    });
	
    	function checkall(){
			var alls=document.getElementsByName("check");
			var ch=document.getElementById("checkall");
			if(ch.checked){
				for(var i=0;i<alls.length;i++){
					alls[i].checked=true;	
				}	
			}else{
				for(var i=0;i<alls.length;i++){
					alls[i].checked=false;	
				}	
			}
		}
		function delAll(){
			var alls=document.getElementsByName("check");
			var ids=new Array();
			for(var i=0;i<alls.length;i++){
				if(alls[i].checked){
					ids.push(alls[i].value);
				}		
			}
			if(ids.length>0){
				if(confirm("确认操作?")){
					alert("成功!");
				}
			}else{
				alert("请选中要操作的项");
			}
		}
    </script>
</head>
<body>

<form action="/dispensing" method="post" class="definewidth m20">
<table class="table table-bordered table-hover definewidth m10">
    <tr>
        <td width="10%" class="tableleft">病例号：</td>
        <td><input type="text" name="brid" <#if condition.brid??>value="${condition.brid}"</#if> /></td>

        <td width="10%" class="tableleft">姓名：</td>
        <td><input type="text" name="brname" <#if condition.brname??>value="${condition.brname}"</#if> /></td>
    </tr>
    <tr>
		  <td colspan="4"><center>
            <button type="submit" class="btn btn-primary" type="button">查询</button> 
            <button type="submit" class="btn btn-primary" type="button">清空</button> 
			</center>
        </td>
    </tr>
</table>
</form>
   
<table class="table table-bordered table-hover definewidth m10" >
   <thead>
    <tr>
    	<th><input type="checkbox" id="checkall" onChange="checkall();"></th>
        <th>病历号</th>
        <th>姓名</th>
        <th>负责人</th>
        <th>操作</th>
    </tr>
    </thead>
    <#if dispensingList??>
        <#list dispensingList as dispensing>
            <tr >
                <td style="vertical-align:middle;"><input type="checkbox" name="check" value="${dispensing.hosrid}"></td>
            <td style="vertical-align:middle;">${dispensing.behpid}</td>
            <td style="vertical-align:middle;">${dispensing.hosrname}</td>
            <td style="vertical-align:middle;">${dispensing.doctor.dname}</td>
                <td style="vertical-align:middle;">
                    <a href="/dispensing/dispensing-give?brid=${dispensing.hosrid}">发药</a>&nbsp;&nbsp;&nbsp;
                    <a href="/dispensing/drugDetail?id=${dispensing.hosrid}">详情...</a>
                </td>
            </tr>
        </#list>
    </#if>


  </table>
  
  <table class="table table-bordered table-hover definewidth m10" >
  	<tr><th colspan="5">  <div class="inline pull-right page">
                <#if page.pageNum!=1>
                    <a href='/dispensing?pageNum=1<#if condition.brid??>&drugName=${condition.brid}</#if><#if condition.brname??>&drugType=${condition.brname}</#if>' >首页</a>
                    <a href="/dispensing?pageNum=${page.pageNum-1}<#if condition.brid??>&drugName=${condition.brid}</#if><#if condition.brname??>&drugType=${condition.brname}</#if>">上一页</a>
                <#else >
                    <a href="/dispensing?pageNum1<#if condition.brid??>&drugName=${condition.brid}</#if><#if condition.brname??>&drugType=${condition.brname}</#if>">首页</a>
                </#if>
                <#if page.pages lte 5 >
                    <#list 1..page.pages as i>
                        <a <#if page.pageNum==i>class='current' </#if> href="/dispensing?pageNum=${i}<#if condition.brid??>&drugName=${condition.brid}</#if><#if condition.brname??>&drugType=${condition.brname}</#if>"  >${i}</a>
                    </#list>
                </#if>
                <#if page.pageNum lte 3 && page.pages gte 5>
                    <#list 1..5 as i>
                        <a <#if page.pageNum==i>class='current'</#if>  href="/dispensing?pageNum=${i}<#if condition.brid??>&drugName=${condition.brid}</#if><#if condition.brname??>&drugType=${condition.brname}</#if>"  >${i}</a>
                    </#list>
                </#if>
                <#if page.pageNum gt 3 && page.pageNum+2 lte page.pages && page.pages gte 5>
                    <#list page.pageNum-2..page.pageNum+2 as i>
                        <a <#if page.pageNum==i>class='current'</#if> href="/dispensing?pageNum=${i}<#if condition.brid??>&drugName=${condition.brid}</#if><#if condition.brname??>&drugType=${condition.brname}</#if>"  >${i}</a>
                    </#list>
                </#if>
                <#if  page.pageNum gt 3 && page.pageNum+2 gt page.pages && page.pages gte 5>
                    <#list page.pages-4..page.pages as i>
                        <a <#if page.pageNum==i>class='current'</#if> href="/dispensing?pageNum=${i}<#if condition.brid??>&drugName=${condition.brid}</#if><#if condition.brname??>&drugType=${condition.brname}</#if>"  >${i}</a>
                    </#list>
                </#if>

                <#if page.pageNum!=page.pages>
                    <a href='/dispensing?pageNum=${page.pageNum+1}<#if condition.brid??>&drugName=${condition.brid}</#if><#if condition.brname??>&drugType=${condition.brname}</#if>' >下一页</a>
                    <a href="/dispensing?pageNum=${page.pages}<#if condition.brid??>&drugName=${condition.brid}</#if><#if condition.brname??>&drugType=${condition.brname}</#if>">尾页</a>
                <#else >
                    <a href="/dispensing?pageNum=${page.pages}<#if condition.brid??>&drugName=${condition.brid}</#if><#if condition.brname??>&drugType=${condition.brname}</#if>">尾页</a>
                </#if>
                &nbsp;&nbsp;&nbsp;共<span class='current'>${page.total}</span>条记录<span class='current'> ${page.pageNum}/${page.pages} </span>页

            </div>
		 <div><button type="button" class="btn btn-success" id="newNav">发药</button>
		 
		 </div>
		 
		 </th></tr>
  </table>
  
</body>
</html>
