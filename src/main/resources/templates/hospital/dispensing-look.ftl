<!DOCTYPE html>
<html>
<head>
    <title>发药详情--中软高科-2015</title>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="/static/Css/bootstrap.css" />
    <link rel="stylesheet" type="text/css" href="/static/Css/bootstrap-responsive.css" />
    <link rel="stylesheet" type="text/css" href="/static/Css/style.css" />
    <script type="text/javascript" src="/static/Js/jquery.js"></script>
    <script type="text/javascript" src="/static/Js/jquery.sorted.js"></script>
    <script type="text/javascript" src="/static/Js/bootstrap.js"></script>
    <script type="text/javascript" src="/static/Js/ckform.js"></script>
    <script type="text/javascript" src="/static/Js/common.js"></script>

    <style type="text/css">
        body {
            padding-bottom: 40px;
        }
        .sidebar-nav {
            padding: 9px 0;
        }

        @media (max-width: 980px) {
            /* Enable use of floated navbar text */
            .navbar-text.pull-right {
                float: none;
                padding-left: 5px;
                padding-right: 5px;
            }
        }


    </style>
    <script type="text/javascript">
	 $(function () {
		$('#newNav').click(function(){
				window.location.href="add.html";
		 });
    });
	
    	function checkall(){
			var alls=document.getElementsByName("check");
			var ch=document.getElementById("checkall");
			if(ch.checked){
				for(var i=0;i<alls.length;i++){
					alls[i].checked=true;	
				}	
			}else{
				for(var i=0;i<alls.length;i++){
					alls[i].checked=false;	
				}	
			}
		}
		function delAll(){
			var alls=document.getElementsByName("check");
			var ids=new Array();
			for(var i=0;i<alls.length;i++){
				if(alls[i].checked){
					ids.push(alls[i].value);
				}		
			}
			if(ids.length>0){
				if(confirm("确认操作?")){
					alert("成功!");
				}
			}else{
				alert("请选中要操作的项");
			}
		}
    $(function () {       
		$('#backid').click(function(){
				window.location.href="dispensing.html";
		 });
    });
    </script>
</head>
<body>
   
<table class="table table-bordered table-hover definewidth m10" >
   <thead>
    <tr>
        <th>病历号</th>
        <th>姓名</th>
        <th>药品名称</th>
        <th>药品数量</th>
        <th>已发数量</th>
        <th>未发数量</th>
        <th>操作</th>
    </tr>
    </thead>
        <#if detailList??>
            <#list detailList as list>
                <#if list.drugpeople??>
                    <#list list.drugpeople as detail>
                        <tr id="${detail.drId}">
                        <input type="hidden" id="${detail.peopleid}"/>
                        <td id="hosrid" style="vertical-align:middle;"><#if list.hosrid??>${list.hosrid}</#if></td>
                        <td style="vertical-align:middle;"><#if list.hosrname??>${list.hosrname}</#if></td>
                        <td style="vertical-align:middle;"><#if detail.drug.drugname??>${detail.drug.drugname}</#if></td>
                        <td id="number" style="vertical-align:middle;"><#if detail.drNumber??>${detail.drNumber}</#if></td>
                        <td id="drGive" style="vertical-align:middle;"><#if detail.drGive??>${detail.drGive}</#if></td>
                        <td id="drNumber" style="vertical-align:middle;">${detail.drNumber - detail.drGive}</td>
                        <td style="vertical-align:middle;">
                            <a href="#" class="give" number="0" >全发</a>&nbsp;&nbsp;&nbsp;
                            <a href="#" class="give" number="1" >发1</a>&nbsp;&nbsp;&nbsp;
                            <a href="#" class="give" number="5" >发5</a>&nbsp;&nbsp;&nbsp;
                            <a href="#" class="give" number="30" >发30</a>
                        </td>
                        </tr>
                    </#list>
                </#if>
            </#list>
        </#if>


  </table>
  
  <table class="table table-bordered table-hover definewidth m10" >
  	<tr><th colspan="5">  <div class="inline pull-right page">

                <#if page.pageNum!=1>
                    <a href='/dispensing/drugDetail?pageNum=1' >首页</a>
                    <a href="/dispensing/drugDetail?pageNum=${page.pageNum-1}">上一页</a>
                <#else >
                    <a href="/dispensing/drugDetail?pageNum=1">首页</a>
                </#if>
                <#if page.pages lte 5 >
                    <#list 1..page.pages as i>
                        <a <#if page.pageNum==i>class='current' </#if> href="/dispensing/drugDetail?pageNum=${i}"  >${i}</a>
                    </#list>
                </#if>
                <#if page.pageNum lte 3 && page.pages gte 5>
                    <#list 1..5 as i>
                        <a <#if page.pageNum==i>class='current'</#if>  href="/dispensing/drugDetail?pageNum=${i}"  >${i}</a>
                    </#list>
                </#if>
                <#if page.pageNum gt 3 && page.pageNum+2 lte page.pages && page.pages gte 5>
                    <#list page.pageNum-2..page.pageNum+2 as i>
                        <a <#if page.pageNum==i>class='current'</#if> href="/dispensing/drugDetail?pageNum=${i}<#if condition.brid??>&drugName=${condition.brid}</#if><#if condition.brname??>&drugType=${condition.brname}</#if>"  >${i}</a>
                    </#list>
                </#if>
                <#if  page.pageNum gt 3 && page.pageNum+2 gt page.pages && page.pages gte 5>
                    <#list page.pages-4..page.pages as i>
                        <a <#if page.pageNum==i>class='current'</#if> href="/dispensing/drugDetail?pageNum=${i}<#if condition.brid??>&drugName=${condition.brid}</#if><#if condition.brname??>&drugType=${condition.brname}</#if>"  >${i}</a>
                    </#list>
                </#if>

                <#if page.pageNum!=page.pages>
                    <a href='/dispensing/drugDetail?pageNum=${page.pageNum+1}' >下一页</a>
                    <a href="/dispensing/drugDetail?pageNum=${page.pages}">尾页</a>
                <#else >
                    <a href="/dispensing/drugDetail?pageNum=${page.pages}">尾页</a>
                </#if>
                &nbsp;&nbsp;&nbsp;共<span class='current'>${page.total}</span>条记录<span class='current'> ${page.pageNum}/${page.pages} </span>页



            </div>
		 <div>
             <a href="/dispensing"><button type="button" class="btn btn-success">返回列表</button></a>
		 </div>
		 
		 </th></tr>
  </table>
  
</body>
<script src="/static/Js/jquery-1.8.2.min.js"></script>
<script>
    $(".give").click(function () {
        var targer = $(this);
        var brid = $(this).parent().siblings("input").first().attr("id");
        var id = $(this).parents("tr").attr("id");
        var num = parseInt($(this).attr("number"));
        var number = parseInt($(this).parent().siblings("#number").text());
        var give = parseInt($(this).parent().siblings("#drGive").text());
        var drNumber = parseInt($(this).parent().siblings("#drNumber").text());
        $.post("/dispensing/dispatch",{"brid":brid,"id":id,"num":num},function (result) {
            if (result == "true"){
                if ( num == 0 || give + num > number){
                    targer.parent().siblings("#drGive").text(number);
                    // $("#drGive").text(number);
                    targer.parent().siblings("#drNumber").text("0");
                    // alert("发药成功!");
                } else {
                    // $("#drGive").text(give+num);
                    targer.parent().siblings("#drGive").text(give+num);
                    targer.parent().siblings("#drNumber").text(drNumber-num);
                    // alert("发药成功!");
                }
            } else {
                alert("发药失败,可能是库存没药!!")
            }
        });




        // alert($(this).parent().attr("number"));
    });
    function giveDrug(num) {
        var give = $("#drGive").text();
        var drNumber = $("#drNumber").text();

    }
</script>
</html>
