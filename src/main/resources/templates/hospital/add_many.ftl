<!DOCTYPE html>
<html>
<head>
    <title>录入住院信息--中软高科-2015</title>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="/static/Css/bootstrap.css" />
    <link rel="stylesheet" type="text/css" href="/static/Css/bootstrap-responsive.css" />
    <link rel="stylesheet" type="text/css" href="/static/Css/style.css" />
    <script type="text/javascript" src="/static/Js/jquery.js"></script>
    <script type="text/javascript" src="/static/Js/jquery.sorted.js"></script>
    <script type="text/javascript" src="/static/Js/bootstrap.js"></script>
    <script type="text/javascript" src="/static/Js/ckform.js"></script>
    <script type="text/javascript" src="/static/Js/common.js"></script>
    <script type="text/javascript" src="/static/Js/ckeditor/ckeditor.js"></script>

    <style type="text/css">
        body {
            padding-bottom: 40px;
        }
        .sidebar-nav {
            padding: 9px 0;
        }
        @media (max-width: 980px) {
            /* Enable use of floated navbar text */
            .navbar-text.pull-right {
                float: none;
                padding-left: 5px;
                padding-right: 5px;
            }
        }

    </style>
    <#--<script type="text/javascript">
    $(function () {       
		$('#backid').click(function(){
				window.location.href="index.html";
		 });
    });
    </script>-->
</head>
<body>
<form action="/beh/doAddMany" method="post" class="definewidth m20">
<table class="table table-bordered table-hover definewidth m10">
    <tr>
        <input type="hidden" name="behpid" value="${many.behpid}" />
        <input type="hidden" name="bhid" value="${many.behospitalinfo.bhid}" />
        <td width="10%" class="tableleft">病历号</td>
        <td><#if many.behpid??>${many.behpid}</#if></td>
    </tr>
    <tr>
        <td width="10%" class="tableleft">姓名</td>
        <td><#if many.hosrname??>${many.hosrname}</#if></td>
    </tr>
    <tr>
        <td width="10%" class="tableleft">证件号</td>
        <td><#if many.hosridcard??>${many.hosridcard}</#if></td>
    </tr>
	
	<tr>
        <td width="10%" class="tableleft">余额</td>
        <td><#if many.behospitalinfo.behpmoney??>${many.behospitalinfo.behpmoney}元</#if></td>
    </tr>
	
    <tr>
        <td width="10%" class="tableleft"><font color="red">*</font>缴费押金</td>
        <td><input type="text" name="behpmoney" value=""/>元</td>
    </tr>

    <tr>
        <td colspan="2">
			<center>
				<button type="submit" class="btn btn-primary" type="button">保存</button> &nbsp;&nbsp;
                <button type="button" class="btn btn-success" onclick="history.back(-1)" id="backid">返回列表</button>
			</center>
		</td>
    </tr>
</table>
</form>
</body>
</html>