<!DOCTYPE html>
<html>
<head>
    <title>修改住院信息-2015</title>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="/static/Css/bootstrap.css" />
    <link rel="stylesheet" type="text/css" href="/static/Css/bootstrap-responsive.css" />
    <link rel="stylesheet" type="text/css" href="/static/Css/style.css" />
    <script type="text/javascript" src="/static/Js/jquery.js"></script>
    <script type="text/javascript" src="/static/Js/jquery.sorted.js"></script>
    <script type="text/javascript" src="/static/Js/bootstrap.js"></script>
    <script type="text/javascript" src="/static/Js/ckform.js"></script>
    <script type="text/javascript" src="/static/Js/common.js"></script>
    <script type="text/javascript" src="/static/Js/ckeditor/ckeditor.js"></script>
    <style type="text/css">
        body {
            padding-bottom: 40px;
        }
        .sidebar-nav {
            padding: 9px 0;
        }
        @media (max-width: 980px) {
            /* Enable use of floated navbar text */
            .navbar-text.pull-right {
                float: none;
                padding-left: 5px;
                padding-right: 5px;
            }
        }
    </style>
    <#--<script type="text/javascript">
    $(function () {       
		$('#backid').click(function(){//返回首页
				window.location.href="index.html";
		 });
    });
    </script>-->
</head>
<body>
<form action="/beh/doUpdate" method="get" class="definewidth m20">
<table class="table table-bordered table-hover definewidth m10">
    <tr>
        <input type="hidden" name="hosrid" value="${info.hosrid}" />
        <td width="10%" class="tableleft">病历号</td>
        <td><#if info.behospitalinfo.behpid??>${info.behospitalinfo.behpid}</#if></td>
    </tr>
    <tr>
        <td width="10%" class="tableleft">姓名</td>
        <td><#if info.hosrname??>${info.hosrname}</#if></td>
    </tr>
    <tr>
        <td width="10%" class="tableleft">证件类型</td>
        <td>身份证</td>
    </tr>
    <tr>
        <td width="10%" class="tableleft">证件号</td>
        <td><#if info.hosridcard??>${info.hosridcard}</#if></td>
    </tr>
    <tr>
        <td width="10%" class="tableleft">社保号</td>
        <td><#if info.hosrssnum??>${info.hosrssnum}</#if></td>
    </tr>
    <tr>
        <td width="10%" class="tableleft">联系电话</td>
        <td><#if info.hosrphone??>${info.hosrphone}</#if></td>
    </tr>
    <tr>
        <td width="10%" class="tableleft">是否自费</td>
        <td>
            <input type="radio" name="hosrvisit" value="${info.hosrvisit}" <#if info.hosrvisit==1>checked</#if> />否&nbsp;&nbsp;&nbsp;
            <input type="radio" name="hosrvisit" value="${info.hosrvisit}" <#if info.hosrvisit==0>checked</#if> />是
        </td>
    </tr>
    <tr>
        <td width="10%" class="tableleft">性别</td>
        <td>
            <input type="radio" name="hosrsex" value="${info.hosrsex}" <#if info.hosrsex==1>checked</#if> />男&nbsp;&nbsp;&nbsp;
            <input type="radio" name="hosrsex" value="${info.hosrsex}" <#if info.hosrsex==0>checked</#if> />女</td>
    </tr>
    <tr>
        <td width="10%" class="tableleft">年龄</td>
        <td><#if info.hosrage??>${info.hosrage}</#if></td>
    </tr>
    <tr>
        <td width="10%" class="tableleft">初复诊</td>
        <td>
            <input type="radio" name="hosrLookdoctor" value="${info.hosrLookdoctor}" <#if info.hosrLookdoctor==0>checked</#if> />初诊&nbsp;&nbsp;&nbsp;
            <input type="radio" name="hosrLookdoctor" value="${info.hosrLookdoctor}" <#if info.hosrLookdoctor==1>checked</#if> />复诊
        </td>
    </tr>
    <tr>
        <td width="10%" class="tableleft">所挂科室</td>
        <td>
            <select name="hrDepartId">
                <#list depart as d>
                    <option value="${d.departid}" <#--<#if d.departid==info.hrDepartId>selected="true"</#if>-->>${d.departname}</option>
                </#list>
            </select>

        </td>
    </tr>
    <tr>
        <td width="10%" class="tableleft">指定医生</td>
        <td>
            <select name="hrDocId">
                <#list doctor as d>
                    <option value="${d.did}" <#--<#if d.did==info.hrDocId>selected="true"</#if>-->>${d.dname}</option>
                </#list>
            </select>
        </td>
    </tr>
	<tr>
        <td width="10%" class="tableleft">备注</td>
        <td><#if info.hosrremark??>${info.hosrremark}</#if></td>
	</tr>
    <tr>
        <td width="10%" class="tableleft">护理</td>
        <td>
            <select name="hehpnurse">
                <#list nurse as n>
                    <option value="${n.hehpnurse}" <#--<#if n.behpid==info.behpid>selected="true"</#if>-->>${n.hehpnurse}</option>
                </#list>
            </select>
        </td>
    </tr>
    <tr>
        <td width="10%" class="tableleft">床位号</td>
        <td>
            <select name="behpbed">
                <#list nurse as n>
                    <option value="${n.behpbed}" <#--<#if n.behpid==info.behpid>selected="true"</#if>-->>${n.behpbed}</option>
                </#list>
            </select>
        </td>
    </tr>
    <tr>
        <td width="10%" class="tableleft">病情</td>
        <td><textarea name="behpillness"><#if info.behospitalinfo.behpillness??>${info.behospitalinfo.behpillness}</#if></textarea></td>
    </tr>
    <tr>
        <td colspan="2">
			<center>
				<button type="submit" class="btn btn-primary">保存</button> &nbsp;&nbsp;
                <button type="button" class="btn btn-success" onclick="history.back(-1)" id="backid">返回列表</button><#-- name="backid"-->
			</center>
		</td>
    </tr>
</table>
</form>
</body>
</html>