<!DOCTYPE html>
<html>
<head>
    <title></title>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="/static/Css/bootstrap.css" />
    <link rel="stylesheet" type="text/css" href="/static/Css/bootstrap-responsive.css" />
    <link rel="stylesheet" type="text/css" href="/static/Css/style.css" />
    <script type="text/javascript" src="/static/Js/jquery.js"></script>
    <script type="text/javascript" src="/static/Js/jquery.sorted.js"></script>
    <script type="text/javascript" src="/static/Js/bootstrap.js"></script>
    <script type="text/javascript" src="/static/Js/ckform.js"></script>
    <script type="text/javascript" src="/static/Js/common.js"></script>
    <script type="text/javascript" src="/static/Js/js.js"></script>
    <style type="text/css">
        body {
            padding-bottom: 40px;
        }
        .sidebar-nav {
            padding: 9px 0;
        }

        @media (max-width: 980px) {
            /* Enable use of floated navbar text */
            .navbar-text.pull-right {
                float: none;
                padding-left: 5px;
                padding-right: 5px;
            }
        }


    </style>
    
     <script type="text/javascript">
     $(function () {
		$('#newNav').click(function(){
				window.location.href="/role/add";
		 });
    });
	
	
    function checkall(){
			var alls=document.getElementsByName("check");
			var ch=document.getElementById("checkall");
			if(ch.checked){
				for(var i=0;i<alls.length;i++){
					alls[i].checked=true;	
				}	
			}else{
				for(var i=0;i<alls.length;i++){
					alls[i].checked=false;	
				}	
			}
		}
		function delAll(){
			var alls=document.getElementsByName("check");
			var ids=new Array();
			for(var i=0;i<alls.length;i++){
				if(alls[i].checked){
					ids.push(alls[i].value);
				}		
			}
			if(ids.length>0){
				if(confirm("确认删除?")){
					alert("删除成功!");
				}
			}else{
				alert("请选中要删除的项");
			}
		}
    </script>   
    
</head>
<body>
<form class="form-inline definewidth m20" action="index.html" method="get">  
    角色名称：
    <input type="text" name="rolename" id="rolename"class="abc input-default" placeholder="" value="">&nbsp;&nbsp;  
    <button type="submit" class="btn btn-primary">查询</button>
</form>
<table class="table table-bordered table-hover definewidth m10" >
    <thead>

    <tr>
    	<th width="5%"><input type="checkbox" id="checkall" onChange="checkall();"></th>
        <th>角色名称</th>
        <th>状态</th>
        <th width="10%">操作</th>
    </tr>
    </thead>
        <#list roleList as role>
	     <tr>
            <input type="hidden" value="${role.roleid}"/>
         	<td style="vertical-align:middle;"><input type="checkbox" name="check" value="1"></td>
            <td>${role.rolename}</td>
            <td>
                <#if role.rolestate==1>已启用<#else>未启用</#if>
            </td>
            <td>
                <a href="/role/update?roleid=${role.roleid}">编辑</a>&nbsp;&nbsp;&nbsp;
                <a href="#" class="removeRole">删除</a>
            </td>
        </tr>
        </#list>
</table>
        
        
   <table class="table table-bordered table-hover definewidth m10" >
  	<tr>
        <th colspan="5">
            <div class="inline pull-right page">
                <a href='/role/list?pageNum=1'>第一页</a> <#if (page.pageNum-1)&gt;0><a href='/role/list?pageNum=${page.pageNum-1}'>上一页</a> </#if>
                <span class='current'>
                    <a href='/role/list?pageNum=1'>1</a></span><a href='/role/list?pageNum=2'>2</a><a href='/role/list?pageNum=3'>3</a><a href='/role/list?pageNum=4'>4</a><a href='/role/list?pageNum=5'>5</a>
                <#if (page.pageNum+1)<=page.pages><a href='/role/list?pageNum=${page.pageNum+1}'>下一页</a></#if>
                <a href='/role/list?pageNum=${page.pages}'>最后一页</a>&nbsp;&nbsp;&nbsp;
                共<span class='current'>${page.total}</span>条记录<span class='current'> ${page.pageNum}/${page.pages}</span>页
            </div>
            <div>
                <button type="button" class="btn btn-success" id="newNav">添加角色</button>&nbsp;&nbsp;&nbsp;
                <button type="button" class="btn btn-success" id="delPro" onClick="delAll();">删除选中</button>
            </div>
        </th>
    </tr>
  </table>
<!--点击删除按钮后弹出的页面-->
<div class="zhezhao"></div>
<div class="remove" id="removeRole">
    <div class="removerChid">
        <h2>提示</h2>
        <div class="removeMain">
            <p>你确定要删除该用户吗？</p>
            <a href="/role/remove?roleid=" id="yes">确定</a>
            <a href="#" id="no">取消</a>
        </div>
    </div>
</div>
</body>
<script>
    $(".removeRole").click(function () {
        var roleid = $(this).parents("tr").children().eq(0).val();
        /*alert(roleid);*/
        var oldUrl = $("#yes").attr("href");
        var newUrl = oldUrl +roleid;
        $("#yes").attr("href",newUrl);
        $('.zhezhao').css('display', 'block');
        $('#removeRole').fadeIn();
    });
</script>

</html>