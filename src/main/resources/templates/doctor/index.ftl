<!DOCTYPE html>
<html>
<head>
    <title>门诊医生--中软高科-2015</title>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="/static/Css/bootstrap.css" />
    <link rel="stylesheet" type="text/css" href="/static/Css/bootstrap-responsive.css" />
    <link rel="stylesheet" type="text/css" href="/static/Css/style.css" />
    <script type="text/javascript" src="/static/Js/jquery.js"></script>
    <script type="text/javascript" src="/static/Js/jquery.sorted.js"></script>
    <script type="text/javascript" src="/static/Js/bootstrap.js"></script>
    <script type="text/javascript" src="/static/Js/ckform.js"></script>
    <script type="text/javascript" src="/static/Js/common.js"></script>

    <style type="text/css">
        body {
            padding-bottom: 40px;
        }
        .sidebar-nav {
            padding: 9px 0;
        }

        @media (max-width: 980px) {
            /* Enable use of floated navbar text */
            .navbar-text.pull-right {
                float: none;
                padding-left: 5px;
                padding-right: 5px;
            }
        }


    </style>
    <script type="text/javascript">
	 $(function () {
		$('#newNav').click(function(){
				window.location.href="/doctor/add";
		 });
    });
	
    	function checkall(){
			var alls=document.getElementsByName("check");
			var ch=document.getElementById("checkall");
			if(ch.checked){
				for(var i=0;i<alls.length;i++){
					alls[i].checked=true;	
				}	
			}else{
				for(var i=0;i<alls.length;i++){
					alls[i].checked=false;	
				}	
			}
		}
		function doExcel(){
			var alls=document.getElementsByName("check");
			var ids=new Array();
			for(var i=0;i<alls.length;i++){
				if(alls[i].checked){
					ids.push(alls[i].value);
				}		
			}
			if(ids.length>0){
				if(confirm("确认操作?")){
                    window.location.href="/doctor/daoexcel?rids="+ids;
				}
			}else{
				alert("请选中要操作的项");
			}
		}
    </script>
</head>
<body>

<form action="/doctor/index" method="post" class="definewidth m20">
<table class="table table-bordered table-hover definewidth m10">
    <tr>
        <td width="10%" class="tableleft">医生编号：</td>
        <td><input type="text" name="did" value="<#if did??>${did}</#if>"/></td>
		
        <td width="10%" class="tableleft">医生姓名：</td>
        <td><input type="text" name="dname" value="<#if dname??>${dname}</#if>"/></td>
		
        <td width="10%" class="tableleft">科室：</td>
        <td><input type="text" name="departname" value="<#if departname??>${departname}</#if>"/></td>
    </tr>
    <tr>
		  <td colspan="6"><center>
            <button type="submit" class="btn btn-primary" type="button">查询</button>
            <button type="reset" class="btn btn-primary" type="button">清空</button>
			</center>
        </td>
    </tr>
</table>
</form>
   
<table class="table table-bordered table-hover definewidth m10" >
   <thead>
    <tr>
    	<th><input type="checkbox" id="checkall" onChange="checkall();"></th>
        <th>医生编号</th>
        <th>医生姓名</th>
        <th>入院时间</th>
        <th>所属科室</th>
        <th>操作</th>
    </tr>
    </thead>
    <#list dlist as li>
	     <tr >
         	<td style="vertical-align:middle;"><input type="checkbox" name="check" value="${li.did}"></td>
            <td style="vertical-align:middle;">${li.did}</td>
            <td style="vertical-align:middle;">${li.dname}</td>
            <td style="vertical-align:middle;">${li.dinnerdate?string("yyyy-MM-dd")}</td>
            <td style="vertical-align:middle;">${li.department.departname}</td>
            <td style="vertical-align:middle;"><a href="/doctor/look?did=${li.did}">详情>>></a>&nbsp;&nbsp;&nbsp;<a href="/doctor/edit?did=${li.did}">更改</a></td>
        </tr>
    </#list>

  </table>
  
  <table class="table table-bordered table-hover definewidth m10" >
  	<tr><th colspan="5">  <div class="inline pull-right page">
                <a href='/doctor/index?pageNum=1<#if did??>&did=${did}</#if><#if dname??>&dname=${dname}</#if><#if departname??>&departname=${departname}</#if>' >第一页</a> <#if (page.pageNum-1)&gt;0><a href='/doctor/index?pageNum=${page.pageNum-1}<#if did??>&did=${did}</#if><#if dname??>&dname=${dname}</#if><#if departname??>&departname=${departname}</#if>'>上一页</a> </#if>
                <span class='current'><a href='/doctor/index?pageNum=1<#if did??>&did=${did}</#if><#if dname??>&dname=${dname}</#if><#if departname??>&departname=${departname}</#if>'>1</a>
                </span><a href='/doctor/index?pageNum=2<#if did??>&did=${did}</#if><#if dname??>&dname=${dname}</#if><#if departname??>&departname=${departname}</#if>'>2</a>
                <a href='/doctor/index?pageNum=3<#if did??>&did=${did}</#if><#if dname??>&dname=${dname}</#if><#if departname??>&departname=${departname}</#if>'>3</a><a href='/doctor/index?pageNum=4<#if did??>&did=${did}</#if><#if dname??>&dname=${dname}</#if><#if departname??>&departname=${departname}</#if>'>4</a>
                <a href='/doctor/index?pageNum=5<#if did??>&did=${did}</#if><#if dname??>&dname=${dname}</#if><#if departname??>&departname=${departname}</#if>'>5</a>
                <#if (page.pageNum+1)<=page.pages> <a href='/doctor/index?pageNum=${page.pageNum+1}<#if did??>&did=${did}</#if><#if dname??>&dname=${dname}</#if><#if departname??>&departname=${departname}</#if>' >下一页</a></#if>
                <a href='/doctor/index?pageNum=${page.pages}<#if did??>&did=${did}</#if><#if dname??>&dname=${dname}</#if><#if departname??>&departname=${departname}</#if>' >最后一页</a>
                &nbsp;&nbsp;&nbsp;共<span class='current'>${page.total}</span>条记录<span class='current'> ${page.pageNum}/${page.pages}</span>页
            </div>
		 <div><button type="button" class="btn btn-success" id="newNav">添加新医生</button>
		 <button type="button" class="btn btn-success" id="delPro" onClick="doExcel()">导出Excel</button>
		 </div>
		 </th></tr>
  </table>
  
</body>
</html>
