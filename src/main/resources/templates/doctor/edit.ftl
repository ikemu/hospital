<!DOCTYPE html>
<html>
<head>
    <title>修改医生--中软高科-2015</title>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="/static/Css/bootstrap.css" />
    <link rel="stylesheet" type="text/css" href="/static/Css/bootstrap-responsive.css" />
    <link rel="stylesheet" type="text/css" href="/static/Css/style.css" />
    <script type="text/javascript" src="/static/Js/jquery.js"></script>
    <script type="text/javascript" src="/static/Js/jquery.sorted.js"></script>
    <script type="text/javascript" src="/static/Js/bootstrap.js"></script>
    <script type="text/javascript" src="/static/Js/ckform.js"></script>
    <script type="text/javascript" src="/static/Js/common.js"></script>
    <script type="text/javascript" src="/static/Js/ckeditor/ckeditor.js"></script>
 

    <style type="text/css">
        body {
            padding-bottom: 40px;
        }
        .sidebar-nav {
            padding: 9px 0;
        }

        @media (max-width: 980px) {
            /* Enable use of floated navbar text */
            .navbar-text.pull-right {
                float: none;
                padding-left: 5px;
                padding-right: 5px;
            }
        }


    </style>
    <script type="text/javascript">
    $(function () {       
		$('#backid').click(function(){
				window.location.href="/doctor/index";
		 });
    });
    </script>
</head>
<body>
<form action="/doctor/editDoctor" method="post" class="definewidth m20">
<table class="table table-bordered table-hover definewidth m10">
    <input type="hidden" name="did" value="${doctor.did}"/>
    <tr>
        <td width="10%" class="tableleft"><font color="red">*</font>姓名</td>
        <td><input type="text" name="dname" value="${doctor.dname}"/></td>
    </tr>
    
    <tr>
        <td width="10%" class="tableleft"><font color="red">*</font>身份证号</td>
        <td><input type="text" name="didcard" value="${doctor.didcard}"/></td>
    </tr>
    <tr>
        <td width="10%" class="tableleft"><font color="red">*</font>手机</td>
        <td><input type="text" name="dphone" value="${doctor.dphone}"/></td>
    </tr>
    <tr>
        <td width="10%" class="tableleft">座机</td>
        <td><input type="text" name="dtell" value="${doctor.dtell}"/></td>
    </tr>
    <tr>
        <td width="10%" class="tableleft"><font color="red">*</font>性别</td>
        <td><input type="radio" name="dsex" value="${doctor.dsex}" <#if doctor.dsex==0>checked</#if>/>男&nbsp;&nbsp;&nbsp;<input type="radio" name="dsex" value="${doctor.dsex}" <#if doctor.dsex==1>checked</#if>/>女</td>
    </tr>
    <tr>
        <td width="10%" class="tableleft"><font color="red">*</font>出生年月</td>
        <td><input type="text" name="dbirth" value="${doctor.dbirth?string("yyyy-MM-dd")}"/></td>
    </tr>
    <tr>
        <td width="10%" class="tableleft"><font color="red">*</font>年龄</td>
        <td>${doctor.dage}</td>
    </tr>
    <tr>
        <td width="10%" class="tableleft">电子邮箱</td>
        <td><input type="text" name="demail" value="${doctor.demail}"/></td>
    </tr>
    <tr>
        <td width="10%" class="tableleft"><font color="red">*</font>所属科室</td>
        <td><select name="dDepartId">
                <#list depart as dt>
                <option value="${dt.departid}" <#if dt.departid==doctor.dDepartId> selected="true"</#if> >${dt.departname}</option>
                </#list>
            </select></td>
    </tr>
    <tr>
        <td width="10%" class="tableleft">学历</td>
        <td><select name="dEduId">
                <#list education as ed>
                    <option value="${ed.eduid}" <#if ed.eduid==doctor.dEduId>selected="true"</#if> >${ed.eduname}</option>
                </#list>
            </select></td>
    </tr>
	<tr>
        <td width="10%" class="tableleft">备注</td>
        <td><textarea><#if doctor.dremark??>${doctor.dremark}</#if></textarea></td>
	</tr>
    <tr>
        <td colspan="2">
			<center>
				<button type="submit" class="btn btn-primary" type="button">保存</button> &nbsp;&nbsp;<button type="button" class="btn btn-success" name="backid" id="backid">返回列表</button>
			</center
		</td>
    </tr>
</table>
</form>
</body>
</html>