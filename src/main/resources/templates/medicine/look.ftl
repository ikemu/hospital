<!DOCTYPE html>
<html>
<head>
    <title>查看--中软高科-2015</title>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="/static/Css/bootstrap.css" />
    <link rel="stylesheet" type="text/css" href="/static/Css/bootstrap-responsive.css" />
    <link rel="stylesheet" type="text/css" href="/static/Css/style.css" />
    <script type="text/javascript" src="/static/Js/jquery.js"></script>
    <script type="text/javascript" src="/static/Js/jquery.sorted.js"></script>
    <script type="text/javascript" src="/static/Js/bootstrap.js"></script>
    <script type="text/javascript" src="/static/Js/ckform.js"></script>
    <script type="text/javascript" src="/static/Js/common.js"></script>
    <script type="text/javascript" src="/static/Js/ckeditor/ckeditor.js"></script>
 

    <style type="text/css">
        body {
            padding-bottom: 40px;
        }
        .sidebar-nav {
            padding: 9px 0;
        }

        @media (max-width: 980px) {
            /* Enable use of floated navbar text */
            .navbar-text.pull-right {
                float: none;
                padding-left: 5px;
                padding-right: 5px;
            }
        }


    </style>
    <script type="text/javascript">
    $(function () {       
		$('#backid').click(function(){
				window.location.href="index.html";
		 });
    });
    </script>
</head>
<body>
<form action="index.html" method="post" class="definewidth m20">
<table class="table table-bordered table-hover definewidth m10">
    <tr>
        <td width="10%" class="tableleft">药品编号</td>
        <td>${drug.drugid}</td>
    </tr>
    <tr>
        <td width="10%" class="tableleft">图片</td>
        <td><img style="width: 200px;" src="/static/Images/<#if drug.drugimgname??>${drug.drugimgname}</#if>"/></td>
    </tr>
    <tr>
        <td width="10%" class="tableleft">进价</td>
        <td><#if drug.druginprice??>${drug.druginprice}</#if>元</td>
    </tr>
    <tr>
        <td width="10%" class="tableleft">售价</td>
        <td><#if drug.drugsaleprice??>${drug.drugsaleprice}</#if>元</td>
    </tr>
    <tr>
        <td width="10%" class="tableleft">药品名称</td>
        <td>${drug.drugname}</td>
    </tr>
    <tr>
        <td width="10%" class="tableleft">药品类型</td>
        <td><#if drug.drugtype="0">处方药<#elseif drug.drugtype=="1">中药<#else>西药</#if></td>
    </tr>
    <tr>
        <td width="10%" class="tableleft">简单描述</td>
        <td><#if drug.drugsimpledesc??>${drug.drugsimpledesc}</#if></td>
    </tr>

    <tr>
        <td width="10%" class="tableleft">过期日期</td>
        <td><#if drug.drugexpdate??>${drug.drugexpdate?string("yyyy年MM月dd日")}</#if></td>
    </tr>
    <tr>
        <td width="10%" class="tableleft">详细描述</td>
        <td><#if drug.drugdesc??>${drug.drugdesc}</#if></td>
    </tr>
    <tr>
        <td width="10%" class="tableleft">生产厂商</td>
        <td><#if drug.drugmakefirm??>${drug.drugmakefirm}</#if></td>
    </tr>
    <tr>
        <td width="10%" class="tableleft">服用说明</td>
        <td><#if drug.drugeatexplain??>${drug.drugeatexplain}</#if></td>
    </tr>
    <tr>
        <td width="10%" class="tableleft">库存</td>
        <td><#if drug.drugnum??>${drug.drugnum}</#if>袋</td>
    </tr>
    <tr>
        <td width="10%" class="tableleft">剩余量</td>
        <td><#if drug.druglastnum??>${drug.druglastnum}</#if>袋</td>
    </tr>
	<tr>
        <td width="10%" class="tableleft">备注</td>
        <td><#if drug.drugremark??>${drug.drugremark}</#if></td>
	</tr>
    <tr>
        <td colspan="2">
			<center>
				<button type="button" class="btn btn-success" name="backid" id="backid">返回列表</button>
			</center
		</td>
    </tr>
</table>
</form>
</body>
</html>