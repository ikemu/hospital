<!DOCTYPE html>
<html>
<head>
    <title>添加药品--中软高科-2015</title>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="/static/Css/bootstrap.css" />
    <link rel="stylesheet" type="text/css" href="/static/Css/bootstrap-responsive.css" />
    <link rel="stylesheet" type="text/css" href="/static/Css/style.css" />
    <script type="text/javascript" src="/static/Js/jquery.js"></script>
    <script type="text/javascript" src="/static/Js/jquery.sorted.js"></script>
    <script type="text/javascript" src="/static/Js/bootstrap.js"></script>
    <script type="text/javascript" src="/static/Js/ckform.js"></script>
    <script type="text/javascript" src="/static/Js/common.js"></script>
    <script type="text/javascript" src="/static/Js/ckeditor/ckeditor.js"></script>
 

    <style type="text/css">
        body {
            padding-bottom: 40px;
        }
        .sidebar-nav {
            padding: 9px 0;
        }

        @media (max-width: 980px) {
            /* Enable use of floated navbar text */
            .navbar-text.pull-right {
                float: none;
                padding-left: 5px;
                padding-right: 5px;
            }
        }


    </style>
    <script type="text/javascript">
    $(function () {       
		$('#backid').click(function(){
				window.location.href="index.html";
		 });
    });
    </script>
</head>
<body>
<form action="/medicine/update" method="post" class="definewidth m20">
<table class="table table-bordered table-hover definewidth m10">
    <#--<input type="hidden" name="drugid" value="${drug.drugid}">-->
    <tr>
        <td width="10%" class="tableleft"><font color="red">*</font>药品编号</td>
        <td><input type="text" name="drugid" <#if drug.drugid??>value="${drug.drugid}"</#if> /></td>
    </tr>
    <tr>
        <td width="10%" class="tableleft">图片</td>
        <td>
            <#--<div class="forminfoRight">-->
                    <#--<div class="pic">-->
                        <#--<img id="picimg" src="/static/images/img06.png" />-->
                        <#--<input class="picfile" type="file" name="pic" />-->
                    <#--</div>-->
            <#--</div>-->
            <input type="hidden" id="drugimgname" name="drugimgname" <#if drug.drugimgname??>value="${drug.drugimgname}"</#if>/>

            <#--<input type="file" class="pic" onclick="updatepic()"-->
        </td>
    </tr>
    <tr>
        <td width="10%" class="tableleft"><font color="red">*</font>进价</td>
        <td><input type="text" name="druginprice" <#if drug.druginprice??>value="${drug.druginprice}"</#if>/></td>
    </tr>
    <tr>
        <td width="10%" class="tableleft"><font color="red">*</font>售价</td>
        <td><input type="text" name="drugsaleprice" <#if drug.drugsaleprice??>value="${drug.drugsaleprice}"</#if>/></td>
    </tr>
    <tr>
        <td width="10%" class="tableleft"><font color="red">*</font>药品名称</td>
        <td><input type="text" name="drugname" <#if drug.drugname??>value="${drug.drugname}"</#if>/></td>
    </tr>
    <tr>
        <td width="10%" class="tableleft"><font color="red">*</font>药品类型</td>
        <td>
            <input type="radio" name="drugtype" value="0" <#if drug.drugtype="0">checked</#if> />处方药&nbsp;&nbsp;&nbsp;
            <input type="radio" name="drugtype" value="1" <#if drug.drugtype="1">checked</#if>/>中药&nbsp;&nbsp;&nbsp;
            <input type="radio" name="drugtype" value="2" <#if drug.drugtype="2">checked</#if> />西药
        </td>
    </tr>
    <tr>
        <td width="10%" class="tableleft">简单描述</td>
        <td><input type="text" name="drugsimpledesc" <#if drug.drugsimpledesc??>value="${drug.drugsimpledesc}"</#if>/></td>
    </tr>

    <tr>
        <td width="10%" class="tableleft"><font color="red">*</font>保质期</td>
        <td><input type="text" name="pname" value="36"/>月</td>
    </tr>
    <tr>
        <td width="10%" class="tableleft">详细描述</td>
        <td>
            <textarea name="drugdesc"><#if drug.drugdesc??>${drug.drugdesc}</#if></textarea>
        </td>
    </tr>
    <tr>
        <td width="10%" class="tableleft">生产厂商</td>
        <td><textarea name="drugmakefirm"><#if drug.drugmakefirm??>${drug.drugmakefirm}</#if></textarea></td>
    </tr>
    <tr>
        <td width="10%" class="tableleft"><font color="red">*</font>服用说明</td>
        <td><input type="text" name="drugeatexplain" <#if drug.drugeatexplain??>value="${drug.drugeatexplain}"</#if>/></td>
    </tr>
   
	<tr>
        <td width="10%" class="tableleft">备注</td>
        <td><textarea name="drugremark"><#if drug.drugremark??>${drug.drugremark}</#if></textarea></td>
	</tr>
    <tr>
        <td colspan="2">
			<center>
				<button type="submit" >保存</button> &nbsp;&nbsp;<button type="button" class="btn btn-success" name="backid" id="backid">返回列表</button>
			</center
		</td>
    </tr>
</table>
</form>

</body>
</html>