<!DOCTYPE html>
<html>
<head>
    <title>药品查询--中软高科-2015</title>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="/static/Css/bootstrap.css" />
    <link rel="stylesheet" type="text/css" href="/static/Css/bootstrap-responsive.css" />
    <link rel="stylesheet" type="text/css" href="/static/Css/style.css" />
    <script type="text/javascript" src="/static/Js/jquery.js"></script>
    <script type="text/javascript" src="/static/Js/jquery.sorted.js"></script>
    <script type="text/javascript" src="/static/Js/bootstrap.js"></script>
    <script type="text/javascript" src="/static/Js/ckform.js"></script>
    <script type="text/javascript" src="/static/Js/common.js"></script>

    <style type="text/css">
        body {
            padding-bottom: 40px;
        }
        .sidebar-nav {
            padding: 9px 0;
        }

        @media (max-width: 980px) {
            /* Enable use of floated navbar text */
            .navbar-text.pull-right {
                float: none;
                padding-left: 5px;
                padding-right: 5px;
            }
        }


    </style>
    <script type="text/javascript">
	 $(function () {
		$('#newNav').click(function(){
				window.location.href="add.html";
		 });
    });
	
    	function checkall(){
			var alls=document.getElementsByName("check");
			var ch=document.getElementById("checkall");
			if(ch.checked){
				for(var i=0;i<alls.length;i++){
					alls[i].checked=true;	
				}	
			}else{
				for(var i=0;i<alls.length;i++){
					alls[i].checked=false;	
				}	
			}
		}
		function delAll(){
			var alls=document.getElementsByName("check");
			var ids=new Array();
			for(var i=0;i<alls.length;i++){
				if(alls[i].checked){
					ids.push(alls[i].value);
				}		
			}
			if(ids.length>0){
				if(confirm("确认操作?")){
					alert("成功!");
				}
			}else{
				alert("请选中要操作的项");
			}
		}
    </script>
</head>
<body>

<form action="/medicine" method="post" class="definewidth m20">
<table class="table table-bordered table-hover definewidth m10">
    <tr>
        <td width="10%" class="tableleft">药品名称：</td>
        <td><input type="text" name="drugName" <#if condition.drugName??>value="${condition.drugName}"</#if>/></td>
		
        <td width="10%" class="tableleft">药品类型：</td>
        <td>
            <select name="drugType">
                <option value="8" <#if condition.drugType="8">selected="selected"</#if> >全部</option>
                <option value="0" <#if condition.drugType="0">selected="selected"</#if> >处方药</option>
                <option value="1" <#if condition.drugType="1">selected="selected"</#if> >中药</option>
                <option value="2" <#if condition.drugType="2">selected="selected"</#if> >西药</option>
            </select>
        </td>
    </tr>
    <tr>
		
		
		  <td colspan="4">
			<center>
				<button type="submit" class="btn btn-primary" type="button">查询</button> 
				<button type="submit" class="btn btn-primary" type="button">清空</button> 
			</center>
        </td>
    </tr>
</table>
</form>
   
<table class="table table-bordered table-hover definewidth m10" >
   <thead>
    <tr>
    	<th><input type="checkbox" id="checkall" onChange="checkall();"></th>
        <th>药品编号</th>
        <th>药品名称</th>
        <th>药品类型</th>
        <th>简单描述</th>
        <th>状态</th>
        <th>剩余量</th>
        <th>操作</th>
    </tr>
    </thead>
        <#if drugList??>
            <#list drugList as drug>
                <tr >
                    <td style="vertical-align:middle;"><input type="checkbox" class="checkDrug" name="check" value="${drug.drugid}"></td>
                    <td style="vertical-align:middle;"><#if drug.drugid??>${drug.drugid}</#if></td>
                <td style="vertical-align:middle;"><#if drug.drugname??>${drug.drugname}</#if></td>
                <td style="vertical-align:middle;"><#if drug.drugtype="0">处方药<#elseif drug.drugtype=="1">中药<#else>西药</#if></td>
                <td style="vertical-align:middle;"><#if drug.drugsimpledesc??>${drug.drugsimpledesc}</#if></td>
                <td style="vertical-align:middle;"><#if drug.drugstate==0>销售中<#else>禁止销售</#if></td>
                    <td style="vertical-align:middle;"><#if drug.druglastnum??>${drug.druglastnum}<#else>0</#if>袋</td>
                    <td style="vertical-align:middle;">
                        <a href="/medicine/updateDrug?id=${drug.drugid}">更改</a>&nbsp;&nbsp;&nbsp;
                        <a href="/medicine/addRepertory?id=${drug.drugid}">修改库存</a>&nbsp;&nbsp;&nbsp;
                        <a href="/medicine/look?id=<#if drug.drugid??>${drug.drugid}</#if>">详情>>></a>
                    </td>
                </tr>
            </#list>
        </#if>

	   
  </table>
  
  <table class="table table-bordered table-hover definewidth m10" >
  	<tr><th colspan="5">  <div class="inline pull-right page">
                <#if page.pageNum!=1>
                    <a href='/medicine?pageNum=1<#if condition.drugName??>&drugName=${condition.drugName}</#if><#if condition.drugType??>&drugType=${condition.drugType}</#if>' >首页</a>
                    <a href="/medicine?pageNum=${page.pageNum-1}<#if condition.drugName??>&drugName=${condition.drugName}</#if><#if condition.drugType??>&drugType=${condition.drugType}</#if>">上一页</a>
                <#else >
                    <a href="/medicine?pageNum1">首页</a>
                </#if>
                <#if page.pages lte 5 >
                    <#list 1..page.pages as i>
                        <a <#if page.pageNum==i>class='current' </#if> href="/medicine?pageNum=${i}<#if condition.drugName??>&drugName=${condition.drugName}</#if><#if condition.drugType??>&drugType=${condition.drugType}</#if>"  >${i}</a>
                    </#list>
                </#if>
                <#if page.pageNum lte 3 && page.pages gte 5>
                    <#list 1..5 as i>
                        <a <#if page.pageNum==i>class='current'</#if>  href="/medicine?pageNum=${i}<#if condition.drugName??>&drugName=${condition.drugName}</#if><#if condition.drugType??>&drugType=${condition.drugType}</#if>"  >${i}</a>
                    </#list>
                </#if>
                <#if page.pageNum gt 3 && page.pageNum+2 lte page.pages && page.pages gte 5>
                    <#list page.pageNum-2..page.pageNum+2 as i>
                        <a <#if page.pageNum==i>class='current'</#if> href="/medicine?pageNum=${i}<#if condition.drugName??>&drugName=${condition.drugName}</#if><#if condition.drugType??>&drugType=${condition.drugType}</#if>"  >${i}</a>
                    </#list>
                </#if>
                <#if  page.pageNum gt 3 && page.pageNum+2 gt page.pages && page.pages gte 5>
                    <#list page.pages-4..page.pages as i>
                        <a <#if page.pageNum==i>class='current'</#if> href="/medicine?pageNum=${i}<#if condition.drugName??>&drugName=${condition.drugName}</#if><#if condition.drugType??>&drugType=${condition.drugType}</#if>"  >${i}</a>
                    </#list>
                </#if>

                <#if page.pageNum!=page.pages>
                    <a href='/medicine?pageNum=${page.pageNum+1}<#if condition.drugName??>&drugName=${condition.drugName}</#if><#if condition.drugType??>&drugType=${condition.drugType}</#if>' >下一页</a>
                    <a href="/medicine?pageNum=${page.pages}<#if condition.drugName??>&drugName=${condition.drugName}</#if><#if condition.drugType??>&drugType=${condition.drugType}</#if>">尾页</a>
                <#else >
                    <a href="/medicine?pageNum=${page.pages}<#if condition.drugName??>&drugName=${condition.drugName}</#if><#if condition.drugType??>&drugType=${condition.drugType}</#if>">尾页</a>
                </#if>
                &nbsp;&nbsp;&nbsp;共<span class='current'>${page.total}</span>条记录<span class='current'> ${page.pageNum}/${page.pages} </span>页

          <#--<a href='#' >第一页</a> <a href='#'>上一页</a>-->
                <#--<span class='current'>1</span><a href='#'>2</a><a href='/chinapost/index.php?m=Label&a=index&p=3'>3</a><a href='#'>4</a><a href='#'>5</a>  <a href='#' >下一页</a> <a href='#' >最后一页</a>-->
		  <#--&nbsp;&nbsp;&nbsp;共<span class='current'>32</span>条记录<span class='current'> 1/33 </span>页-->
		  </div>
		 <div>
             <a href="/medicine/addDrug"><button type="button" class="btn btn-success" id="newNav">添加新药</button></a>
             <input type="hidden" id="pageNum" value="${page.pageNum}">
			<button type="button" class="btn btn-success" id="exportExcel">导出Excel</button>
		 </div>
		 
		 </th></tr>
  </table>
  
</body>
<script src="/static/Js/jquery-1.12.4.js"></script>
<script>
    $("#exportExcel").click(function () {
        if (confirm("确定要导出吗?")){
            var checkDrug = $(".checkDrug");
            var ids = new Array();
            for (var i = 0; i < checkDrug.length; i++) {
                if (checkDrug[i].checked)
                    ids.push(parseInt(checkDrug[i].value));
            }

            if (ids.length > 0){
                window.location.href="/medicine/excel?ids="+ids;
            } else {
                alert("请选择要导出的项!")
            }

        }

    });
</script>
</html>
