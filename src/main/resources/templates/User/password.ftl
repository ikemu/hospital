<!DOCTYPE html>
<html>
<head>
    <title></title>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="/static/Css/bootstrap.css" />
    <link rel="stylesheet" type="text/css" href="/static/Css/bootstrap-responsive.css" />
    <link rel="stylesheet" type="text/css" href="/static/Css/style.css" />
    <script type="text/javascript" src="/static/Js/jquery.js"></script>
	<script type="text/javascript" src="/static/Js/jquery.sorted.js"></script>
	<script type="text/javascript" src="/static/Js/bootstrap.js"></script>
	<script type="text/javascript" src="/static/Js/ckform.js"></script>
	<script type="text/javascript" src="/static/Js/common.js"></script>

</head>
<script type="text/javascript">
    window.onload = function(){//页面加载完之后触发事件，执行函数
        var inp = document.getElementsByTagName("input");
        var inp1 = inp[0];//原密码
        var inp2 = inp[1];//新密码
        var inp3 = inp[2];//确认密码
        var inp4 = inp[3];//保存
        inp1.onblur = function(){
            var pwd = this.value;
            if(pwd==""&&pwd==null){
                $("#msg").css("display","block").html("请输入原密码！").slideDown();
                timerMove($("#msg"));
                this.value = "";
                this.focus();
            }
        };
        inp2.onblur = function(){
            var reg = /^[0-9a-zA-Z]{6,12}$/;
            var pwd = this.value;
            var flag = reg.test(pwd);
            if(pwd&&!flag){
                $("#msg").css("display","block").html("请输入6-12位密码！").slideDown();
                timerMove($("#msg"));
                this.value = "";
                this.focus();
            }
        };
        inp3.onblur = function(){
			var rep = this.value;
            var pwd = inp2.value;
            if(rep&&rep!==pwd){
                $("#msg").css("display","block").html("两次密码不一致！").slideDown();
                timerMove($("#msg"));
                this.value = "";
                this.focus();
            }
        };

        inp4.onclick = function(){//点击保存
            if(inp1.value==""){
                $("#msg").css("display","block").html("原密码不能为空！").slideDown();
                timerMove($("#msg"));
                return false;
            }else if(inp2.value==""){
                $("#msg").css("display","block").html("新密码不能为空！").slideDown();
                timerMove($("#msg"));
                return false;
            }else if(inp3.value==""){
                $("#msg").css("display","block").html("请确认密码！").slideDown();
                timerMove($("#msg"));
                return false;
            }
        }

        if($("#msg").innerHTML != ""){
            $("#msg").css("display","block").slideDown();
            timerMove($("#msg"));
        }
        //定时器
        function timerMove($this){
            var time=3;
            var timer=setInterval(function(){
                time--;
                if(time<0){
                    clearInterval(timer);
                    time=3;
                    $("#msg").css("display","none").slideUp();
                }
            },1000);
        }
    }
</script>
<body>
<form class="form-inline definewidth m20" action="/user/updatePassword" method="post">
   <table>
		<tr>
			<td align="right"><font color="red">*</font>原密码:</td><td><input type="password" name="oldPassword" /></td>
		</tr>
		<tr>
			<td align="right"><font color="red">*</font>新密码:</td><td><input type="password" name="newPassword"/></td>
		</tr>
		<tr>
			<td align="right"><font color="red">*</font>确认密码:</td><td><input type="password"/></td>
		</tr>
		<tr>
			<td colspan="2" align="center"><br/>
				<div id="msg" style="text-align: center;color: blueviolet;display: none;font-size: 14px;font-weight: bold;">
					<#if msg??>${msg}</#if>
				</div>
				<input type="submit" value="保存" class="btn btn-primary"/>
			</td>
		</tr>
   </table>
</form>
</body>
</html>