<!DOCTYPE html>
<html>
<head>
    <title></title>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="/static/Css/bootstrap.css" />
    <link rel="stylesheet" type="text/css" href="/static/Css/bootstrap-responsive.css" />
    <link rel="stylesheet" type="text/css" href="/static/Css/style.css" />
    <script type="text/javascript" src="/static/Js/jquery.js"></script>
    <script type="text/javascript" src="/static/Js/jquery.sorted.js"></script>
    <script type="text/javascript" src="/static/Js/bootstrap.js"></script>
    <script type="text/javascript" src="/static/Js/ckform.js"></script>
    <script type="text/javascript" src="/static/Js/common.js"></script>

 

    <style type="text/css">
        body {
            padding-bottom: 40px;
        }
        .sidebar-nav {
            padding: 9px 0;
        }

        @media (max-width: 980px) {
            /* Enable use of floated navbar text */
            .navbar-text.pull-right {
                float: none;
                padding-left: 5px;
                padding-right: 5px;
            }
        }


    </style>
</head>
<body>
<form action="/user/updateUser" method="post" class="definewidth m20">

    <input type="hidden" name="userid" value="${users.userid}" />
    <table class="table table-bordered table-hover definewidth m10">

        <tr>
            <td width="10%" class="tableleft">
            <font color="red">*</font>登录名</td>
            <td>${users.username}/td>
        </tr>
        <tr>
            <td class="tableleft"><font color="red">*</font>密码</td>
            <td><input type="password" name="password" value="${users.password}"/></td>
        </tr>
        <tr>
            <td class="tableleft"><font color="red">*</font>真实姓名</td>
            <td><input type="text" name="name" value="<#if users.name??>${users.name}<#else>空</#if>"/></td>
        </tr>
        <tr>
            <td class="tableleft">邮箱</td>
            <td><input type="text" name="useremail" value="<#if users.useremail??>${users.useremail}<#else>空</#if>"/></td>
        </tr>
        <tr>
            <td class="tableleft"><font color="red">*</font>状态</td>
            <td>
                <input type="radio" name="userstate" value="1" checked/> 启用
                <input type="radio" name="userstate" value="0" /> 禁用

            </td>
        </tr>
        <tr>
            <td class="tableleft"><font color="red">*</font>角色</td>
            <td>
                <select name="role">
                    <option value="">--请选择--</option>
                <#list rlist as rl>
                    <option value="${rl.roleid}">${rl.rolename}</option>
                </#list>
                </select>
        	</td>
        </tr>
        <tr>
            <td class="tableleft"></td>
            <td>
                <button type="submit" class="btn btn-primary">更新</button>&nbsp;&nbsp;
                <button type="button" class="btn btn-success" name="backid" id="backid">返回列表</button>
            </td>
        </tr>
    </table>
</form>
</body>
</html>
<script>
    $(function () {       
		$('#backid').click(function(){
				window.location.href="/user/userList";
		 });
    });
</script>