<!DOCTYPE html>
<html>
<head>
    <base href="<%=basePath%>">

    <title></title>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="/static/Css/bootstrap.css" />
    <link rel="stylesheet" type="text/css" href="/static/Css/bootstrap-responsive.css" />
    <link rel="stylesheet" type="text/css" href="/static/Css/style.css" />
    <script type="text/javascript" src="/static/Js/jquery.js"></script>
    <script type="text/javascript" src="/static/Js/jquery.sorted.js"></script>
    <script type="text/javascript" src="/static/Js/bootstrap.js"></script>
    <script type="text/javascript" src="/static/Js/ckform.js"></script>
    <script type="text/javascript" src="/static/Js/common.js"></script>



    <style type="text/css">
        body {
            padding-bottom: 40px;
        }

        .sidebar-nav {
            padding: 9px 0;
        }

        @media ( max-width : 980px) {
            /* Enable use of floated navbar text */
            .navbar-text.pull-right {
                float: none;
                padding-left: 5px;
                padding-right: 5px;
            }
        }
    </style>
    <script>
        //校验用户名
        function ckUsername() {
            var username = document.getElementById("username").value;
            if (username.trim().length==0) {
                document.getElementById("spanusername").style.color = "red";
                document.getElementById("spanusername").innerHTML = "用户名不能为空!";
                return false;
            }
            var reg = /^[a-zA-Z][a-zA-Z0-9_]{4,15}$/;
            if(reg.test(username)==false){
                document.getElementById("spanusername").style.color = "red";
                document.getElementById("spanusername").innerHTML = "字母开头，允许5-16字节，允许字母数字下划线!";
                return false;
            }



            //ajax:发送异步请求到后台Servlet 上 判断用户账号是否存在于数据库中 如果存在提示

            //1.创建异步对象
            var xhr = new XMLHttpRequest();

            //2.设置回调函数:当异步对象回来之后执行
            xhr.onreadystatechange = function() {
                //代表异步对象请求结束 并且服务器内部没有报错
                if (xhr.readyState == 4 && xhr.status == 200) {
                    var result = xhr.responseText; //代表服务器返回的 数据
                    if (result == "yes") {
                        document.getElementById("spanusername").style.color = "red";
                        document.getElementById("spanusername").innerHTML = "Sorry,用户名存在!";
                        flag=false;
                    } else {
                        document.getElementById("spanusername").style.color = "green";
                        document.getElementById("spanusername").innerHTML ="";
                        document.getElementById("spanusername").innerHTML = "恭喜,用户名可用!";
                        flag=true;
                    }
                }
            };

            //3.设置请求的方式(get post)
            var url = "UserServlet?act=ckUsername"; //请求的地址
            var data = "username=" + username; //要发送的数据
            xhr.open("POST", url, true); // true是异步
            xhr.setRequestHeader("Content-Type",
                "application/x-www-form-urlencoded");
            //4.发送请求
            xhr.send(data);

            return flag;

        }

        //校验密码
        function ckPass() {
            var pass = document.getElementById("password").value;
            if (pass.length == 0) {
                document.getElementById("spanpassword").style.color = "red";
                document.getElementById("spanpassword").innerHTML = "密码不能为空！";
                return false;
            }
            var reg = /^[a-zA-Z]\w{5,17}$/;
            if (reg.test(pass) == true) {
                if(pass.length>5&&pass.length<9){
                    document.getElementById("spanpassword").style.color = "#FF0000";
                    document.getElementById("spanpassword").innerHTML = "&nbsp;&nbsp;&nbsp;√&nbsp;&nbsp;&nbsp;密码强度：弱！！！";
                    return true;
                }
                if(pass.length>8&&pass.length<13){
                    document.getElementById("spanpassword").style.color = "#FF9900";
                    document.getElementById("spanpassword").innerHTML = "&nbsp;&nbsp;&nbsp;√&nbsp;&nbsp;&nbsp;密码强度：中！！";
                    return true;
                }
                if(pass.length>12&&pass.length<19){
                    document.getElementById("spanpassword").style.color = "green";
                    document.getElementById("spanpassword").innerHTML = "&nbsp;&nbsp;&nbsp;√&nbsp;&nbsp;&nbsp;密码强度：强！";
                    return true;
                }
            } else {
                document.getElementById("spanpassword").style.color = "red";
                document.getElementById("spanpassword").innerHTML = "请重新输入密码！正确格式为：以字母开头，长度在6-18之间，只能包含字符、数字和下划线。";
                return false;

            }

        }

        //校验真实姓名
        function ckName() {
            var name = document.getElementById("name").value;
            if (name.trim().length == 0) {
                document.getElementById("spanname").style.color = "red";
                document.getElementById("spanname").innerHTML = "名字不能为空!";
                return false;
            }
            document.getElementById("spanname").style.color = "green";
            document.getElementById("spanname").innerHTML = "&nbsp;&nbsp;&nbsp;√";
            return true;
        }

        // 校验邮箱
        function ckMail() {
            var useremail = document.getElementById("useremail").value;
            var reg = /^[a-zA-Z0-9_-]+@[a-zA-Z0-9_-]+(\.[a-zA-Z0-9_-]+)+$/;
            if (reg.test(useremail) == false) {
                document.getElementById("spanmail").style.color = "red";
                document.getElementById("spanmail").innerHTML = "邮箱格式不正确!";
                return false;
            }
            document.getElementById("spanmail").style.color = "green";
            document.getElementById("spanmail").innerHTML = "&nbsp;&nbsp;&nbsp;√";
            return true;
        }

        //校验角色信息
        function ckRole() {
            var role = document.getElementById("role").value;

            if (role == 0) {
                document.getElementById("spanrole").style.color = "red";
                document.getElementById("spanrole").innerHTML = "请选择角色!";
                return false;
            }
            document.getElementById("spanrole").style.color = "green";
            document.getElementById("spanrole").innerHTML = "&nbsp;&nbsp;&nbsp;√";
            return true;
        }

        //表单验证
        function ckForm() {
            if (confirm("确定要保存吗")==true){
                return ckUsername()&&ckPass()&&ckName()&&ckMail()&&ckRole();
            }else{
                return false;
            }
        }


    </script>
</head>
<body>
<form action="/user/addUser" method="post"
      class="definewidth m20" onsubmit="return ckForm();">
    <input type="hidden" name="id" value="{$user.id}" />
    <table class="table table-bordered table-hover definewidth m10">
        <tr>
            <td width="10%" class="tableleft">登录名</td>
            <td><input type="text" name="username" id="username"
                       onblur="ckUsername();" /> <span id="spanusername"></span></td>
        </tr>
        <tr>
            <td class="tableleft">密码</td>
            <td><input type="password" name="password" id="password"
                       onblur="ckPass();" /> <span id="spanpassword"></span></td>
        </tr>
        <tr>
            <td class="tableleft">真实姓名</td>
            <td><input type="text" name="name" id="name" onblur="ckName();" />
                <span id="spanname"></span></td>
        </tr>
        <tr>
            <td class="tableleft">邮箱</td>
            <td><input type="text" name="useremail" id="useremail"
                       onblur="ckMail();" /> <span id="spanmail"></span></td>
        </tr>
        <tr>
            <td class="tableleft">状态</td>
            <td><input type="radio" name="userstate" value="1" checked />
                启用 <input type="radio" name="userstate" value="0" /> 禁用</td>
        </tr>
        <tr>
            <td class="tableleft">角色</td>
            <td><select name="user_role_id" id="role" onblur="ckRole();">
                    <option value="0">--请选择--</option>
                    <#list rlist as rl>
                        <option value="${rl.roleid}">${rl.rolename}</option>
                    </#list>
                </select> <span id="spanrole"></span></td>
        </tr>
        <tr>
            <td class="tableleft"></td>
            <td>
                <button type="submit" class="btn btn-primary" type="button" onclick="add()">添加</button>&nbsp;&nbsp;
                <button type="button" class="btn btn-success" name="backid"
                        id="backid" onclick="window.history.back()">返回列表</button>
            </td>
        </tr>
    </table>
</form>
</body>
</html>