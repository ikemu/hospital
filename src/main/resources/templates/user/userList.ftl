<!DOCTYPE html>
<html>
<head>
    <title></title>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="/static/Css/bootstrap.css" />
    <link rel="stylesheet" type="text/css" href="/static/Css/bootstrap-responsive.css" />
    <link rel="stylesheet" type="text/css" href="/static/Css/style.css" />
    <script type="text/javascript" src="/static/Js/jquery.js"></script>
    <script type="text/javascript" src="/static/Js/jquery.sorted.js"></script>
    <script type="text/javascript" src="/static/Js/bootstrap.js"></script>
    <script type="text/javascript" src="/static/Js/ckform.js"></script>
    <script type="text/javascript" src="/static/Js/common.js"></script>
    <script type="text/javascript" src="/static/Js/js.js"></script>
    <style type="text/css">
        body {
            padding-bottom: 40px;
        }
        .sidebar-nav {
            padding: 9px 0;
        }

        @media (max-width: 980px) {
            /* Enable use of floated navbar text */
            .navbar-text.pull-right {
                float: none;
                padding-left: 5px;
                padding-right: 5px;
            }
        }
    </style>
    <script type="text/javascript">
     $(function () {
		$('#newNav').click(function(){
				window.location.href="/user/add";
		 });
    });
     //单项删除
     /*function dele(ids){
         if(confirm("确认删除")==true){
             window.location.href="/user/remove";
         }
     }*/
     //选择
    function checkall(){
			var alls=document.getElementsByName("check");
			var ch=document.getElementById("checkall");
			if(ch.checked){
				for(var i=0;i<alls.length;i++){
					alls[i].checked=true;	
				}	
			}else{
				for(var i=0;i<alls.length;i++){
					alls[i].checked=false;	
				}	
			}
		}
		function delAll(){
			var alls=document.getElementsByName("check");
			var ids=new Array();
			for(var i=0;i<alls.length;i++){
				if(alls[i].checked){
					ids.push(alls[i].value);
				}		
			}
			if(ids.length>0){
				if(confirm("确认删除?")){
                    window.location.href="/user/removes?rids="+ids;
				}
			}else{
				alert("请选中要删除的项");
			}
		}
    </script>
</head>
<body>
<form class="form-inline definewidth m20" action="/user/listByUsername" method="post">
    用户名称：
    <input type="text" name="name" id="name" class="abc input-default" value="<#if name??>${name}</#if>">&nbsp;&nbsp;
    <button type="submit" class="btn btn-primary">查询</button>

</form>
<table class="table table-bordered table-hover definewidth m10">
    <thead>
    <tr>
    	<th width="5%"><input type="checkbox" id="checkall" onChange="checkall();"></th>
        <th>用户账户</th>
        <th>真实姓名</th>
        <th>角色</th>
        <th  width="10%">操作</th>
    </tr>
    </thead>
        <#list usersList as user>
	     <tr>
            <input type="hidden" value="${user.userid}"/>
         	<td style="vertical-align:middle;"><input type="checkbox" name="check" value="${user.userid}"></td>

            <td>${user.username}</td>
            <td><#if user.name??>${user.name}<#else>空</#if></td>
            <td>
                <#if user.userstate??> ${((user.userstate)==1)?string('已启用','禁用')}<#else >空</#if>
            <td>
                <a href="/user/update?userid=${user.userid}">编辑</a>&nbsp;&nbsp;&nbsp;
                <a href="#" class="removeUser">删除</a>
            </td>

         </tr>
        </#list>
</table>
<table class="table table-bordered table-hover definewidth m10" >
    <tr>
        <th colspan="5">
            <div class="inline pull-right page">
                <a href='/user/list?pageNum=1'>第一页</a> <#if (page.pageNum-1)&gt;0><a href='/user/list?pageNum=${page.pageNum-1}'>上一页</a> </#if>
                    <span class='current'>
                    <a href='/user/list?pageNum=1'>1</a></span><a href='/user/list?pageNum=2'>2</a><a href='/user/list?pageNum=3'>3</a><a href='/user/list?pageNum=4'>4</a><a href='/user/list?pageNum=5'>5</a>
                <#if (page.pageNum+1)<=page.pages><a href='/user/list?pageNum=${page.pageNum+1}'>下一页</a></#if>
                <a href='/user/list?pageNum=${page.pages}'>最后一页</a>&nbsp;&nbsp;&nbsp;
                共<span class='current'>${page.total}</span>条记录<span class='current'> ${page.pageNum}/${page.pages}</span>页
            </div>
            <div>
                <button type="button" class="btn btn-success" id="newNav">添加用户</button>&nbsp;&nbsp;&nbsp;
                <button type="button" class="btn btn-success" id="delPro" onClick="delAll();">删除选中</button>
            </div>

    </th>
    </tr>
</table>
<!--点击删除按钮后弹出的页面-->
<div class="zhezhao"></div>
<div class="remove" id="removeUse">
    <div class="removerChid">
        <h2>提示</h2>
        <div class="removeMain">
            <p>你确定要删除该用户吗？</p>
            <a href="/user/remove?userid=" id="yes">确定</a>
            <a href="#" id="no">取消</a>
        </div>
    </div>
</div>
</body>
<script>
    $(".removeUser").click(function () {
        var userid = $(this).parents("tr").children().eq(0).val();
        /*alert(userid);*/
        var oldUrl = $("#yes").attr("href");
        var newUrl = oldUrl + userid;
        $("#yes").attr("href",newUrl);
        $('.zhezhao').css('display', 'block');
        $('#removeUse').fadeIn();
    });
</script>
</html>