<!DOCTYPE html>
<html>
<head>
    <title>门诊查询--中软高科-2015</title>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="/static/Css/bootstrap.css" />
    <link rel="stylesheet" type="text/css" href="/static/Css/bootstrap-responsive.css" />
    <link rel="stylesheet" type="text/css" href="/static/Css/style.css" />
    <script type="text/javascript" src="/static/Js/jquery.js"></script>
    <script type="text/javascript" src="/static/Js/jquery.sorted.js"></script>
    <script type="text/javascript" src="/static/Js/bootstrap.js"></script>
    <script type="text/javascript" src="/static/Js/ckform.js"></script>
    <script type="text/javascript" src="/static/Js/common.js"></script>
    <script type="text/javascript" src="/static/Js/jquery-1.12.4.js"></script>
    <style type="text/css">
        body {
            padding-bottom: 40px;
        }
        .sidebar-nav {
            padding: 9px 0;
        }

        @media (max-width: 980px) {
            /* Enable use of floated navbar text */
            .navbar-text.pull-right {
                float: none;
                padding-left: 5px;
                padding-right: 5px;
            }
        }


    </style>
    <script type="text/javascript">
	 $(function () {
		$('#newNav').click(function(){
				window.location.href="/registration/add";
		 });
    });
	
    	function checkall(){
			var alls=document.getElementsByName("check");
			var ch=document.getElementById("checkall");
			if(ch.checked){
				for(var i=0;i<alls.length;i++){
					alls[i].checked=true;	
				}	
			}else{
				for(var i=0;i<alls.length;i++){
					alls[i].checked=false;	
				}	
			}
		}
		function delAll(){
			var alls=document.getElementsByName("check");
			var ids=new Array();
			for(var i=0;i<alls.length;i++){
				if(alls[i].checked){
					ids.push(alls[i].value);
				}		
			}
			if(ids.length>0){
				if(confirm("确认操作?")){
                    window.location.href="/registration/delRegistrations?rids="+ids;
				}
			}else{
				alert("请选中要操作的项");
			}
		}
     function doExcel(){
         var alls=document.getElementsByName("check");
         var ids=new Array();
         for(var i=0;i<alls.length;i++){
             if(alls[i].checked){
                 ids.push(alls[i].value);
             }
         }
         if(ids.length>0){
             if(confirm("确认操作?")){
                 window.location.href="/registration/daoexcel?rids="+ids;
             }
         }else{
             alert("请选中要操作的项");
         }
     }
    </script>
</head>
<body>

<form action="/registration/index" method="post" class="definewidth m20">
<table class="table table-bordered table-hover definewidth m10">
    <tr>
        <td width="10%" class="tableleft">病历号：</td>
        <td><input type="text" name="behpid" value="<#if behpid??>${behpid}</#if>"/></td>
		
        <td width="10%" class="tableleft">主治医生：</td>
        <td><input type="text" name="dname" value="<#if dname??>${dname}</#if>"/></td>
		
        <td width="10%" class="tableleft">科室：</td>
        <td><input type="text" name="departname" value="<#if departname??>${departname}</#if>"/></td>
    </tr>
    <tr>
		
        <td width="10%" class="tableleft">挂号时间：</td>
		
		  <td colspan="5">
			<input type="text" name="hosrfromdate" value="<#if hosrfromdate??>${hosrfromdate}</#if>"/>&nbsp;至&nbsp;<input type="text" name="hosrtodate" value="<#if hosrtodate??>${hosrtodate}</#if>"/>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <button type="submit" class="btn btn-primary" type="button">查询</button> 
            <button type="reset" class="btn btn-primary" type="button">清空</button>
			
        </td>
    </tr>
</table>
</form>
   
<table class="table table-bordered table-hover definewidth m10" >
   <thead>
    <tr>
    	<th><input type="checkbox" id="checkall" onChange="checkall();"></th>
        <th>门诊编号</th>
        <th>主治医生</th>
        <th>挂号时间</th>
        <th>挂号科室</th>
        <th>状态</th>
        <th>操作</th>
    </tr>
    </thead>
    <#list hlist as li>
	     <tr >
            <input type="hidden" value="${li.hosrid}"/>
         	<td style="vertical-align:middle;"><input type="checkbox" name="check" value="${li.hosrid}"></td>
            <td style="vertical-align:middle;">${li.hrDepartId}</td>
            <td style="vertical-align:middle;">${li.doctor.dname}</td>
            <td style="vertical-align:middle;">${li.hosrdate?string("yyyy-MM-dd")}</td>
            <td style="vertical-align:middle;">${li.department.departname}</td>
            <td style="vertical-align:middle;">${li.state.statename}</td>
            <td style="vertical-align:middle;">
        <#if li.state.stateid == 1><a href="/registration/look?hosrid=${li.hosrid}">详情>>></a>&nbsp;&nbsp;&nbsp;<a href="/registration/edit?hosrid=${li.hosrid}">更改</a>&nbsp;&nbsp;&nbsp;<a href="#<#--javascript:alert('退号成功！');-->" class="removeRegistration">退号</a></#if>
        <#if li.state.stateid == 3><a href="/registration/look?hosrid=${li.hosrid}">详情>>></a>&nbsp;&nbsp;&nbsp;<a href="/registration/edit?hosrid=${li.hosrid}">更改</a></#if>
        <#if li.state.stateid == 2||li.state.stateid == 4||li.state.stateid == 5||li.state.stateid == 6><a href="/registration/look?hosrid=${li.hosrid}">详情>>></a>&nbsp;&nbsp;&nbsp;<a href="/registration/edit?hosrid=${li.hosrid}">更改</a></#if>
        </td>
        </tr>
    </#list>
  </table>
  
  <table class="table table-bordered table-hover definewidth m10" >
  	<tr><th colspan="5">  <div class="inline pull-right page">
                <a href='/registration/index?pageNum=1<#if behpid??>&behpid=${behpid}</#if><#if dname??>&dname=${dname}</#if><#if departname??>&departname=${departname}</#if><#if hosrfromdate??>&hosrfromdate=${hosrfromdate}</#if><#if hosrfromdate??>&hosrfromdate=${hosrfromdate}</#if>' >第一页</a>
                <#if (page.pageNum-1)&gt;0><a href='/registration/index?pageNum=${page.pageNum-1}'>上一页</a> </#if>    <span class='current'><a href='/registration/index?pageNum=1'>1</a></span><a href='/registration/index?pageNum=2'>2</a><a href='/registration/index?pageNum=3'>3</a><a href='/registration/index?pageNum=4'>4</a><a href='/registration/index?pageNum=5'>5</a>  <#if (page.pageNum+1)<=page.pages> <a href='/registration/index?pageNum=${page.pageNum+1}' >下一页</a></#if> <a href='/registration/index?pageNum=${page.pages}' >最后一页</a>
                &nbsp;&nbsp;&nbsp;共<span class='current'>${page.total}</span>条记录<span class='current'> ${page.pageNum}/${page.pages}</span>页
            </div>
		 <div><button type="button" class="btn btn-success" id="newNav">门诊挂号</button>&nbsp;&nbsp;&nbsp;
		 <button type="button" class="btn btn-success" id="delPro" onClick="delAll();">退号</button>&nbsp;&nbsp;&nbsp;
		 <button type="button" class="btn btn-success" id="delPro" onClick="doExcel()">导出Excel</button>
		
		 
		 </div>
		 
		 </th></tr>
  </table>
<!--点击退号按钮后弹出的页面-->
<div class="zhezhao"></div>
<div class="remove" id="removeBi">
    <div class="removerChid">
        <h2>提示</h2>
        <div class="removeMain">
            <p>你确定要退号吗？</p>
            <a href="/registration/delRegistration?hosrid=" id="yes">确定</a>
            <a href="#" id="no">取消</a>
        </div>
    </div>
</div>
<script>
    $(function () {
        $('.removeRegistration').click(function () {
            var hosrid=$(this).parents("tr").children().eq(0).val();
            var url=$("#yes").attr("href");
            $("#yes").attr("href",url+hosrid);
            $('.zhezhao').css('display', 'block');
            $('#removeRegistration').fadeIn();
        });
    });
</script>
</body>
</html>
