<!DOCTYPE html>
<html>
<head>
    <title>查看--中软高科-2015</title>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="/static/Css/bootstrap.css" />
    <link rel="stylesheet" type="text/css" href="/static/Css/bootstrap-responsive.css" />
    <link rel="stylesheet" type="text/css" href="/static/Css/style.css" />
    <script type="text/javascript" src="/static/Js/jquery.js"></script>
    <script type="text/javascript" src="/static/Js/jquery.sorted.js"></script>
    <script type="text/javascript" src="/static/Js/bootstrap.js"></script>
    <script type="text/javascript" src="/static/Js/ckform.js"></script>
    <script type="text/javascript" src="/static/Js/common.js"></script>
    <script type="text/javascript" src="/static/Js/ckeditor/ckeditor.js"></script>
 

    <style type="text/css">
        body {
            padding-bottom: 40px;
        }
        .sidebar-nav {
            padding: 9px 0;
        }

        @media (max-width: 980px) {
            /* Enable use of floated navbar text */
            .navbar-text.pull-right {
                float: none;
                padding-left: 5px;
                padding-right: 5px;
            }
        }


    </style>
    <script type="text/javascript">
    $(function () {       
		$('#backid').click(function(){
				window.location.href="/registration/index";
		 });
    });
    </script>
</head>
<body>
<form action="index.html" method="post" class="definewidth m20">
<table class="table table-bordered table-hover definewidth m10">
    <tr>
        <td width="10%" class="tableleft">姓名</td>
        <td>${hosregister.hosrname}</td>
    </tr>

    <tr>
        <td width="10%" class="tableleft">身份证号</td>
        <td>${hosregister.hosridcard}</td>
    </tr>
    <tr>
        <td width="10%" class="tableleft">挂号费</td>
        <td>${hosregister.hosridcard}</td>
    </tr>
    <tr>
        <td width="10%" class="tableleft">社保号</td>
        <td>${hosregister.hosrssnum}</td>
    </tr>
    <tr>
        <td width="10%" class="tableleft">联系电话</td>
        <td>${hosregister.hosrphone}</td>
    </tr>
    <tr>
        <td width="10%" class="tableleft">是否自费</td>
        <td><#if hosregister.hosrvisit==0>是</#if><#if hosregister.hosrvisit==1>否</#if></td>
    </tr>
    <tr>
        <td width="10%" class="tableleft">性别</td>
        <td><#if hosregister.hosrsex==0>男</#if><#if hosregister.hosrsex==1>女</#if></td>
    </tr>
    <tr>
        <td width="10%" class="tableleft">年龄</td>
        <td>${hosregister.hosrage}</td>
    </tr>
    <tr>
        <td width="10%" class="tableleft">职业</td>
        <td>${hosregister.hosrjob}</td>
    </tr>
    <tr>
        <td width="10%" class="tableleft">初复诊</td>
        <td><#if hosregister.hosrLookdoctor==0>初诊</#if><#if hosregister.hosrLookdoctor==1>复诊</#if></td>
    </tr>
    <tr>
        <td width="10%" class="tableleft">所挂科室</td>
        <td>${hosregister.department.departname}</td>
    </tr>
    <tr>
        <td width="10%" class="tableleft">指定医生</td>
        <td>${hosregister.doctor.dname}</td>
    </tr>
	<tr>
        <td width="10%" class="tableleft">备注</td>
        <td>${hosregister.hosrremark}</td>
	</tr>
    <tr>
        <td colspan="2">
			<center>
				<button type="button" class="btn btn-success" name="backid" id="backid">返回列表</button>
			</center
		</td>
    </tr>
</table>
</form>
</body>
</html>