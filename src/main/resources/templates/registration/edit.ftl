<!DOCTYPE html>
<html>
<head>
    <title>挂号--中软高科-2015</title>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="/static/Css/bootstrap.css" />
    <link rel="stylesheet" type="text/css" href="/static/Css/bootstrap-responsive.css" />
    <link rel="stylesheet" type="text/css" href="/static/Css/style.css" />
    <script type="text/javascript" src="/static/Js/jquery.js"></script>
    <script type="text/javascript" src="/static/Js/jquery.sorted.js"></script>
    <script type="text/javascript" src="/static/Js/bootstrap.js"></script>
    <script type="text/javascript" src="/static/Js/ckform.js"></script>
    <script type="text/javascript" src="/static/Js/common.js"></script>
    <script type="text/javascript" src="/static/Js/ckeditor/ckeditor.js"></script>
 

    <style type="text/css">
        body {
            padding-bottom: 40px;
        }
        .sidebar-nav {
            padding: 9px 0;
        }

        @media (max-width: 980px) {
            /* Enable use of floated navbar text */
            .navbar-text.pull-right {
                float: none;
                padding-left: 5px;
                padding-right: 5px;
            }
        }


    </style>
    <script type="text/javascript">
    $(function () {       
		$('#backid').click(function(){
				window.location.href="/registration/index";
		 });
    });
    </script>
</head>
<body>
<form action="/registration/editHosregister" method="post" class="definewidth m20">
<table class="table table-bordered table-hover definewidth m10">
    <input type="hidden" name="hosrid" value="${hosregister.hosrid}"/>
    <tr>
        <td width="10%" class="tableleft"><font color="red">*</font>姓名</td>
        <td><input type="text" name="hosrname" value="${hosregister.hosrname}"/></td>
    </tr>

    <tr>
        <td width="10%" class="tableleft"><font color="red">*</font>身份证号</td>
        <td><input type="text" name="hosridcard" value="${hosregister.hosridcard}"/></td>
    </tr>
    <tr>
        <td width="10%" class="tableleft"><font color="red">*</font>挂号费</td>
        <td><input type="text" name="hosrmoney" value="${hosregister.hosridcard}"/>元</td>
    </tr>
    <tr>
        <td width="10%" class="tableleft">社保号</td>
        <td><input type="text" name="hosrssnum" value="${hosregister.hosrssnum}"/></td>
    </tr>
    <tr>
        <td width="10%" class="tableleft"><font color="red">*</font>联系电话</td>
        <td><input type="text" name="hosrphone" value="${hosregister.hosrphone}"/></td>
    </tr>
    <tr>
        <td width="10%" class="tableleft"><font color="red">*</font>是否自费</td>
        <td><input type="radio" name="hosrvisit" value="1" <#if hosregister.hosrvisit==1>checked</#if>/>否&nbsp;&nbsp;&nbsp;<input type="radio" name="hosrvisit" value="0" <#if hosregister.hosrvisit==0>checked</#if>/>是</td>
    </tr>
    <tr>
        <td width="10%" class="tableleft"><font color="red">*</font>性别</td>
        <td><input type="radio" name="hosrsex" value="0" <#if hosregister.hosrsex==0>checked</#if>/>男&nbsp;&nbsp;&nbsp;<input type="radio" name="hosrsex" value="1" <#if hosregister.hosrsex==1>checked</#if>/>女</td>
    </tr>
    <tr>
        <td width="10%" class="tableleft"><font color="red">*</font>年龄</td>
        <td><input type="text" name="hosrage" value="${hosregister.hosrage}"/></td>
    </tr>
    <tr>
        <td width="10%" class="tableleft">职业</td>
        <td><input type="text" name="hosrjob" value="${hosregister.hosrjob}"/></td>
    </tr>
    <tr>
        <td width="10%" class="tableleft">初复诊</td>
        <td><input type="radio" name="hosrLookdoctor" value="0" <#if hosregister.hosrLookdoctor==0>checked</#if>/>初诊&nbsp;&nbsp;&nbsp;<input type="radio" name="hosrLookdoctor" value="1" <#if hosregister.hosrLookdoctor==1>checked</#if>/>复诊</td>
    </tr>
    <tr>
        <td width="10%" class="tableleft"><font color="red">*</font>所挂科室</td>
        <td><select name="hrDepartId">
                <#list depart as dt>
                    <option value="${dt.departid}" <#if dt.departid==hosregister.hrDepartId> selected="true"</#if> >${dt.departname}</option>
                </#list>
            </select></td>
    </tr>
    <tr>
        <td width="10%" class="tableleft"><font color="red">*</font>指定医生</td>
        <td><select name="hrDocId">
                <#list doctor as d>
                    <option value="${d.did}" <#if d.did==hosregister.hrDocId> selected="true"</#if> >${d.dname}</option>
                </#list>
            </select></td>
    </tr>
	<tr>
        <td width="10%" class="tableleft">备注</td>
        <td><textarea>${hosregister.hosrremark}</textarea></td>
	</tr>
    <tr>
        <td colspan="2">
			<center>
				<button type="submit" class="btn btn-primary" type="button">保存</button> &nbsp;&nbsp;<button type="button" class="btn btn-success" name="backid" id="backid">返回列表</button>
			</center>
		</td>
    </tr>
</table>
</form>
</body>
</html>