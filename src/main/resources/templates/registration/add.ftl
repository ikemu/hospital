<!DOCTYPE html>
<html>
<head>
    <title>更改挂号--中软高科-2015</title>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="/static/Css/bootstrap.css" />
    <link rel="stylesheet" type="text/css" href="/static/Css/bootstrap-responsive.css" />
    <link rel="stylesheet" type="text/css" href="/static/Css/style.css" />
    <script type="text/javascript" src="/static/Js/jquery.js"></script>
    <script type="text/javascript" src="/static/Js/jquery.sorted.js"></script>
    <script type="text/javascript" src="/static/Js/bootstrap.js"></script>
    <script type="text/javascript" src="/static/Js/ckform.js"></script>
    <script type="text/javascript" src="/static/Js/common.js"></script>
    <script type="text/javascript" src="/static/Js/ckeditor/ckeditor.js"></script>
 

    <style type="text/css">
        body {
            padding-bottom: 40px;
        }
        .sidebar-nav {
            padding: 9px 0;
        }

        @media (max-width: 980px) {
            /* Enable use of floated navbar text */
            .navbar-text.pull-right {
                float: none;
                padding-left: 5px;
                padding-right: 5px;
            }
        }


    </style>
    <script type="text/javascript">
    $(function () {       
		$('#backid').click(function(){
				window.location.href="/registration/index";
		 });
    });
    </script>
</head>
<body>
<form action="/registration/addHosregister" method="post" class="definewidth m20">
<table class="table table-bordered table-hover definewidth m10">
    <tr>
        <td width="10%" class="tableleft"><font color="red">*</font>姓名</td>
        <td><input type="text" name="hosrname" value="<#if hosregister.hosrname??>${hosregister.hosrname}</#if>"/></td>
    </tr>

    <tr>
        <td width="10%" class="tableleft"><font color="red">*</font>身份证号</td>
        <td><input type="text" name="hosridcard" value="<#if hosregister.hosridcard??>${hosregister.hosridcard}</#if>"/></td>
    </tr>
    <tr>
        <td width="10%" class="tableleft">社保号</td>
        <td><input type="text" name="hosrssnum" value="<#if hosregister.hosrssnum??>${hosregister.hosrssnum}</#if>"/></td>
    </tr>
    <tr>
        <td width="10%" class="tableleft"><font color="red">*</font>挂号费</td>
        <td><input type="text" name="hosrmoney" value="<#if hosregister.hosridcard??>${hosregister.hosridcard}</#if>"/>元</td>
    </tr>
    <tr>
        <td width="10%" class="tableleft"><font color="red">*</font>联系电话</td>
        <td><input type="text" name="hosrphone" value="<#if hosregister.hosrphone??>${hosregister.hosrphone}</#if>"/></td>
    </tr>
    <tr>
        <td width="10%" class="tableleft"><font color="red">*</font>是否自费</td>
        <td><input type="radio" name="hosrvisit" value="1" checked/>否&nbsp;&nbsp;&nbsp;<input type="radio" name="hosrvisit" value="0"/>是</td>
    </tr>
    <tr>
        <td width="10%" class="tableleft"><font color="red">*</font>性别</td>
        <td><input type="radio" name="hosrsex" value="0" checked/>男&nbsp;&nbsp;&nbsp;<input type="radio" name="hosrsex" value="1" />女</td>
    </tr>
    <tr>
        <td width="10%" class="tableleft">年龄</td>
        <td><input type="text" name="hosrage" value="<#if hosregister.hosrage??>${hosregister.hosrage}</#if>"/></td>
    </tr>
    <tr>
        <td width="10%" class="tableleft">职业</td>
        <td><input type="text" name="hosrjob" value="<#if hosregister.hosrjob??>${hosregister.hosrjob}</#if>"/></td>
    </tr>
    <tr>
        <td width="10%" class="tableleft">初复诊</td>
        <td><input type="radio" name="hosrLookdoctor" value="0" checked/>初诊&nbsp;&nbsp;&nbsp;<input type="radio" name="hosrLookdoctor" value="1"/>复诊</td>
    </tr>
    <tr>
        <td width="10%" class="tableleft"><font color="red">*</font>所挂科室</td>
        <td><select name="dDepartId">
                <#list depart as dt>
                    <option value="${dt.departid}" >${dt.departname}</option>
                </#list>
            </select></td>
    </tr>
    <tr>
        <td width="10%" class="tableleft"><font color="red">*</font>指定医生</td>
        <td><select name="hrDocId">
                <#list doctor as d>
                    <option value="${d.did}">${d.dname}</option>
                </#list>
            </select></td>
    </tr>
	<tr>
        <td width="10%" class="tableleft">备注</td>
        <td><textarea><#if hosregister.hosrremark??>${hosregister.hosrremark}</#if></textarea></td>
	</tr>
    <tr>
        <td colspan="2">
			<center>
				<button type="submit" class="btn btn-primary" type="button">保存</button> &nbsp;&nbsp;
                <button type="button" class="btn btn-success" name="backid" id="backid">返回列表</button>
			</center
		</td>
    </tr>
</table>
</form>
</body>
</html>